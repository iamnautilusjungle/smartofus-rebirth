<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEffectItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dofus_effect_item', function (Blueprint $table) {
            $table->increments("id");
            $table->unsignedSmallInteger('effect_id');
            $table->unsignedInteger('item_id');
            $table->smallInteger('dice_num')->nullable();
            $table->smallInteger('dice_side')->nullable();
            $table->smallInteger('value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characteristic_item');
    }
}
