<?php

use App\Models\Title;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTitlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('titles', function (Blueprint $table) {
            $table->unsignedTinyInteger("id")->autoIncrement();
            $table->string("name");
            $table->string("description")->nullable();
            $table->timestamps();
        });

        foreach ([
            [
                "name" => "Tofu Tilisateur",
                "description" => "Un utilisateur classique."
            ],
            [
                "name" => "Tofu Royal",
                "description" => "Créateur du site."
            ],
        ] as $titleVals) {
            $title = new Title($titleVals);
            $title->save();
        }

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedTinyInteger("title_id")->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('titles');

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('title_id');
        });
    }
}
