<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpellsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('dofus_spells', function (Blueprint $table) {
      $table->unsignedSmallInteger('id');
      $table->json('name_translations')->nullable();
      $table->unsignedMediumInteger("name_id")->nullable();
      $table->json('description_translations')->nullable();
      $table->unsignedMediumInteger("description_id")->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('dofus_spells');
  }
}
