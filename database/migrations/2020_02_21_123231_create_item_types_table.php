<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dofus_item_types', function (Blueprint $table) {
            $table->unsignedSmallInteger('id');
            $table->json('name_translations')->nullable();
            $table->unsignedMediumInteger("name_id")->nullable();
            $table->smallInteger("craft_xp_ratio")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_types');
    }
}
