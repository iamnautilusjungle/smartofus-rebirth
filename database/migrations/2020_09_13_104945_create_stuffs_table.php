<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStuffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dofus_stuffs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger("creator_id")->nullable();
            $table->string("name")->nullable();
            $table->unsignedTinyInteger("level")->default(1);
            $table->boolean("is_private")->default(false);

            $table->unsignedInteger("weapon_id")->nullable();
            $table->unsignedInteger("hat_id")->nullable();
            $table->unsignedInteger("cloak_id")->nullable();
            $table->unsignedInteger("amulet_id")->nullable();
            $table->unsignedInteger("ring_one_id")->nullable();
            $table->unsignedInteger("ring_two_id")->nullable();
            $table->unsignedInteger("belt_id")->nullable();
            $table->unsignedInteger("boots_id")->nullable();
            $table->unsignedInteger("trophy_one_id")->nullable();
            $table->unsignedInteger("trophy_two_id")->nullable();
            $table->unsignedInteger("trophy_three_id")->nullable();
            $table->unsignedInteger("trophy_four_id")->nullable();
            $table->unsignedInteger("trophy_five_id")->nullable();
            $table->unsignedInteger("trophy_six_id")->nullable();
            $table->unsignedInteger("shield_id")->nullable();

            foreach (["vitality", "wisdom", "strength", "intelligence", "agility", "chance"] as $f) {
                $table->unsignedSmallInteger("${f}_base")->default(0);
                $table->unsignedTinyInteger("${f}_parchment")->default(0);
            }

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dofus_stuffs');
    }
}
