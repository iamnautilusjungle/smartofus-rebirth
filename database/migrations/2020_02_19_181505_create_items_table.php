<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('dofus_items', function (Blueprint $table) {
      $table->unsignedInteger('id');
      $table->json('name_translations')->nullable();
      $table->unsignedTinyInteger("level")->nullable();
      $table->unsignedMediumInteger("name_id")->nullable();
      $table->unsignedSmallInteger("set_id")->nullable();
      $table->unsignedSmallInteger("type_id")->nullable();
      $table->unsignedTinyInteger("crafter_id")->nullable();
      $table->unsignedTinyInteger("critical_hit_bonus")->nullable();
      $table->smallInteger("craft_xp_ratio")->nullable();
      $table->timestamps();

      $table->primary(["id"]);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('items');
  }
}
