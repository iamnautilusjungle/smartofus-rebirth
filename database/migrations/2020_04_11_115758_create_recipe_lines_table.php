<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipeLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dofus_recipe_lines', function (Blueprint $table) {
            $table->unsignedInteger('crafted_id');
            $table->unsignedInteger('needed_id');
            $table->unsignedSmallInteger('quantity');
            $table->timestamps();

            $table->primary(["crafted_id", "needed_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dofus_recipe_lines');
    }
}
