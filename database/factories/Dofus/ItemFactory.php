<?php

namespace Database\Factories\Dofus;

use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \App\Models\Dofus\Item::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->unique()->randomNumber(4),
            'name_id' => $this->faker->unique()->randomNumber(7),
            'set_id' => $this->faker->optional()->randomNumber(3),
            'type_id' => $this->faker->randomNumber(3),
            'crafter_id' => $this->faker->randomNumber(2),
            'craft_xp_ratio' => $this->faker->randomNumber(2),
        ];
    }
}
