<?php

namespace Database\Seeders\Dofus;

use Illuminate\Database\Seeder;

class StuffSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Dofus\Stuff::factory()
            ->count(25)
            ->create();
    }
}
