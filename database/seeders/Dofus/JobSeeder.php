<?php

namespace Database\Seeders\Dofus;

use Illuminate\Database\Seeder;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Dofus\Job::factory()
            ->count(25)
            ->hasCrafts(100)
            ->create();
    }
}
