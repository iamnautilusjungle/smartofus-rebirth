<?php

namespace Database\Seeders;

use Database\Seeders\Dofus\JobSeeder;
use Database\Seeders\Dofus\StuffSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            JobSeeder::class,
            StuffSeeder::class
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
