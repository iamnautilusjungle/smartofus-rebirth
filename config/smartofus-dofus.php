<?php

use App\Feeders\BreedFeeder;
use App\Feeders\CharacteristicFeeder;
use App\Feeders\EffectFeeder;
use App\Feeders\ItemFeeder;
use App\Feeders\ItemSetFeeder;
use App\Feeders\ItemTypeFeeder;
use App\Feeders\JobFeeder;
use App\Feeders\RecipeFeeder;
use App\Feeders\SpellFeeder;

return [
  "feeders" => [
    "list" => [
      ItemFeeder::class,
      JobFeeder::class,
      RecipeFeeder::class,
      EffectFeeder::class,
      ItemTypeFeeder::class,
      ItemSetFeeder::class,
      CharacteristicFeeder::class,
      BreedFeeder::class,
      SpellFeeder::class,
    ],
    "langs" => [
      "fr",
      "en"
    ]
  ]
];
