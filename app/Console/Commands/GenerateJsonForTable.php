<?php

namespace App\Console\Commands;

use App\Feeders\AbstractFeeder;
use Illuminate\Console\Command;

class GenerateJsonForTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartofus:locales {--custom}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a JSON of strings for a table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $list = AbstractFeeder::getList();

        $choices = config("smartofus-dofus.feeders.list");
        $langs = config("smartofus-dofus.feeders.langs");

        if ($this->option("custom")) {
            $choices = $this->choice('Which tables do you want to feed? (separate choices with a comma like 2,3,5)', $list, null, null, true);
            $choices = (array) $choices;
        }

        AbstractFeeder::locales($choices, $langs);
    }
}
