<?php

namespace App\Console\Commands;

use App\Feeders\AbstractFeeder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class Feed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smartofus:feed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Feeds a json into the database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $list = AbstractFeeder::getList();

        $choices = $this->choice('Which tables do you want to feed? (separate choices with a comma like 2,3,5)', $list, null, null, true);
        $choices = (array) $choices;

        if (in_array('All', $choices)) {
            $choices = $list;
        }

        $bar = $this->output->createProgressBar(count($choices));
        $bar->start();

        foreach ($choices as $table) {
            AbstractFeeder::FEEDERS[$table]::feed();
            $bar->advance();
        }

        $bar->finish();
    }
}
