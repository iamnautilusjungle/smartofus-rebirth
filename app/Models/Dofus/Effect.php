<?php

namespace App\Models\Dofus;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Effect extends Model
{
  protected $table = "dofus_effects";

  protected $hidden = [
    'name_translations',
  ];

  use HasTranslations;

  public $translatable = ['name_translations'];

  const SPELLS_BONUSES = [
    280,
    281,
    282,
    283,
    284,
    285,
    286,
    287,
    288,
    289,
    290,
    291,
    292,
    293,
    294,
    295,
    296,
  ];

  const SPECIAL_EFFECTS = [
    1175
  ];

  const HIDDEN_EFFECTS = [
    10,
    983,
  ];

  protected $fillable = [
    "id",
    "name_id",
  ];

  // public function getSpellDescAttribute()
  // {
  //     return Cache::rememberForever("spelldesc_" . App::getLocale() . "_" . $this->pivot->dice_num, function () {
  //         return Spell::find($this->pivot->dice_num)->description;
  //     });
  // }

  // public function getSpellNameAttribute()
  // {
  //     return Cache::rememberForever("spellname_" . App::getLocale() . "_" . $this->pivot->dice_num, function () {
  //         return Spell::find($this->pivot->dice_num)->name;
  //     });
  // }

  public function getNameAttribute()
  {
    $name = $this->name_translations;
    $pivot = $this->pivot;

    if (!$pivot) {
      return $name;
    }

    if (in_array($this->id, self::SPELLS_BONUSES)) {
      $name = str_replace("#3", $pivot->value, $name);
      $name = str_replace("#1", $this->spell_name, $name);
      return $name;
    }

    if (in_array($this->id, self::SPECIAL_EFFECTS)) {
      $name = str_replace("#1", $this->spell_desc, $name);
      return $name;
    }

    if ($pivot->dice_num && $pivot->dice_side) {
      $name = str_replace("{~1~2", "", $name);
      $name = str_replace("}", "", $name);
      $name = str_replace("#1", $pivot->dice_num, $name);
      $name = str_replace("#2", $pivot->dice_side, $name);
      return $name;
    }

    if ($pivot->dice_num && !$pivot->dice_side) {
      $name = preg_replace("/#1{.*}/", "", $name);
      $name = str_replace("#2", $pivot->dice_num, $name);
      return $name;
    }

    return $name;
  }
}
