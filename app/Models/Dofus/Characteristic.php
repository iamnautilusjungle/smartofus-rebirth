<?php

namespace App\Models\Dofus;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Characteristic extends Model
{
  protected $table = "dofus_characteristics";

  protected $hidden = [
    'name_translations',
  ];

  use HasTranslations;

  protected $fillable = [
    "id",
    "name_id"
  ];

  public $translatable = ['name_translations'];
}
