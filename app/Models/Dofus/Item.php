<?php

namespace App\Models\Dofus;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Item extends Model
{
  protected $table = "dofus_items";

  protected $hidden = [
    'name_translations',
    'craft_xp_ratio',
  ];

  use HasTranslations;

  public $translatable = ['name_translations'];

  protected $fillable = [
    "id",
    "level",
    "craft_xp_ratio",
    "set_id",
    "type_id",
    "name_id",
    "critical_hit_bonus",
  ];

  public function crafter()
  {
    return $this->belongsTo(Job::class, "crafter_id");
  }

  public function recipe()
  {
    return $this->hasMany(RecipeLine::class, "crafted_id");
  }

  public function type()
  {
    return $this->belongsTo(ItemType::class, "type_id");
  }

  public function effects()
  {
    return $this->belongsToMany(Effect::class, "dofus_effect_item")->whereNotIn("dofus_effects.id", Effect::HIDDEN_EFFECTS)->where("name_translations->fr", "!=", "CORRUPTED")->withPivot("dice_num", "dice_side", "value");;
  }

  public function set()
  {
    return $this->belongsTo(ItemSet::class, "set_id");
  }

  public function getCraftXpAttribute()
  {
    $baseXp = $this->level * 20;
    if ($this->craft_xp_ratio == -1) { // -1 means inheritance
      if ($this->type && $this->type->craft_xp_ratio == -1) {
        return $baseXp;
      } else {
        return $baseXp * ($this->type->craft_xp_ratio / 100);
      }
    } else {
      return $baseXp * ($this->craft_xp_ratio / 100);
    }
  }
}
