<?php

namespace App\Models\Dofus;

use App\Models\User;
use App\Services\IdEncodingService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stuff extends Model
{
  protected $table = "dofus_stuffs";

  use HasFactory;

  protected $hidden = [
    'id',
    'name_translations',
  ];

  protected $fillable = [
    "creator_id",
    "name",
    "level",
    "is_private",
    "weapon_id",
    "hat_id",
    "cloak_id",
    "amulet_id",
    "ring_one_id",
    "ring_two_id",
    "belt_id",
    "boots_id",
    "trophy_one_id",
    "trophy_two_id",
    "trophy_three_id",
    "trophy_four_id",
    "trophy_five_id",
    "trophy_six_id",
    "shield_id",
    "vitality_base",
    "vitality_parchment",
    "wisdom_base",
    "wisdom_parchment",
    "strength_base",
    "strength_parchment",
    "intelligence_base",
    "intelligence_parchment",
    "agility_base",
    "agility_parchment",
    "chance_base",
    "chance_parchment",
  ];

  protected $appends = [
    "code"
  ];

  const ITEM_FIELDS = [
    "weapon" => "weapon_id",
    "hat" => "hat_id",
    "cloak" => "cloak_id",
    "amulet" => "amulet_id",
    "ring_one" => "ring_one_id",
    "ring_two" => "ring_two_id",
    "belt" => "belt_id",
    "boots" => "boots_id",
    "trophy_one" => "trophy_one_id",
    "trophy_two" => "trophy_two_id",
    "trophy_three" => "trophy_three_id",
    "trophy_four" => "trophy_four_id",
    "trophy_five" => "trophy_five_id",
    "trophy_six" => "trophy_six_id",
    "shield" => "shield_id",
  ];

  public function creator()
  {
    return $this->belongsTo(User::class, "creator_id");
  }

  public function breeds()
  {
    return $this->belongsToMany(Breed::class);
  }

  public function getCodeAttribute()
  {
    return empty($this->id) ? null : IdEncodingService::encode($this->id);
  }

  public function getBreedsIdsAttribute()
  {
    return $this->breeds()->pluck("id");
  }

  public function loadItems()
  {
    $items = Item::whereIn("id", array_filter(array_map(function ($e) {
      return $this[$e];
    }, self::ITEM_FIELDS), function ($e) {
      return !empty($e);
    }))->with("effects")->get();

    $columns = $items->pluck("id");

    foreach (self::ITEM_FIELDS as $relation => $field) {
      if (($key = array_search($this[$field], $columns->toArray())) !== false) {
        $this[$relation] = $items[$key];
      }
    }

    return $this;
  }
}
