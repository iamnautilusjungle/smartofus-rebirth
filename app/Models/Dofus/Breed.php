<?php

namespace App\Models\Dofus;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Breed extends Model
{
  protected $table = "dofus_breeds";

  protected $hidden = [
    'short_name_translations',
  ];

  use HasTranslations;

  protected $fillable = [
    "id",
    "short_name_id"
  ];

  public $translatable = ['short_name_translations'];
}
