<?php

namespace App\Models\Dofus;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class ItemSet extends Model
{
  protected $table = "dofus_item_sets";

  protected $hidden = [
    'name_translations',
  ];

  use HasTranslations;

  public $translatable = ['name_translations'];

  protected $fillable = [
    "id",
    "name_id",
  ];
}
