<?php

namespace App\Models\Dofus;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class ItemType extends Model
{
  protected $table = "dofus_item_types";

  protected $fillable = [
    "id",
    "craft_xp_ratio",
    "name_id",
  ];

  protected $hidden = [
    'name_translations',
    'craft_xp_ratio',
  ];

  use HasTranslations;

  public $translatable = ['name_translations'];

  // Equipments
  const EQUIPMENT = "Equipment";

  const EQUIPMENTS = [
    self::AMULET,
    self::BACKPACK,
    self::BELT,
    self::BOOTS,
    self::CLOAK,
    self::DOFUS,
    self::HAT,
    self::LIVING_OBJECT,
    self::RING,
    self::SHIELD,
    self::TROPHY,
  ];

  const AMULET = 1;
  const BACKPACK = 81;
  const BELT = 10;
  const BOOTS = 11;
  const CLOAK = 17;
  const DOFUS = 23;
  const HAT = 16;
  const LIVING_OBJECT = 113;
  const RING = 9;
  const SHIELD = 82;
  const TROPHY = 151;

  // Weapons
  const WEAPON = "Weapon";

  const WEAPONS = [
    self::AXE,
    self::BOW,
    self::HAMMER,
    self::PICKAXE,
    self::SCYTHE,
    self::SHOVEL,
    self::SOUL_STONE,
    self::STAFF,
    self::SWORD,
    self::TOOL,
    self::WAND,
  ];

  const AXE = 19;
  const BOW = 2;
  const DAGGER = 5;
  const HAMMER = 7;
  const PICKAXE = 21;
  const SCYTHE = 22;
  const SHOVEL = 8;
  const SOUL_STONE = 83;
  const STAFF = 4;
  const SWORD = 6;
  const TOOL = 20;
  const WAND = 3;

  // Consumables
  const CONSUMABLE = "Consumable";

  const CONSUMABLES = [];

  // Resources
  const RESOURCE = "Resource";

  const RESOURCES = [];

  // Categories
  const CATEGORIES = [
    self::EQUIPMENT => self::EQUIPMENTS,
    self::WEAPON => self::WEAPONS,
    self::RESOURCE => self::RESOURCES,
    self::CONSUMABLE => self::CONSUMABLES,
  ];

  // Pets
  const PETS = [
    self::PET,
    self::MOUNT
  ];

  const PET = 1;
  const MOUNT = 2;

  const TROPHUS = [
    self::TROPHY,
    self::DOFUS
  ];

  public static function getCategoryNameFromType($id)
  {
    foreach (self::CATEGORIES as $key => $category) {
      if (in_array($id, $category))
        return $key;
    }
  }
}
