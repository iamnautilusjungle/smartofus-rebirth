<?php

namespace App\Models\Dofus;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Job extends Model
{
  protected $table = "dofus_jobs";

  use HasFactory;

  use HasTranslations;

  public $translatable = ['name_translations'];

  protected $hidden = [
    'name_translations',
  ];

  const MAGUSES = [
    44,
    48,
    62,
    63,
    64,
    74,
  ];

  const COLLECTERS = [
    2,
    24,
    26,
    28,
    36,
    41,
  ];

  const CRAFTERS = [
    60,
    13,
    65,
    16,
    15,
    11,
    27,
  ];

  const COLLECTER = 1;
  const CRAFTER = 2;
  const MAGUS = 3;

  protected $fillable = [
    "id",
    "name_id",
  ];

  public function getJobTypeAttribute()
  {
    if (in_array($this->id, static::COLLECTERS)) {
      return static::COLLECTER;
    } else if (in_array($this->id, static::CRAFTERS)) {
      return static::CRAFTER;
    } else if (in_array($this->id, static::MAGUSES)) {
      return static::MAGUS;
    }
  }

  public function crafts()
  {
    return $this->hasMany(Item::class, "crafter_id");
  }

  public function type()
  {
    return $this->belongsTo(ItemType::class, "type_id");
  }

  public function scopeGoodJobs($query)
  {
    return $query->whereIn('id', array_merge(self::MAGUSES, self::CRAFTERS, self::COLLECTERS));
  }

  public function scopeMaguses($query)
  {
    return $query->whereIn('id', self::MAGUSES);
  }

  public function scopeCrafters($query)
  {
    return $query->whereIn('id', self::CRAFTERS);
  }

  public function scopeCollecters($query)
  {
    return $query->whereIn('id', self::COLLECTERS);
  }

  public function scopeNotMaguses($query)
  {
    return $query->whereNotIn('id', self::MAGUSES);
  }

  public function getCraftXpAttribute()
  {
    $baseXp = $this->level * 20;
    if ($this->craft_xp_ratio == -1) { // -1 means inheritance
      if ($this->type && $this->type->craft_xp_ratio == -1) {
        return $baseXp;
      } else {
        return $baseXp * ($this->type->craft_xp_ratio / 100);
      }
    } else {
      return $baseXp * ($this->craft_xp_ratio / 100);
    }
  }
}
