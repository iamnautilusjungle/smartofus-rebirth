<?php

namespace App\Models\Dofus;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Spell extends Model
{
  use HasFactory;
  use HasTranslations;

  public $timestamps = false;

  protected $table = "dofus_spells";

  public $translatable = ['name_translations', 'description_translations'];

  protected $hidden = [
    "name_id",
    'name_translations',
    "description_id",
    'description_translations',
  ];

  protected $appends = [
    "name",
    "description",
  ];

  protected $fillable = [
    "id",
    "name_id",
    "description_id"
  ];
}
