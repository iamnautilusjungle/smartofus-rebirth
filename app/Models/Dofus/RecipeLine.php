<?php

namespace App\Models\Dofus;

use Illuminate\Database\Eloquent\Model;

class RecipeLine extends Model
{
  protected $table = "dofus_recipe_lines";

  protected $fillable = [
    "crafted_id",
    "needed_id",
    "quantity"
  ];

  public function crafted()
  {
    return $this->belongsTo(Item::class, "crafted_id");
  }

  public function needed()
  {
    return $this->belongsTo(Item::class, "needed_id");
  }
}
