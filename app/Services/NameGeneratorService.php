<?php

namespace App\Services;

class NameGeneratorService
{
    public static function generate(): string
    {
        return "Tofu#" . rand(pow(10, 4 - 1), pow(10, 4) - 1);
    }
}
