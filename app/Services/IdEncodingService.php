<?php

namespace App\Services;

use Hashids\Hashids;

class IdEncodingService
{
    private static $CODER;

    private static function getCoderInstance(): Hashids
    {
        if (!isset(self::$CODER)) {
            self::$CODER = new Hashids(config("smartofus.encoding_salt"));
        }

        return self::$CODER;
    }

    public static function encode(int $id)
    {
        return self::getCoderInstance()->encode($id);
    }

    public static function decode($id)
    {
        return self::getCoderInstance()->decode($id);
    }
}
