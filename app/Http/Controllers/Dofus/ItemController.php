<?php

namespace App\Http\Controllers\Dofus;

use App\Facades\ItemFacade;
use App\Http\Controllers\Controller;
use App\Models\Dofus\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
  public function index(Request $request)
  {
    $limit = $request->limit ?? 20;

    $query = Item::query();
    $query->select("id", "level", "name_id", "set_id", "type_id", "critical_hit_bonus");
    $query->with(["effects:id,name_id", "type:id,name_id", "set:id,name_id"]);
    $query->whereIn("type_id", $request->types);
    $query->orderBy("level", "DESC");
    $query->orderBy("id", "DESC");

    return $query->paginate($limit);
  }
}
