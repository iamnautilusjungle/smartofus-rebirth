<?php

namespace App\Http\Controllers\Dofus;

use App\Http\Controllers\Controller;
use App\Models\Dofus\Stuff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class StuffController extends Controller
{
  public function myStuffs()
  {
    Gate::authorize('myStuffs', Stuff::class);

    return Stuff::where("creator_id", "=", Auth::id())->get()->append(["breeds_ids"]);
  }

  public function create()
  {
    $stuff = new Stuff();

    $stuff->creator()->associate(Auth::user());
    $stuff->level = 200;
    $stuff->created_at = now();
    $stuff->updated_at = now();
    $stuff->name ??= "New";

    return $stuff;
  }

  public function store(Request $request)
  {
    Gate::authorize('store', Stuff::class);

    $stuff = new Stuff();

    $stuff->fill($request->stuff);
    $stuff->creator()->associate(Auth::user());
    $stuff->save();

    return $stuff;
  }

  public function show(Request $request, Stuff $stuff)
  {
    Gate::authorize('show', $stuff);

    $stuff->loadItems();
    $stuff->load(["creator:id,name,title_id", "creator.title:id,name"]);

    return $stuff;
  }

  public function edit(Request $request, Stuff $stuff)
  {
    Gate::authorize('edit', $stuff);

    $stuff->loadItems();
    $stuff->load(["creator"]);
    $stuff->append(["breeds_ids"]);
    $stuff->creator->withTitle();

    return $stuff;
  }

  public function update(Request $request, Stuff $stuff)
  {
    Gate::authorize('update', $stuff);

    $stuff->fill($request->stuff);
    $stuff->breeds()->sync($request->stuff['breeds_ids']);
    $stuff->save();

    $stuff->breeds_ids = $request->stuff['breeds_ids'];

    return $stuff;
  }

  public function destroy(Request $request, Stuff $stuff)
  {
    Gate::authorize('destroy', $stuff);

    $stuff->delete();
  }
}
