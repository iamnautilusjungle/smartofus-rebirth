<?php

namespace App\Http\Controllers\Dofus;

use App\Http\Controllers\Controller;
use App\Models\Dofus\Job;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function jobs()
    {
        return Job::goodJobs()->notMaguses()->get()->each->append("job_type");
    }

    public function job(Job $job)
    {
        return tap($job
            ->load(
                [
                    "crafts" => function ($query) {
                        $query->select("id", "name_id", "crafter_id", "level", "type_id", "craft_xp_ratio")->orderBy("level", "ASC");
                    },
                    "crafts.recipe" => function ($query) {
                        $query->select("crafted_id", "needed_id", "quantity");
                    },
                    "crafts.recipe.needed" => function ($query) {
                        $query->select("id", "name_id");
                    },
                ]
            ), function ($job) {
            return $job->crafts->each->append("craft_xp");
        });
    }
}
