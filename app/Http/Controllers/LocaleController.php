<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LocaleController extends Controller
{
    public function add(Request $request, $locale, $file)
    {
        $toStr = implode(",", $request->all());
        Log::warning("Missing key for $file ($locale): $toStr");
    }
}
