<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class SpaController extends Controller
{
    public function user()
    {
        $user = Auth::user();
        if (!empty($user)) {
            $user->load("title:id,name");
        }
        return $user;
    }
}
