<?php

namespace App\Http\Controllers\Auth\Socialite;

class GoogleController extends BaseController
{
    static $type = "google";
}
