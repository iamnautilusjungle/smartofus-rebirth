<?php

namespace App\Http\Controllers\Auth\Socialite;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Services\NameGeneratorService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BaseController extends Controller
{
    use AuthenticatesUsers;

    static $type;

    public function login(Request $request)
    {
        $this->validateLogin($request);

        $soUser = Socialite::driver(static::$type)->userFromToken($request->token);

        if (is_null($soUser)) {
            throw new NotFoundHttpException("We couldn't find you in " . static::$type);
        }

        $user = User::where("email", "=", $soUser->getEmail())->first();

        if (is_null($user)) {
            $user = $this->register($soUser);
        };

        Auth::login($user);

        return $user->withTitle();
    }

    public function register($soUser)
    {
        return User::create([
            "name" => NameGeneratorService::generate(),
            "email" => $soUser->getEmail(),
            "email_verified_at" => now()
        ]);
    }

    protected function validateLogin(Request $request)
    {
        $request->validate([
            'token' => 'required|string',
        ]);
    }
}
