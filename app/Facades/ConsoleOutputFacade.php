<?php

namespace App\Facades;

class ConsoleOutputFacade
{
    static $consoleOutputInstance;

    public static function getInstance()
    {
        self::$consoleOutputInstance = self::$consoleOutputInstance ?? new \Symfony\Component\Console\Output\ConsoleOutput();
        return self::$consoleOutputInstance;
    }

    public static function info(String $str): void
    {
        self::getInstance()->writeln($str);
    }
}
