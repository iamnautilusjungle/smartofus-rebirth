<?php

namespace App\Facades;

use App\Models\Dofus\ItemType;

class ItemFacade
{
    const AMULET_ID = 'amulet_id';
    const RING_ONE_ID = 'ring_one_id';
    const RING_TWO_ID = 'ring_two_id';
    const WEAPON_ID = 'weapon_id';
    const SHIELD_ID = 'shield_id';
    const HAT_ID = 'hat_id';
    const CLOAK_ID = 'cloak_id';
    const BELT_ID = 'belt_id';
    const BOOTS_ID = 'boots_id';
    const PET_ID = 'pet_id';
    const TROPHY_ONE_ID = 'trophy_one_id';
    const TROPHY_TWO_ID = 'trophy_two_id';
    const TROPHY_THREE_ID = 'trophy_three_id';
    const TROPHY_FOUR_ID = 'trophy_four_id';
    const TROPHY_FIVE_ID = 'trophy_five_id';
    const TROPHY_SIX_ID = 'trophy_six_id';

    const TYPES_CONVERSION_ARRAY = [
        self::AMULET_ID => ItemType::AMULET,
        self::RING_ONE_ID => ItemType::RING,
        self::RING_TWO_ID => ItemType::RING,
        self::WEAPON_ID => ItemType::WEAPONS,
        self::SHIELD_ID => ItemType::SHIELD,
        self::HAT_ID => ItemType::HAT,
        self::CLOAK_ID => ItemType::CLOAK,
        self::BELT_ID => ItemType::BELT,
        self::BOOTS_ID => ItemType::BOOTS,
        self::PET_ID => ItemType::PETS,
        self::TROPHY_ONE_ID => ItemType::TROPHUS,
        self::TROPHY_TWO_ID => ItemType::TROPHUS,
        self::TROPHY_THREE_ID => ItemType::TROPHUS,
        self::TROPHY_FOUR_ID => ItemType::TROPHUS,
        self::TROPHY_FIVE_ID => ItemType::TROPHUS,
        self::TROPHY_SIX_ID => ItemType::TROPHUS,
    ];

    public static function itemStringTypeToTypeIds(String $itemStringType)
    {
        if (array_key_exists($itemStringType, self::TYPES_CONVERSION_ARRAY)) {
            $data = self::TYPES_CONVERSION_ARRAY[$itemStringType];
            return is_array($data) ? $data : [$data];
        }
        return [];
    }
}
