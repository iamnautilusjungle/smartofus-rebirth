<?php

namespace App\Feeders;

use App\Facades\ConsoleOutputFacade;
use App\Models\Dofus\Spell;

class SpellFeeder extends AbstractFeeder
{
  public static function getModel()
  {
    return Spell::class;
  }

  public static function getFileName()
  {
    return "Spells.json";
  }

  public static function getTextFields()
  {
    return ["name_id", "description_id"];
  }

  public static function getTextMapping()
  {
    return [
      "name_id" => "name_translations",
      "description_id" => "description_translations",
    ];
  }

  public static function feed(): void
  {
    $spells = \JsonMachine\JsonMachine::fromFile(static::getFile());
    foreach ($spells as $spellJson) {
      ConsoleOutputFacade::info("Creating spell " . $spellJson['id']);
      $spell = Spell::updateOrCreate([
        "id" => $spellJson['id']
      ], [
        "id" => $spellJson['id'],
        "name_id" => $spellJson['nameId'],
        "description_id" => $spellJson['descriptionId'],
      ]);
      // $i = 1;
      // $spell->levels()->detach();
      // foreach ($spellJson['spellLevels'] as $spellLevel) {
      //   $spell->levels()->attach($spellLevel, ["spell_id" => $spellJson['id'], "level" => $i++]);
      // }
    }
  }
}
