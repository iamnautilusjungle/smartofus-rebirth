<?php

namespace App\Feeders;

use App\Facades\ConsoleOutputFacade;
use App\Models\Dofus\Item;

class ItemFeeder extends AbstractFeeder
{
  public static function getModel()
  {
    return Item::class;
  }

  public static function getFileName()
  {
    return "Items.json";
  }

  public static function getTextFields()
  {
    return ["name_id"];
  }

  public static function getTextMapping()
  {
    return [
      "name_id" => "name_translations"
    ];
  }

  public static function feed(): void
  {
    $items = \JsonMachine\JsonMachine::fromFile(static::getFile());
    foreach ($items as $item) {
      ConsoleOutputFacade::info("Updating item " . $item['id']);
      $newItem = Item::updateOrCreate([
        "id" => $item["id"],
      ], [
        "craft_xp_ratio" => $item["craftXpRatio"],
        "level" => $item["level"],
        "set_id" => $item["itemSetId"] == -1 ? null : $item["itemSetId"],
        "type_id" => $item["typeId"],
        "name_id" => $item["nameId"],
        "critical_hit_bonus" => $item["criticalHitBonus"] ?? null,
      ]);
      $newItem->effects()->detach();
      foreach ($item["possibleEffects"] as $effect) {
        ConsoleOutputFacade::info("Updating effect " . $effect['effectId'] . " for item " . $item['id']);
        $newItem->effects()->attach($effect["effectId"], [
          "item_id" => $item["id"],
          "dice_num" => $effect["diceNum"],
          "dice_side" => $effect["diceSide"],
          "value" => $effect["value"],
        ]);
      }
    };
  }
}
