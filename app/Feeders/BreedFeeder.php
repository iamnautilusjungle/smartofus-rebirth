<?php

namespace App\Feeders;

use App\Facades\ConsoleOutputFacade;
use App\Models\Dofus\Breed;

class BreedFeeder extends AbstractFeeder
{
    public static function getModel()
    {
        return Breed::class;
    }

    public static function getFileName()
    {
        return "Breeds.json";
    }

    public static function getTextFields()
    {
        return ["short_name_id"];
    }

    public static function getTextMapping()
    {
        return [
            "short_name_id" => "short_name_translations"
        ];
    }

    public static function feed(): void
    {
        $breeds = \JsonMachine\JsonMachine::fromFile(static::getFile());
        foreach ($breeds as $breedJson) {
            ConsoleOutputFacade::info("Updating breed " . $breedJson['id']);
            Breed::updateOrCreate([
                "id" => $breedJson['id']
            ], [
                "id" => $breedJson['id'],
                "short_name_id" => $breedJson['shortNameId'],
            ]);
        }
    }
}
