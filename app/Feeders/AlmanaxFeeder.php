<?php

namespace App\Feeders;

use App\Models\Almanax;
use App\Models\AlmanaxBonusCategory;
use App\Models\AlmanaxBonusType;
use Illuminate\Support\Facades\Storage;

class AlmanaxFeeder extends AbstractFeeder
{
    public static function getModel()
    {
        return Almanax::class;
    }

    public static function getFileName()
    {
        return "almanax.json";
    }

    public static function feed(): void
    {
        $contents = Storage::disk("local")->get(static::getFile());
        $json = json_decode($contents);
        foreach ($json as $day) {
            $almanax = Almanax::firstOrNew([
                "day" => $day->day,
                "month" => $day->month,
            ]);

            $almanax->offering_id = $day->offering_id;
            $almanax->quantity = $day->quantity;
            $almanax->kamas = $day->kamas;

            $almanax->setTranslation("bonus_value_translations", "fr", $day->bonus_value);

            $bonusType = $day->bonus_type->name;

            // Bonus Type
            $almanaxBonusType = AlmanaxBonusType::updateOrCreate([
                "name_translations->fr" => $bonusType
            ]);

            $almanaxBonusType->setTranslation("name_translations", "fr", $bonusType);

            $categories = collect($day->bonus_type->categories);

            foreach ($categories as $category) {
                $cat = AlmanaxBonusCategory::updateOrCreate([
                    "id" => $category->id
                ]);
                $cat->setTranslation("name_translations", "fr", $category->name);
            }

            $almanaxBonusType->save();

            $almanaxBonusType->categories()->sync($categories->pluck("id"));

            $almanax->bonusType()->associate($almanaxBonusType);

            $almanax->save();
        }
    }
}
