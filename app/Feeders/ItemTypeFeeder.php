<?php

namespace App\Feeders;

use App\Facades\ConsoleOutputFacade;
use App\Models\Dofus\ItemType;

class ItemTypeFeeder extends AbstractFeeder
{
    public static function getModel()
    {
        return ItemType::class;
    }

    public static function getFileName()
    {
        return "ItemTypes.json";
    }

    public static function getTextFields()
    {
        return ["name_id"];
    }

    public static function getTextMapping()
    {
        return [
            "name_id" => "name_translations"
        ];
    }
    public static function feed(): void
    {
        $types = \JsonMachine\JsonMachine::fromFile(static::getFile());
        foreach ($types as $type) {
            ConsoleOutputFacade::info("Updating item type" . $type['id']);
            ItemType::updateOrCreate([
                "id" => $type["id"],
            ], [
                "craft_xp_ratio" => $type["craftXpRatio"],
                "name_id" => $type["nameId"],
            ]);
        }
    }
}
