<?php

namespace App\Feeders;

use App\Models\Dofus\Characteristic;
use App\Facades\ConsoleOutputFacade;

class CharacteristicFeeder extends AbstractFeeder
{
    public static function getModel()
    {
        return Characteristic::class;
    }

    public static function getFileName()
    {
        return "Characteristics.json";
    }

    public static function getTextFields()
    {
        return ["name_id"];
    }

    public static function getTextMapping()
    {
        return [
            "name_id" => "name_translations"
        ];
    }

    public static function feed(): void
    {
        $chars = \JsonMachine\JsonMachine::fromFile(static::getFile());
        foreach ($chars as $charJson) {
            ConsoleOutputFacade::info("Updating characteristic " . $charJson['id']);
            Characteristic::updateOrCreate([
                "id" => $charJson['id']
            ], [
                "id" => $charJson['id'],
                "name_id" => $charJson['nameId'],
            ]);
        }
    }
}
