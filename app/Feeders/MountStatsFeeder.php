<?php

namespace App\Feeders;

use App\Models\Dofus\Item;
use App\Facades\ConsoleOutputFacade;
use App\Models\Dofus\Effect;

class MountStatsFeeder extends AbstractFeeder
{
    public static function getModel()
    {
        // return Effect::class;
    }

    public static function getFileName()
    {
        return "Mounts.json";
    }

    public static function getTextFields()
    {
        return [];
    }

    public static function getTextMapping()
    {
        return [];
    }

    public static function feed(): void
    {
        $mounts = \JsonMachine\JsonMachine::fromFile(static::getFile());
        foreach ($mounts as $mountJson) {
            ConsoleOutputFacade::info("Updating mount " . $mountJson['id']);
            $item = Item::find($mountJson['certificateId']);
            $item->effects()->detach();
            foreach ($mountJson["effects"] as $effect) {
                ConsoleOutputFacade::info("Updating effect " . $effect['effectId'] . " for item " . $mountJson['id']);
                $item->effects()->attach($effect["effectId"], [
                    "item_id" => $item["id"],
                    "dice_num" => $effect["diceNum"],
                    "dice_side" => $effect["diceSide"],
                    "value" => $effect["value"],
                ]);
            }
        }
    }
}
