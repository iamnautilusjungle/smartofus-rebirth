<?php

namespace App\Feeders;

use App\Facades\ConsoleOutputFacade;
use App\Models\Dofus\ItemSet;

class ItemSetFeeder extends AbstractFeeder
{
    public static function getModel()
    {
        return ItemSet::class;
    }

    public static function getFileName()
    {
        return "ItemSets.json";
    }

    public static function getTextFields()
    {
        return ["name_id"];
    }

    public static function getTextMapping()
    {
        return [
            "name_id" => "name_translations"
        ];
    }

    public static function feed(): void
    {
        $sets = \JsonMachine\JsonMachine::fromFile(static::getFile());
        foreach ($sets as $set) {
            ConsoleOutputFacade::info("Updating set " . $set['id']);
            ItemSet::updateOrCreate([
                "id" => $set["id"],
            ], [
                "name_id" => $set["nameId"],
            ]);
        };
    }
}
