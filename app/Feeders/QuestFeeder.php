<?php

namespace App\Feeders;

use App\Facades\ConsoleOutputFacade;
use App\Models\ItemType;
use App\Models\Quest;

class QuestFeeder extends AbstractFeeder
{
    public static function getModel()
    {
        return Quest::class;
    }

    public static function getFileName()
    {
        return "Quests.json";
    }

    public static function feed(): void
    {
        $quests = \JsonMachine\JsonMachine::fromFile(static::getFile());
        foreach ($quests as $questJson) {
            ConsoleOutputFacade::info("Updating quest " . $questJson['id']);
            Quest::updateOrCreate([
                "id" => $questJson['id']
            ], [
                "id" => $questJson['id'],
                "name_id" => $questJson['nameId']
            ]);
        }
    }
}
