<?php

namespace App\Feeders;

use App\Facades\ConsoleOutputFacade;
use App\Models\Dofus\Item;
use App\Models\Dofus\RecipeLine;

class RecipeFeeder extends AbstractFeeder
{
    public static function getModel()
    {
        return RecipeLine::class;
    }

    public static function getFileName()
    {
        return "Recipes.json";
    }

    public static function feed(): void
    {
        $recipes = \JsonMachine\JsonMachine::fromFile(static::getFile());
        foreach ($recipes as $recipe) {
            ["ingredientIds" => $ingredientIds, "quantities" => $quantities, "jobId" => $jobId, "resultId" => $resultId] = $recipe;

            ConsoleOutputFacade::info("Updating crafter for item " . $resultId);

            // Update job crafter id
            Item::where("id", "=", $resultId)->update([
                "crafter_id" => $jobId
            ]);

            // Update or create recipe
            foreach ($ingredientIds as $key => $ingredientId) {
                ConsoleOutputFacade::info("Updating recipe line $ingredientId for item $resultId");
                RecipeLine::updateOrCreate([
                    "crafted_id" => $resultId,
                    "needed_id" => $ingredientId,
                    "quantity" => $quantities[$key]
                ]);
            }
        }
    }
}
