<?php

namespace App\Feeders;

use App\Models\Dofus\Effect;
use App\Facades\ConsoleOutputFacade;

class EffectFeeder extends AbstractFeeder
{
    public static function getModel()
    {
        return Effect::class;
    }

    public static function getFileName()
    {
        return "Effects.json";
    }

    public static function getTextFields()
    {
        return ["name_id"];
    }

    public static function getTextMapping()
    {
        return [
            "name_id" => "name_translations"
        ];
    }

    public static function feed(): void
    {
        $effects = \JsonMachine\JsonMachine::fromFile(static::getFile());
        foreach ($effects as $effectJson) {
            ConsoleOutputFacade::info("Updating effect " . $effectJson['id']);
            Effect::updateOrCreate([
                "id" => $effectJson['id']
            ], [
                "id" => $effectJson['id'],
                "name_id" => $effectJson['descriptionId'],
            ]);
        }
    }
}
