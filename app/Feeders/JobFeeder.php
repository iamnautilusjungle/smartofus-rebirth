<?php

namespace App\Feeders;

use App\Facades\ConsoleOutputFacade;
use App\Models\Dofus\Job;
use Illuminate\Support\Facades\Storage;

class JobFeeder extends AbstractFeeder
{
    public static function getModel()
    {
        return Job::class;
    }

    public static function getFileName()
    {
        return "Jobs.json";
    }

    public static function getTextFields()
    {
        return ["name_id"];
    }

    public static function getTextMapping()
    {
        return [
            "name_id" => "name_translations"
        ];
    }

    public static function feed(): void
    {
        $jobs = \JsonMachine\JsonMachine::fromFile(static::getFile());
        foreach ($jobs as $jobJson) {
            ConsoleOutputFacade::info("Updating job " . $jobJson['id']);
            Job::updateOrCreate([
                "id" => $jobJson['id']
            ], [
                "id" => $jobJson['id'],
                "name_id" => $jobJson['nameId'],
            ]);
        }
    }
}
