<?php

namespace App\Feeders;

use App\Facades\ConsoleOutputFacade;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

abstract class AbstractFeeder
{
    const CORRUPTED_STRING = "CORRUPTED - PLEASE REPORT THIS";

    const FILE_NAME = "/private/translators/i18n_%lang%.json";

    const LIST = [
        self::ITEMS,
        self::ITEM_TYPES,
        self::RECIPES,
        self::ALMANAX,
        self::JOBS,
        self::CHARACTERISTICS,
        self::QUESTS,
        self::EFFECTS,
        self::SPELLS,
        self::MOUNTS_STATS,
        self::SETS,
        self::BREEDS,
    ];

    const ITEMS = "Items (" . self::ITEMS_FILE . ")";
    const ITEM_TYPES = "Items Types (" . self::ITEM_TYPES_FILE . ")";
    const RECIPES = "Recipes (" . self::RECIPES_FILE . ")";
    const ALMANAX = "Almanax (" . self::ALMANAX_FILE . ")";
    const JOBS = "Jobs (" . self::JOBS_FILE . ")";
    const CHARACTERISTICS = "Characteristics (" . self::CHARACTERISTICS_FILE . ")";
    const QUESTS = "Quests (" . self::QUESTS_FILE . ")";
    const EFFECTS = "Effects (" . self::EFFECTS_FILE . ")";
    const SPELLS = "Spells (" . self::SPELLS_FILE . ")";
    const MOUNTS_STATS = "MountsStats (" . self::MOUNTS_FILE . ")";
    const SETS = "Sets (" . self::SETS_FILE . ")";
    const BREEDS = "Breeds (" . self::BREEDS_FILE . ")";

    const ITEMS_FILE = "Items.json";
    const ITEM_TYPES_FILE = "ItemTypes.json";
    const RECIPES_FILE = "Recipes.json";
    const ALMANAX_FILE = "almanax.json";
    const JOBS_FILE = "Jobs.json";
    const CHARACTERISTICS_FILE = "Characteristics.json";
    const QUESTS_FILE = "Quests.json";
    const EFFECTS_FILE = "Effects.json";
    const SPELLS_FILE = "Spells.json";
    const MOUNTS_FILE = "Mounts.json";
    const SETS_FILE = "ItemSets.json";
    const BREEDS_FILE = "Breeds.json";

    const FEEDERS = [
        self::ITEMS => ItemFeeder::class,
        self::ITEM_TYPES => ItemTypeFeeder::class,
        self::RECIPES => RecipeFeeder::class,
        self::ALMANAX => AlmanaxFeeder::class,
        self::JOBS => JobFeeder::class,
        self::CHARACTERISTICS => CharacteristicFeeder::class,
        self::QUESTS => QuestFeeder::class,
        self::EFFECTS => EffectFeeder::class,
        self::SPELLS => SpellFeeder::class,
        self::MOUNTS_STATS => MountStatsFeeder::class,
        self::SETS => ItemSetFeeder::class,
        self::BREEDS => BreedFeeder::class,
    ];

    public static function getTranslationArray($lang)
    {
        return static::getContents($lang);
    }

    public static  function getContents($lang)
    {
        return (array) json_decode(static::getTranslationFile($lang));
    }

    public static  function getTranslationFile($lang)
    {
        return file_get_contents(storage_path("app" . str_replace("%lang%", $lang, static::FILE_NAME)));
    }

    public static function getRoot()
    {
        return "/private/feeders/";
    }

    abstract public static function getFileName();

    abstract public static function getModel();

    abstract public static function feed(): void;

    public static function getFile()
    {
        return storage_path("app/" . static::getRoot() . static::getFileName());
    }

    public static function getTextFields()
    {
        return [];
    }

    public static function getTextMapping()
    {
        return [];
    }

    public static function getList()
    {
        // Complete list
        $list = array_merge(["All"], AbstractFeeder::LIST);

        // jsons in storage/private/feeders
        $jsons = array_map(function ($x) {
            return basename($x);
        }, Storage::files("/private/feeders"));

        // Rename not found files
        $list = array_map(function ($e) use ($jsons) {
            preg_match('#\((.*?)\)#', $e, $match);
            if (isset($match[1])) {
                if (!in_array($file = $match[1], $jsons)) {
                    $e = substr($e, 0, strlen($e) - 1);
                    return $e . " NOT FOUND)";
                }
            }
            return $e;
        }, $list);

        return $list;
    }

    public static function locales($models, $langs): void
    {
        foreach ($langs as $lang) {
            $content = static::getContents($lang);
            foreach ($models as $model) {
                if (!empty($model::getTextFields())) {
                    static::localeOne($model, $lang, $content);
                }
            }
        }
    }

    public static function translate($models, $langs): void
    {
        $contents = [];
        foreach ($langs as $lang) {
            $contents[$lang] = static::getContents($lang);
        }

        foreach ($models as $model) {
            static::translateOne($model, $langs, $contents);
        }
    }

    public static function translateOne($model, $langs, $contents): void
    {
        foreach ($model::getModel()::all() as $object) {
            foreach ($langs as $lang) {
                foreach ($model::getTextFields() as $field) {
                    $object->setTranslation($model::getTextMapping()[$field], $lang, $contents[$lang][$object->$field] ?? "NO STRING FOUND");
                }
            }
            $object->save();
        }
    }

    public static function localeOne($model, $lang, $content)
    {
        $data = $model::getModel()::select($model::getTextFields())->get();

        $ids = collect();

        foreach ($model::getTextFields() as $field) {
            $ids = $ids->merge($data->pluck($field));
        }

        $ids = $ids->toArray();
        $explode = explode("\\", $model::getModel());
        $explode = $explode[count($explode) - 2] . $explode[count($explode) - 1];

        File::put(public_path("/locales/$lang/$explode.json"), json_encode(array_filter($content, function ($k) use ($ids) {
            return in_array($k, $ids);
        }, ARRAY_FILTER_USE_KEY)));
    }
}
