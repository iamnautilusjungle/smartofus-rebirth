<?php

namespace App\Policies\Dofus;

use App\Models\Dofus\Stuff;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StuffPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function myStuffs()
    {
        return true;
    }

    public function index()
    {
        return true;
    }

    public function create()
    {
        return true;
    }

    public function store()
    {
        return true;
    }

    public function show()
    {
        return true;
    }

    public function edit(User $user, Stuff $stuff)
    {
        return $user->id == $stuff->creator_id;
    }

    public function update(User $user, Stuff $stuff)
    {
        return $user->id == $stuff->creator_id;
    }

    public function destroy(User $user, Stuff $stuff)
    {
        return $user->id == $stuff->creator_id;
    }
}
