import './i18n';

import React from 'react';
import { render } from 'react-dom';
import { RecoilRoot } from 'recoil';
import App from './components/App/App';

require('./bootstrap');

render((
	<RecoilRoot>
		<App />
	</RecoilRoot>
),
document.getElementById('app'));
