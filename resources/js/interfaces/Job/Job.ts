export interface Job {
	id: number;
	name_id: number;
	job_type: JobType;
}

export enum JobType {
	COLLECTER = 1,
	CRAFTER = 2,
	MAGUS = 3,
}
