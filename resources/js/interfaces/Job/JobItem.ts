export interface JobItem {
	id: number;
	name_id: number;
	recipe: RecipeLine[];
	level: number;
	craft_xp: number;
}

export interface RecipeLine {
	crafted_id: number;
	needed_id: number;
	quantity: number;
	needed: JobItem;
}

export interface Craft {
	item: JobItem;
	quantity: number;
	xp_gained: number;
	xp_total: number;
}

export interface ResourceLine {
	item: JobItem;
	quantity: number;
}
