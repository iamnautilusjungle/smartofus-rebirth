export interface CharacteristicsForDamageCalculation extends SpecificCharacteristicsForDamageCalculation {
	power: number;
	flatDamage: number;
	flatCritDamage: number;
}

export interface SpecificCharacteristicsForDamageCalculation {
	characteristic: number;
	flatCharacteristicDamage: number;
}
