import { StuffItem } from './StuffItem';

export interface EquipmentsOfStuff {
	amulet?: StuffItem;
	ring_one?: StuffItem;
	ring_two?: StuffItem;
	weapon?: StuffItem;
	shield?: StuffItem;
	hat?: StuffItem;
	cloak?: StuffItem;
	belt?: StuffItem;
	boots?: StuffItem;
	pet?: StuffItem;
	trophy_one?: StuffItem;
	trophy_two?: StuffItem;
	trophy_three?: StuffItem;
	trophy_four?: StuffItem;
	trophy_five?: StuffItem;
	trophy_six?: StuffItem;
}

export interface EquipmentsIdsOfStuff {
	amulet_id?: number;
	ring_one_id?: number;
	ring_two_id?: number;
	weapon_id?: number;
	shield_id?: number;
	hat_id?: number;
	cloak_id?: number;
	belt_id?: number;
	boots_id?: number;
	pet_id?: number;
	trophy_one_id?: number;
	trophy_two_id?: number;
	trophy_three_id?: number;
	trophy_four_id?: number;
	trophy_five_id?: number;
	trophy_six_id?: number;
}
