import { Effects } from '../../consts/effects';

export interface Damage {
	min: number;
	max: number;
	min_cs: number | undefined;
	max_cs: number | undefined;
	avg: number;
	type: Effects;
	icon: string;
}
