export interface EffectToCalculate {
	id: number;
	min: number;
	max: number;
	critical_hit_bonus: number | undefined;
}
