import { Set } from './Set';
import { Effect } from '../Effect/Effect';
import { ItemType } from './ItemType';

export interface StuffItem {
	id: number;
	name_id: number;
	set?: Set | undefined;
	level: number;
	type: ItemType;
	effects: Effect[];
	critical_hit_bonus?: number | undefined;
}
