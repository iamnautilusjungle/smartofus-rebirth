import { User } from '../User';
import { Characteristics, Parchments } from '../Effect/Characteristics';
import { EquipmentsIdsOfStuff, EquipmentsOfStuff } from './EquipmentsOfStuff';

export interface CharacteristicsOrParchments extends Characteristics, Parchments {

}

export interface Stuff extends CharacteristicsOrParchments, EquipmentsOfStuff, EquipmentsIdsOfStuff {
	code: string;
	creator_id: number;
	creator?: User | undefined;
	level: number;
	name?: string | undefined;
	is_private?: boolean | undefined;
	breeds_ids?: Array<number> | undefined;
}

export interface StuffEffects extends Map<number, number> {

}
