import { Title } from './Title';

export interface User {
	id: number;
	name: string;
	email: string;
	title?: Title | undefined;
}
