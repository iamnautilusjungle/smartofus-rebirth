export interface Effect {
	id: number;
	name_id: number;
	pivot: EffectPivot
}

export interface EffectPivot {
	item_id: number;
	effect_id: number;
	dice_num: number;
	dice_side: number;
	value: number;
}
