export interface Characteristics {
	vitality_base: number;
	wisdom_base: number;
	strength_base: number;
	intelligence_base: number;
	agility_base: number;
	chance_base: number;
}

export interface Parchments {
	agility_parchment: number;
	chance_parchment: number;
	vitality_parchment: number;
	wisdom_parchment: number;
	strength_parchment: number;
	intelligence_parchment: number;
}
