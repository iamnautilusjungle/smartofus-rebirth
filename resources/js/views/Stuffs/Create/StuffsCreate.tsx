import axios from 'axios';
import { useSnackbar } from 'notistack';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { useRecoilValue } from 'recoil';
import StuffsCreateOrEdit from '../CreateOrEdit/StuffsCreateOrEdit';
import AuthUserAtom from '../../../atoms/AuthUser';
import { Stuff } from '../../../interfaces';
import { StuffBuilder } from '../../../services';

export default function StuffsCreate() {
	const { enqueueSnackbar } = useSnackbar();
	const history = useHistory();
	const { t } = useTranslation(['DofusJobsPage', 'DofusJob']);
	const [stuff, setStuff] = useState<Stuff>();

	const auth = useRecoilValue(AuthUserAtom);

	function loadInitialStuff() {
		axios.get('/web-api/dofus/stuffs/create').then((r) => {
			const loadedStuff: Stuff = r.data;
			setStuff(loadedStuff);
		}).catch(() => {
			enqueueSnackbar(
				t('DofusStuffsPage:stuffs.create.error'),
				{ variant: 'error', autoHideDuration: 5000 },
			);
		});
	}

	useEffect(() => {
		loadInitialStuff();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const onSaveClick = () => {
		if (!stuff || !(new StuffBuilder(stuff).hasItems())) {
			enqueueSnackbar(
				t('DofusStuffsPage:saving.cantSaveEmpty'),
				{ variant: 'error', autoHideDuration: 5000 },
			);
			return;
		}
		axios.post('/web-api/dofus/stuffs', {
			stuff,
		}).then((r) => {
			enqueueSnackbar(
				t('DofusStuffsPage:saving.success'),
				{ variant: 'success', autoHideDuration: 5000 },
			);
			history.push(`/stuffs/${r.data.code}/edit`);
		}).catch(() => {
			enqueueSnackbar(
				t('DofusStuffsPage:saving.error'),
				{ variant: 'error', autoHideDuration: 5000 },
			);
		});
	};

	return stuff ? (
		<StuffsCreateOrEdit type="Create" auth={auth} stuff={stuff} setStuff={setStuff} creator={auth} onSaveClick={onSaveClick} />
	) : null;
}
