import React, { useEffect, useState } from 'react';
import {
	Avatar,
	Button,
	ButtonGroup,
	Paper,
	Table, TableBody, TableCell, TableContainer, TableHead, TableRow,
} from '@material-ui/core';
import { useHistory } from 'react-router';
import axios from 'axios';
import { AvatarGroup } from '@material-ui/lab';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import PageContainer from '../../../components/PageContainer/PageContainer';
import { Stuff } from '../../../interfaces';

export default function Stuffs() {
	const history = useHistory();
	// const { t, ready } = useTranslation(['DofusBreed', 'DofusStuffsPage']);

	const [stuffs, setStuffs] = useState<Stuff[]>([]);

	useEffect(() => {
		axios.get('/web-api/dofus/stuffs/my-stuffs').then((r) => {
			setStuffs(r.data);
		});
	}, []);

	// const createNewStuff = () => history.push('/stuffs/create');

	const editStuff = (stuff: Stuff) => history.push(`/stuffs/${stuff.code}/edit`);

	const viewStuff = (stuff: Stuff) => history.push(`/stuffs/${stuff.code}`);

	return (
		<PageContainer>
			<TableContainer component={Paper}>
				<Table size="medium">
					<TableHead>
						<TableRow>
							<TableCell>Stuff</TableCell>
							<TableCell>Characters</TableCell>
							<TableCell>Actions</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{stuffs.map((stuff) => (
							<TableRow key={stuff.code}>
								<TableCell component="th" scope="row">
									{stuff.name}
								</TableCell>
								<TableCell component="th" scope="row">
									<AvatarGroup max={18}>
										{
											stuff?.breeds_ids?.map((breed) => (
												<Avatar alt="breed" src={String(`/images/vignettingMiniCharacter/mini_${breed}_${breed % 2}.png`)} />
											))
										}
									</AvatarGroup>
								</TableCell>
								<TableCell component="th" scope="row">
									<ButtonGroup variant="contained">
										<Button onClick={() => viewStuff(stuff)} aria-label="View">
											<VisibilityIcon />
										</Button>
										<Button onClick={() => editStuff(stuff)} aria-label="Edit">
											<EditIcon />
										</Button>
									</ButtonGroup>
								</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
			</TableContainer>
		</PageContainer>
	);
}
