import React from 'react';
import {
	Box, Divider, Grid,
} from '@material-ui/core';
import TofuCard from '../../../../components/TofuCard/TofuCard';
import StatsDisplayer from '../StatsDisplayer/StatsDisplayer';
import {
	VITALITY,
	WISDOM,
	STRENGTH,
	INTELLIGENCE,
	AGILITY,
	CHANCE,
	POWER,
	DODGE,
	AP_PARRY,
	MP_PARRY,
	PODS,
	LOCK,
	AP_REDUCTION,
	MP_REDUCTION,
	HIT_POINTS,
	PROSPECTING,
	INITIATIVE,
	SUMMONABLE_CREATURES,
	HEAL,
	CRITICAL,
} from '../../../../consts/characteristics';
import CharacteristicsPicker from '../CharacteristicsPicker/CharacteristicsPicker';
import { CharacteristicsOrParchments, Stuff } from '../../../../interfaces';

interface ComponentProps {
	effectsList: Map<number, number>;
	setCharacteristic: (v: number, k: keyof CharacteristicsOrParchments) => void;
	setParchment: (v: number) => void;
	stuff: Stuff;
}

export default function MainStats(props: ComponentProps) {
	return (
		<TofuCard>
			<Grid container>
				<Grid item xs={6}>
					<StatsDisplayer
						statsList={[
							HIT_POINTS,
							PROSPECTING,
						]}
						effectsList={props.effectsList}
					/>
				</Grid>
				<Grid item xs={6}>
					<StatsDisplayer
						statsList={[
							CRITICAL,
							INITIATIVE,
							SUMMONABLE_CREATURES,
							HEAL,
						]}
						effectsList={props.effectsList}
					/>
				</Grid>
			</Grid>
			<Divider />
			<Box mt={1} mb={1}>
				<Grid container>
					<Grid item xs={6}>
						<Grid item xs={12}>Stats</Grid>
						<StatsDisplayer
							statsList={[
								VITALITY,
								WISDOM,
								STRENGTH,
								INTELLIGENCE,
								AGILITY,
								CHANCE,
								POWER,
							]}
							effectsList={props.effectsList}
						/>
					</Grid>
					<Grid item xs={6}>
						<CharacteristicsPicker setParchment={props.setParchment} setCharacteristic={props.setCharacteristic} stuff={props.stuff} />
					</Grid>
				</Grid>
			</Box>
			<Divider />
			<Box mt={1}>
				<Grid container>
					<Grid item xs={6}>
						<StatsDisplayer
							statsList={[
								DODGE,
								AP_PARRY,
								MP_PARRY,
								PODS,
							]}
							effectsList={props.effectsList}
						/>

					</Grid>
					<Grid item xs={6}>
						<StatsDisplayer
							statsList={[
								LOCK,
								AP_REDUCTION,
								MP_REDUCTION,
							]}
							effectsList={props.effectsList}
						/>
					</Grid>
				</Grid>
			</Box>
		</TofuCard>
	);
}
