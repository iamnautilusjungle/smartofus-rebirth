import React from 'react';
import {
	Box, Divider, Grid, Typography,
} from '@material-ui/core';
import TofuCard from '../../../../components/TofuCard/TofuCard';
import StatsDisplayer from '../StatsDisplayer/StatsDisplayer';
import {
	AIR_DAMAGE,
	CRITICAL_DAMAGE,
	EARTH_DAMAGE,
	FIRE_DAMAGE,
	NEUTRAL_DAMAGE,
	WATER_DAMAGE,
	PUSHBACK_DAMAGE,
	PERCENT_WEAPON_DAMAGE,
	PERCENT_SPELL_DAMAGE,
	PERCENT_MELEE_DAMAGE,
	PERCENT_RANGED_DAMAGE, NEUTRAL_RESISTANCE,
	EARTH_RESISTANCE,
	FIRE_RESISTANCE,
	WATER_RESISTANCE,
	AIR_RESISTANCE,
	CRITICAL_RESISTANCE,
	PERCENT_WEAPON_RESISTANCE,
	PERCENT_MELEE_RESISTANCE, PERCENT_NEUTRAL_RESISTANCE,
	PERCENT_EARTH_RESISTANCE,
	PERCENT_FIRE_RESISTANCE,
	PERCENT_WATER_RESISTANCE,
	PERCENT_AIR_RESISTANCE,
	PUSHBACK_RESISTANCE,
	PERCENT_SPELL_RESISTANCE,
	PERCENT_RANGED_RESISTANCE,
} from '../../../../consts/characteristics';
import Profile from './Profile';
import IUser from '../../../../types/IUser';

interface ComponentProps {
	user: IUser | undefined;
	effectsList: Map<number, number>;
}

export default function SecondaryStats(props: ComponentProps) {
	return (
		<Grid container>
			<Box mb={1} style={{ flexGrow: 1 }}>
				<Grid item xs={12}>
					<Grid container>
						<Grid item xs={12}>
							<Profile user={props.user} />
						</Grid>
					</Grid>
				</Grid>
			</Box>
			<Grid item xs={12}>
				<TofuCard>
					<Box mb={1}>
						<Grid container>
							<Grid item xs={12}>
								Dommages
							</Grid>
							<Grid item xs={6}>
								<StatsDisplayer
									statsList={[
										NEUTRAL_DAMAGE,
										EARTH_DAMAGE,
										FIRE_DAMAGE,
										WATER_DAMAGE,
										AIR_DAMAGE,
									]}
									effectsList={props.effectsList}
								/>
							</Grid>
							<Grid item xs={6}>
								<StatsDisplayer
									statsList={[
										CRITICAL_DAMAGE,
										PUSHBACK_DAMAGE,
										PERCENT_WEAPON_DAMAGE,
										PERCENT_SPELL_DAMAGE,
										PERCENT_MELEE_DAMAGE,
										PERCENT_RANGED_DAMAGE,
									]}
									effectsList={props.effectsList}
								/>
							</Grid>
						</Grid>
					</Box>
					<Divider />
					<Box mt={1}>
						<Grid container>
							<Grid item xs={12}>
								Résistances
							</Grid>
							<Grid item xs={6}>
								<StatsDisplayer
									statsList={[
										NEUTRAL_RESISTANCE,
										EARTH_RESISTANCE,
										FIRE_RESISTANCE,
										WATER_RESISTANCE,
										AIR_RESISTANCE,
										CRITICAL_RESISTANCE,
										PERCENT_WEAPON_RESISTANCE,
										PERCENT_MELEE_RESISTANCE,
									]}
									effectsList={props.effectsList}
								/>

							</Grid>
							<Grid item xs={6}>
								<StatsDisplayer
									statsList={[
										PERCENT_NEUTRAL_RESISTANCE,
										PERCENT_EARTH_RESISTANCE,
										PERCENT_FIRE_RESISTANCE,
										PERCENT_WATER_RESISTANCE,
										PERCENT_AIR_RESISTANCE,
										PUSHBACK_RESISTANCE,
										PERCENT_SPELL_RESISTANCE,
										PERCENT_RANGED_RESISTANCE,
									]}
									effectsList={props.effectsList}
								/>
							</Grid>
						</Grid>
					</Box>
				</TofuCard>
			</Grid>
			<Grid item xs={12}>
				<Box mt={1} style={{ flexGrow: 1 }}>
					<Grid item xs={12}>
						<TofuCard>
							<Typography>Pub ici</Typography>
						</TofuCard>
					</Grid>
				</Box>
			</Grid>
		</Grid>
	);
}
