import React from 'react';
import {
	Avatar, Box, Grid, Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import IUser from '../../../../types/IUser';
import TofuCard from '../../../../components/TofuCard/TofuCard';

const useStyles = makeStyles((theme) => ({
	large: {
		width: theme.spacing(7),
		height: theme.spacing(7),
	},
}));

interface ComponentProps {
	user: IUser | undefined
}

export default function Profile(props: ComponentProps) {
	const classes = useStyles();

	return (
		<Grid container>
			<Grid item xs={12}>
				<TofuCard>
					<Box display="flex" bgcolor="background.paper">
						<Box mr={1}>
							<Avatar alt={props.user?.name || 'Déconnecté'} src="/images/users/unknown.png" className={classes.large} />
						</Box>
						<Box flexGrow={1}>
							<Typography variant="h6">{props.user?.name || 'Déconnecté'}</Typography>
							<Typography variant="body1">{props.user ? props.user.title?.name : null}</Typography>
						</Box>
					</Box>
				</TofuCard>
			</Grid>
		</Grid>
	);
}
