import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';
import { StuffItem } from '../../../../interfaces';
import { EquipmentsOfStuff } from '../../../../interfaces/Stuff/EquipmentsOfStuff';

const useStyles = makeStyles(() => createStyles({
	btn: {
		width: '100%',
		paddingTop: '50%' /* 1:1 Aspect Ratio */,
		paddingBottom: '50%' /* 1:1 Aspect Ratio */,
		position: 'relative',
	},
	container: {
		position: 'absolute',
	},
}));

interface ComponentProps {
	type: keyof EquipmentsOfStuff,
	selectedItem: StuffItem | undefined,
	noSelectionImage: number,
	desequipItem: (type: keyof EquipmentsOfStuff) => void;
	openItemsDialog: (type: keyof EquipmentsOfStuff) => void;
}

export default function ItemIcon(props: ComponentProps) {
	const classes = useStyles();

	function handleClick() {
		if (props.selectedItem) {
			props.desequipItem(props.type);
		} else {
			props.openItemsDialog(props.type);
		}
	}

	return (
		<Button
			onClick={handleClick}
			className={classes.btn}
			style={
				props.selectedItem
					? {
						overflow: 'hidden',
						backgroundSize: 'cover',
						backgroundPosition: 'center',
						backgroundImage: `url('/images/items/${String(props.selectedItem.id)}.png')`,
					}
					: {
						overflow: 'hidden',
						backgroundSize: 'cover',
						backgroundPosition: 'center',
						filter: 'grayscale(100%)',
						backgroundImage: `url('/images/items/${String(props.noSelectionImage)}.png')`,
					}
			}
		/>
	);
}
