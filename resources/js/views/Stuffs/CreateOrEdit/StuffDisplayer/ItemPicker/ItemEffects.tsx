import { Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Effect } from '../../../../../interfaces';
import { EffectToString } from '../../../../../services/EffectToString/EffectToString';

interface ComponentProps {
	effects: Effect[];
}

export default function ItemEffects(props: ComponentProps) {
	return (
		<>
			{
				props.effects
					? props.effects.map((effect: Effect) => (
						<ItemEffect effect={effect} />
					))
					: null
			}
		</>
	);
}

interface ItemEffectProps {
	effect: Effect;
}

function ItemEffect(props: ItemEffectProps) {
	const { t, ready } = useTranslation(['DofusEffect']);

	const [defaultEffectString, setDefaultEffectString] = useState('');
	const [effectString, setEffectString] = useState('');

	useEffect(() => {
		setDefaultEffectString(t(`DofusEffect:${props.effect.name_id}`));
	}, [ready]);

	useEffect(() => {
		setEffectString((new EffectToString(props.effect, defaultEffectString).getValue()));
	}, [props.effect, defaultEffectString]);

	return (
		<Typography key={props.effect.id}>
			<img alt={effectString} src="/images/darkStone/Characteristics/icon_cross_characteristics.png" style={{ height: '20px' }} />
			{effectString}
		</Typography>
	);
}
