import {
	Avatar, createStyles, makeStyles, Theme, Typography,
} from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { StuffItem } from '../../../../../interfaces';
import ItemEffects from './ItemEffects';

interface ComponentProps {
	item: StuffItem | undefined,
}

const useStyles = makeStyles((theme: Theme) => createStyles({
	img: {
		width: '4em',
		height: '4em',
	},
	red: {
		color: 'red',
	},
	green: {
		color: 'green',
	},
	large: {
		width: theme.spacing(7),
		height: theme.spacing(7),
	},
	tdWidth: {
		width: '100%',
	},
}));

export default function ItemShown(props: ComponentProps) {
	const classes = useStyles();

	const { t, ready } = useTranslation(['DofusItem', 'DofusItemType', 'DofusItemSet']);

	return ready ? (
		<>
			<table>
				<tbody>
					<tr>
						<td rowSpan={3}>
							{
								props.item
									? (
										<Avatar src={String(`/images/items/${props.item.id}.png`)} className={classes.large} />
									) : (
										<Skeleton variant="circle" width="4em" height="4em" />
									)
							}
						</td>
						<td className={classes.tdWidth}>
							{
								props.item
									? (
										<Typography variant="body1">{t(`DofusItem:${props.item.name_id}`)}</Typography>
									) : (
										<Skeleton variant="text" />
									)
							}
						</td>
					</tr>
					<tr>
						<td className={classes.tdWidth}>
							{
								props.item
									? (
										<Typography variant="body2">{(`${t(`DofusItemType:${props.item.type.name_id}`)} - Niveau ${props.item.level}`)}</Typography>
									) : (
										<Skeleton variant="text" />
									)
							}
						</td>
					</tr>
					<tr>
						<td className={classes.tdWidth}>
							{
								props.item
									? (
										<Typography variant="body2">{props.item.set && t(`DofusItemSet:${props.item.set.name_id}`)}</Typography>
									) : (
										<Skeleton variant="text" />
									)
							}
						</td>
					</tr>
				</tbody>
			</table>
			{
				props.item ? (
					<ItemEffects effects={props.item.effects} />
				) : (
					<>
						<Skeleton variant="text" />
						<Skeleton variant="text" />
						<Skeleton variant="text" />
						<Skeleton variant="text" />
						<Skeleton variant="text" />
						<Skeleton variant="text" />
						<Skeleton variant="text" />
					</>
				)
			}
		</>
	) : null;
}
