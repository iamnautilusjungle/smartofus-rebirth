import React from 'react';
import { Box, Grid } from '@material-ui/core';
import ItemShown from './ItemShown';
import { StuffItem } from '../../../../../interfaces';

interface ComponentProps {
	itemList: StuffItem[];
	equipItem: (item: StuffItem) => void;
	isLoading: Boolean;
	hasReachedLastPage: Boolean;
}

export default function ItemPicker(props: ComponentProps) {
	return (
		<>
			<Grid container spacing={1}>
				{
					props.itemList.map((item: StuffItem) => (
						<Grid key={item.id} item xs={12} sm={6} md={3}>
							<Box onClick={() => props.equipItem(item)}>
								<ItemShown item={item} />
							</Box>
						</Grid>
					))
				}
				{
					(!props.hasReachedLastPage && props.isLoading) ? [1, 2, 3, 4, 5, 6, 7, 8].map((n: number) => (
						<Grid key={n} item xs={12} sm={6} md={3}>
							<Box>
								<ItemShown item={undefined} />
							</Box>
						</Grid>
					)) : null
				}
			</Grid>
		</>
	);
}
