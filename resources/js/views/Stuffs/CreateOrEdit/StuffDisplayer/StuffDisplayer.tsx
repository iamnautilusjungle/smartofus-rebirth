import React, { useEffect, useState } from 'react';
import {
	AppBar, Box, Button, Dialog, DialogContent, Grid, GridSize, Hidden, IconButton, makeStyles, Toolbar, Typography,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import axios from 'axios';
import _ from 'lodash';
import { useSnackbar } from 'notistack';
import { useTranslation } from 'react-i18next';
import { getTypeFromStuffString } from '../../../../consts/itemTypes';
import ItemPicker from './ItemPicker/ItemPicker';
import ItemIcon from './ItemIcon';
import { Stuff, StuffItem } from '../../../../interfaces';
import { EquipmentsOfStuff } from '../../../../interfaces/Stuff/EquipmentsOfStuff';

const useStyles = makeStyles((theme) => ({
	appBar: {
		position: 'relative',
	},
	title: {
		marginLeft: theme.spacing(2),
		flex: 1,
	},
}));

interface ComponentProps {
	stuff: Stuff;
	desequipItem: (type: keyof EquipmentsOfStuff) => void;
	equipItem: (item: StuffItem) => void;
}

const NO_SELECTED_ICON = {
	amulet: 8272,
	ring_one: 7248,
	ring_two: 2469,
	weapon: 8575,
	shield: 18670,
	hat: 8474,
	cloak: 8866,
	belt: 2369,
	boots: 8291,
	pet: 2074,
	trophy_one: 739,
	trophy_two: 972,
	trophy_three: 737,
	trophy_four: 8072,
	trophy_five: 694,
	trophy_six: 7754,
};

export default function StuffDisplayer(props: ComponentProps) {
	const { t, ready } = useTranslation(['DofusStuffsPage']);
	const { enqueueSnackbar } = useSnackbar();

	const [open, setOpen] = React.useState(false);
	const [isLoading, setIsLoading] = React.useState(false);
	const [itemList, setItemList] = useState<Array<StuffItem>>([]);
	const [filterTypes, setFilterTypes] = useState<Array<number>>([]);
	const [page, setPage] = useState<number>(1);
	const [hasReachedLastPage, setHasReachedLastPage] = useState<Boolean>(false);

	useEffect(() => {
		setItemList([]);
	}, [filterTypes]);

	const getNextItems = () => {
		if (!!filterTypes && filterTypes.length !== 0 && !hasReachedLastPage) {
			axios.get('/web-api/dofus/items', {
				params: {
					page,
					types: filterTypes,
				},
			}).then((r) => {
				setItemList((previousItemList) => [...previousItemList, ...r.data.data]);
				setHasReachedLastPage(r.data.current_page === r.data.last_page);
			}).catch(() => {
				enqueueSnackbar(
					t('DofusStuffsPage:stuffs.items.load.error'),
					{ variant: 'error', autoHideDuration: 5000 },
				);
			}).finally(() => {
				setIsLoading(false);
			});
		}
	};

	useEffect(() => {
		getNextItems();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, filterTypes]);

	const classes = useStyles();

	const handleOpenItemDialog = (type: keyof EquipmentsOfStuff) => {
		setOpen(true);

		const newTypes = getTypeFromStuffString(type);
		if (!_.isEqual(filterTypes, newTypes)) {
			setFilterTypes(newTypes);
			setPage(1);
		}
	};

	const handleItemsScroll = (e: React.UIEvent<Element>) => {
		const target = e.target as HTMLElement;
		const scrollFromTop = target.scrollTop + target.offsetHeight;
		const totalSize = target.scrollHeight;
		const threshold = totalSize - (target.offsetHeight);
		if ((scrollFromTop > threshold) && !isLoading) {
			setIsLoading(true);
			setPage(page + 1);
		}
	};

	const item = (type: keyof EquipmentsOfStuff, xsSize: GridSize, lgSize: GridSize) => (
		<Grid key={type} item xs={xsSize} lg={lgSize}>
			<ItemIcon type={type} selectedItem={props.stuff[type]} openItemsDialog={handleOpenItemDialog} desequipItem={props.desequipItem} noSelectionImage={NO_SELECTED_ICON[type]} />
		</Grid>
	);

	const equipItem = (itemToEquip: StuffItem) => {
		setOpen(false);
		props.equipItem(itemToEquip);
	};

	const handleClose = () => {
		setOpen(false);
	};

	return ready ? (
		<>
			<Grid container spacing={1}>
				<Grid item xs={12} lg={2}>
					<Grid container spacing={1}>
						{
							[
								'amulet',
								'ring_one',
								'ring_two',
								'weapon',
								'shield',
							].map((type) => item(type as keyof EquipmentsOfStuff, 2, 12))
						}
					</Grid>
				</Grid>
				<Hidden mdDown>
					<Grid item xs={false} lg={8}>
						Hey there
					</Grid>
				</Hidden>
				<Grid item xs={12} lg={2}>
					<Grid container spacing={1}>
						{
							[
								'hat',
								'cloak',
								'belt',
								'boots',
								'pet',
							].map((type) => item(type as keyof EquipmentsOfStuff, 2, 12))
						}
					</Grid>
				</Grid>
				<Grid item xs={12}>
					<Grid container spacing={1}>
						{
							[
								'trophy_one',
								'trophy_two',
								'trophy_three',
								'trophy_four',
								'trophy_five',
								'trophy_six',
							].map((type) => item(type as keyof EquipmentsOfStuff, 2, 2))
						}
					</Grid>
				</Grid>
			</Grid>
			<Dialog scroll="paper" fullScreen open={open} onClose={handleClose}>
				<AppBar className={classes.appBar}>
					<Toolbar>
						<IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
							<CloseIcon />
						</IconButton>
						<Typography variant="h6" className={classes.title}>
							Pick an item
						</Typography>
					</Toolbar>
				</AppBar>
				<DialogContent
					onScroll={(e) => handleItemsScroll(e)}
					style={{ overflowX: 'hidden', overflowY: 'auto' }}
				>
					<Box my={1}>
						Filter
					</Box>
					<Box my={1}>
						<Grid item xs={12}>
							<ItemPicker itemList={itemList} equipItem={equipItem} isLoading={isLoading} hasReachedLastPage={hasReachedLastPage} />
						</Grid>
					</Box>
					<Grid item xs={12}>
						{
							!hasReachedLastPage && (
								isLoading ? null : (
									<Button onClick={() => setPage(page + 1)} variant="contained">{t('DofusStuffsPage:nextPage')}</Button>
								)
							)
						}
					</Grid>
				</DialogContent>
			</Dialog>
		</>
	) : null;
}
