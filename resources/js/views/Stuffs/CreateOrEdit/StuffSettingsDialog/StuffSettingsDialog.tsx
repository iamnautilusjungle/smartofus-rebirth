import {
	Box,
	Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid, makeStyles, TextField, Tooltip,
} from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Stuff, Breed } from '../../../../interfaces';
import { StuffBuilder, BreedsService } from '../../../../services';

const useStyles = makeStyles(() => ({
	monochrome: {
		filter: 'grayscale(1)',
	},
}));

interface ComponentProps {
	open: boolean;
	setOpen: React.Dispatch<React.SetStateAction<boolean>>;
	stuff: Stuff;
	setStuff: React.Dispatch<React.SetStateAction<Stuff | undefined>>;
	saveSettings?: ((stuff: Stuff) => void) | undefined;
}

StuffSettingsDialog.defaultProps = {
	saveSettings: undefined,
};

interface SettingsStuff {
	breeds_ids: Array<number>;
	name: string;
}

export default function StuffSettingsDialog(props: ComponentProps) {
	const { t, ready } = useTranslation(['DofusStuffsPage']);

	const [stuffManager] = useState<StuffBuilder>(new StuffBuilder(props.stuff));
	const [settings, setSettings] = useState<SettingsStuff>({
		...{
			breeds_ids: [],
			name: '',
		},
		...props.stuff,
	});

	const updateStuff = () => {
		let newStuff = stuffManager.setBreeds(settings.breeds_ids);
		newStuff = stuffManager.setName(settings.name);
		props.setStuff(newStuff);
		if (props.saveSettings) {
			props.saveSettings(newStuff);
			props.setOpen(false);
		}
	};

	return ready ? (
		<Dialog open={props.open} aria-labelledby="form-dialog-title">
			<DialogTitle id="form-dialog-title">
				{t('DofusStuffsPage:stuffs.title')}
			</DialogTitle>
			<DialogContent>
				<StuffSettings settings={settings} setSettings={setSettings} />
			</DialogContent>
			<DialogActions>
				<Button onClick={() => props.setOpen(false)} color="secondary">
					Cancel
				</Button>
				<Button onClick={updateStuff} color="primary">
					Save
				</Button>
			</DialogActions>
		</Dialog>
	) : null;
}

interface StuffSettingsProps {
	settings: SettingsStuff;
	setSettings: React.Dispatch<React.SetStateAction<SettingsStuff>>;
}

const chunkOfBreeds = _.chunk(BreedsService.all(), 6);

function StuffSettings(props: StuffSettingsProps) {
	const { t, ready } = useTranslation(['DofusBreed', 'DofusStuffsPage']);

	const classes = useStyles();

	const [breeds, setBreeds] = React.useState<Array<number>>(() => props.settings.breeds_ids);
	const [name, setName] = React.useState<string>(props.settings.name);

	useEffect(() => {
		props.setSettings({
			breeds_ids: breeds.sort(),
			name,
		});
	}, [breeds, name]);

	const handleBreeds = (event: React.MouseEvent<HTMLElement>, newBreeds: number[]) => {
		setBreeds(newBreeds);
	};

	return ready ? (
		<Grid container spacing={2} direction="column" alignItems="center">
			<Grid item>
				<Box pb={1}>
					<TextField defaultValue={props.settings.name} fullWidth onInput={(event: React.ChangeEvent<HTMLInputElement>) => setName(event.target.value)} label={String(t('DofusStuffsPage:stuffName'))} variant="outlined" size="small" />
				</Box>
				<Box>
					{
						chunkOfBreeds.map((listOfBreeds, index) => (
							// eslint-disable-next-line react/no-array-index-key
							<div key={index}>
								<ToggleButtonGroup size="small" value={breeds} onChange={handleBreeds}>
									{
										listOfBreeds.map((breed: Breed) => (
											<ToggleButton value={breed.id} key={breed.id}>
												<Tooltip title={String(t(`DofusBreed:${breed.shortNameId}`))}>
													<img className={breeds.includes(breed.id) ? '' : classes.monochrome} width="30" src={String(`/images/vignettingMiniCharacter/mini_${breed.id}_${breed.id % 2}.png`)} alt={String(t(`DofusBreed:${breed.shortNameId}`))} />
												</Tooltip>
											</ToggleButton>
										))
									}
								</ToggleButtonGroup>
							</div>
						))
					}
				</Box>
			</Grid>
		</Grid>
	) : null;
}
