import { Grid, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import CharacteristicIcon from '../../../../components/CharacteristicIcon/CharacteristicIcon';
import { Effects } from '../../../../consts/effects';
import { DamageCalculatorFacade } from '../../../../facades/DamageCalculatorFacade';
import { Damage, StuffEffects, StuffItem } from '../../../../interfaces';

interface ComponentProps {
	weapon?: StuffItem | undefined;
	effectsList: StuffEffects;
}

WeaponDisplayer.defaultProps = {
	weapon: undefined,
};

export default function WeaponDisplayer(props: ComponentProps) {
	const [damages, setDamages] = useState<Damage[]>([]);

	useEffect(() => {
		if (props.weapon) {
			setDamages(DamageCalculatorFacade.calculateWeapon(props.effectsList, props.weapon));
		} else {
			setDamages([]);
		}
	}, [props.weapon, props.effectsList]);

	return (
		<Grid container>
			<Grid item xs={12}>
				{
					props.weapon ? (
						<>
							<TotalOfDamage damages={damages} />
							<Grid container>
								<>
									{
										// eslint-disable-next-line react/no-array-index-key
										damages.map((damage, index) => <WholeDamageLine key={index} damage={damage} />)
									}
								</>
							</Grid>
							<TotalOfHealing damages={damages} />
						</>
					) : (
						<Typography variant="subtitle2">No weapon</Typography>
					)
				}
			</Grid>
		</Grid>
	);
}

function TotalOfDamage(props: { damages: Damage[] }) {
	const [min, setMin] = useState(0);
	const [max, setMax] = useState(0);
	const [minCs, setMinCs] = useState(0);
	const [maxCs, setMaxCs] = useState(0);

	useEffect(() => {
		setMin(props.damages.map((e) => e.min).reduce((buffer, e) => buffer + e, 0));
		setMax(props.damages.map((e) => e.max).reduce((buffer, e) => buffer + e, 0));
		setMinCs(props.damages.map((e) => e.min_cs || 0).reduce((buffer, e) => buffer + e, 0));
		setMaxCs(props.damages.map((e) => e.max_cs || 0).reduce((buffer, e) => buffer + e, 0));
	}, [props.damages]);

	return (
		<Grid container>
			<Grid item xs={6}>
				<Typography>
					{`Total: ${min} à ${max}`}
				</Typography>
			</Grid>
			<Grid item xs={6}>
				<Typography>
					{`CC: ${minCs} à ${maxCs}`}
				</Typography>
			</Grid>
		</Grid>
	);
}

function TotalOfHealing(props: { damages: Damage[] }) {
	const [min, setMin] = useState(0);
	const [max, setMax] = useState(0);
	const [minCs, setMinCs] = useState(0);
	const [maxCs, setMaxCs] = useState(0);

	useEffect(() => {
		const stealFilter = (e: Damage) => [
			Effects.WATER_STEAL,
			Effects.AIR_STEAL,
			Effects.EARTH_STEAL,
			Effects.NEUTRAL_STEAL,
			Effects.FIRE_STEAL,
		].includes(e.type);

		const filteredDamages = props.damages.filter(stealFilter);

		const sumReduce = (buffer: number, e: number) => buffer + e;

		setMin(filteredDamages.map((e) => Math.floor(e.min / 2)).reduce(sumReduce, 0));
		setMax(filteredDamages.map((e) => Math.floor(e.max / 2)).reduce(sumReduce, 0));
		setMinCs(filteredDamages.map((e) => Math.floor((e.min_cs || 0) / 2)).reduce(sumReduce, 0));
		setMaxCs(filteredDamages.map((e) => Math.floor((e.max_cs || 0) / 2)).reduce(sumReduce, 0));
	}, [props.damages]);

	return (
		<Grid container>
			<Grid item xs={6}>
				<Typography>
					{`Total soin: ${min} à ${max}`}
				</Typography>
			</Grid>
			<Grid item xs={6}>
				<Typography>
					{`CC soin: ${minCs} à ${maxCs}`}
				</Typography>
			</Grid>
		</Grid>
	);
}

function WholeDamageLine(props: { damage: Damage }) {
	return (
		<Grid container>
			<Grid item xs={6}>
				<DamageLine min={props.damage.min} max={props.damage.max} icon={props.damage.icon} type={props.damage.type} />
			</Grid>
			<Grid item xs={6}>
				{
					typeof props.damage.min_cs !== 'undefined' ? (
						<DamageLine min={props.damage.min_cs} max={props.damage.max_cs || props.damage.min_cs} icon={props.damage.icon} type={props.damage.type} />
					) : (
						<Grid item xs={6} />
					)
				}
			</Grid>
		</Grid>
	);
}

function DamageLine(props: { min: number; max: number; icon: string; type: number; }) {
	return (
		<Grid container>
			<Grid item xs={2}>
				<CharacteristicIcon icon={props.icon} />
			</Grid>
			<Grid item xs={10}>
				<Typography>
					{props.min}
					{' '}
					à
					{' '}
					{props.max}
				</Typography>
			</Grid>
			{
				[
					Effects.WATER_STEAL,
					Effects.AIR_STEAL,
					Effects.EARTH_STEAL,
					Effects.NEUTRAL_STEAL,
					Effects.FIRE_STEAL,
				].includes(props.type) ? <HealLine min={props.min} max={props.max} /> : null
			}
		</Grid>
	);
}

function HealLine(props: { min: number; max: number }) {
	return (
		<>
			<Grid item xs={2}>
				<CharacteristicIcon icon="tx_heal" />
			</Grid>
			<Grid item xs={10}>
				<Typography>
					{Math.floor(props.min / 2)}
					{' '}
					à
					{' '}
					{Math.floor(props.max / 2)}
				</Typography>
			</Grid>
		</>
	);
}
