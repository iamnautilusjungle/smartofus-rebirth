import React from 'react';
import { Button, Grid } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import CharacteristicPicker from './CharacteristicPicker';
import { StuffCharacteristicsCalculator } from '../../../../services';
import {
	CharacteristicsOrParchments, Stuff,
} from '../../../../interfaces';

interface ComponentProps {
	setCharacteristic: (v: number, k: keyof CharacteristicsOrParchments) => void;
	stuff: Stuff;
	setParchment: (v: number) => void;
}

export default function CharacteristicsPicker(props: ComponentProps) {
	const { t, ready } = useTranslation(['DofusStuffsPage']);

	return ready ? (
		<Grid container>
			<Grid item xs={6}>{t('DofusStuffsPage:stuffs.base')}</Grid>
			<Grid item xs={6}>{t('DofusStuffsPage:stuffs.parchment')}</Grid>
			<CharacteristicPicker
				maxBase={995}
				setCharacteristic={props.setCharacteristic}
				stuff={props.stuff}
				base="vitality_base"
				parchment="vitality_parchment"
			/>
			<CharacteristicPicker
				maxBase={331}
				setCharacteristic={props.setCharacteristic}
				stuff={props.stuff}
				base="wisdom_base"
				parchment="wisdom_parchment"
			/>
			<CharacteristicPicker
				maxBase={398}
				setCharacteristic={props.setCharacteristic}
				stuff={props.stuff}
				base="strength_base"
				parchment="strength_parchment"
			/>
			<CharacteristicPicker
				maxBase={398}
				setCharacteristic={props.setCharacteristic}
				stuff={props.stuff}
				base="intelligence_base"
				parchment="intelligence_parchment"
			/>
			<CharacteristicPicker
				maxBase={398}
				setCharacteristic={props.setCharacteristic}
				stuff={props.stuff}
				base="agility_base"
				parchment="agility_parchment"
			/>
			<CharacteristicPicker
				maxBase={398}
				setCharacteristic={props.setCharacteristic}
				stuff={props.stuff}
				base="chance_base"
				parchment="chance_parchment"
			/>
			<Grid item xs={6}>{(new StuffCharacteristicsCalculator(props.stuff)).getRemainingCharacteristicsPoints()}</Grid>
			<Grid item xs={6}>
				<Grid container>
					<Grid item xs={6}>
						<Button variant="contained" color="primary" size="small" fullWidth style={{ padding: 0, minWidth: 0 }} onClick={() => props.setParchment(0)}>
							0
						</Button>
					</Grid>
					<Grid item xs={6}>
						<Button variant="contained" color="primary" size="small" fullWidth style={{ padding: 0, minWidth: 0 }} onClick={() => props.setParchment(100)}>
							100
						</Button>
					</Grid>
				</Grid>
			</Grid>
		</Grid>
	) : null;
}
