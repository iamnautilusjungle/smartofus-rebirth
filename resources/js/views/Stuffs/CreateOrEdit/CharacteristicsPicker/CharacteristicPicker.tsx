import React from 'react';
import { Grid, TextField } from '@material-ui/core';
import {
	Characteristics, CharacteristicsOrParchments, Parchments, Stuff,
} from '../../../../interfaces';

interface ComponentProps {
	setCharacteristic: (v: number, k: keyof CharacteristicsOrParchments) => void;
	base: keyof Characteristics;
	parchment: keyof Parchments;
	stuff: Stuff;
	maxBase: number;
}

export default function CharacteristicPicker(props: ComponentProps) {
	return (
		<Grid container>
			<Grid item xs={6}>
				<TextField
					variant="outlined"
					size="small"
					inputProps={{
						min: 0,
						max: props.maxBase,
						style: { paddingTop: 0, paddingBottom: 0 },
						onBlur: (event) => String(props.setCharacteristic(Math.max(0, Math.min(props.maxBase, ~~(event.target.value.replace(/[^0-9]/g, '')))), props.base)),
					}}
					onChange={(event) => String(props.setCharacteristic(Math.max(0, Math.min(props.maxBase, ~~(event.target.value.replace(/[^0-9]/g, '')))), props.base))}
					value={props.stuff[props.base] || 0}
				/>
			</Grid>
			<Grid item xs={6}>
				<TextField
					variant="outlined"
					size="small"
					inputProps={{
						min: 0,
						max: 100,
						style: { paddingTop: 0, paddingBottom: 0 },
						onBlur: (event) => String(props.setCharacteristic(Math.max(0, Math.min(100, ~~(event.target.value.replace(/[^0-9]/g, '')))), props.parchment)),
					}}
					onChange={(event) => String(props.setCharacteristic(Math.max(0, Math.min(100, ~~(event.target.value.replace(/[^0-9]/g, '')))), props.parchment))}
					value={props.stuff[props.parchment] || 0}
				/>
			</Grid>
		</Grid>
	);
}
