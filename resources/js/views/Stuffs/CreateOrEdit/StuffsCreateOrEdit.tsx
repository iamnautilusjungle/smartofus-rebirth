import React, {
	Dispatch, SetStateAction, useEffect, useRef, useState,
} from 'react';
import {
	Button, Grid, makeStyles, Tooltip, Typography,
	SvgIconTypeMap,
	Box,
} from '@material-ui/core';
import _ from 'lodash';
import SaveIcon from '@material-ui/icons/Save';
import LinkIcon from '@material-ui/icons/Link';
import EditIcon from '@material-ui/icons/Edit';
import SettingsIcon from '@material-ui/icons/Settings';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { useTranslation } from 'react-i18next';
import { OverridableComponent } from '@material-ui/core/OverridableComponent';
import { useHistory, useLocation } from 'react-router';
import PageContainer from '../../../components/PageContainer/PageContainer';
import TofuCard from '../../../components/TofuCard/TofuCard';
import StuffDisplayer from './StuffDisplayer/StuffDisplayer';
import MiniBar from '../../../components/MiniBar/MiniBar';
import SecondaryStats from './SecondaryStats/SecondaryStats';
import MainStats from './MainStats/MainStats';
import IUser from '../../../types/IUser';
import { StuffBuilder, CopyToClipboard, StuffEffectsCalculator } from '../../../services';
import ConditionDisplay from '../../../components/ConditionDisplay/ConditionDisplay';
import StuffSettingsDialog from './StuffSettingsDialog/StuffSettingsDialog';
import {
	CharacteristicsOrParchments, StuffItem, Stuff, StuffEffects,
} from '../../../interfaces';
import { EquipmentsOfStuff } from '../../../interfaces/Stuff/EquipmentsOfStuff';
import WeaponDisplayer from './WeaponDisplayer/WeaponDisplayer';

const useStyles = makeStyles((theme) => ({
	title: {
		flexGrow: 1,
	},
	minibarButton: {
		marginRight: theme.spacing(1),
	},
}));

interface ComponentProps {
	type: 'Create' | 'Edit' | 'Show';
	auth: IUser | undefined;
	creator: IUser | undefined;
	stuff: Stuff;
	onSaveClick: () => void;
	setStuff: Dispatch<SetStateAction<Stuff | undefined>>;
	saveSettings?: ((stuff: Stuff) => void) | undefined;
}

StuffsCreateOrEdit.defaultProps = {
	saveSettings: undefined,
};

export default function StuffsCreateOrEdit(props: ComponentProps) {
	const { t, ready } = useTranslation(['DofusStuffsPage']);
	const classes = useStyles();
	const history = useHistory();
	const location = useLocation();

	const [effectsList, setEffectsList] = useState<StuffEffects>(new Map());
	const [stuffManager, setStuffBuilder] = useState<StuffBuilder>();
	const [openStuffSettings, setOpenStuffSettings] = useState(false);

	const debouncedSave = useRef(_.debounce((stuff) => {
		if (stuff) {
			setStuffBuilder(new StuffBuilder(stuff));
			setEffectsList((new StuffEffectsCalculator(stuff).getEffectsMap()));
		}
	}, 300))
		.current;

	useEffect(() => {
		debouncedSave(props.stuff);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [props.stuff]);

	const setCharacteristic = (v: number, k: keyof CharacteristicsOrParchments) => {
		props.setStuff(stuffManager?.setCharacteristic(k, v));
	};

	const setParchment = (v: number) => {
		props.setStuff(stuffManager?.setParchment(v));
	};

	const desequipItem = (type: keyof EquipmentsOfStuff) => {
		props.setStuff(stuffManager?.desequipItem(type));
	};

	const equipItem = (item: StuffItem) => {
		props.setStuff(stuffManager?.equipItem(item));
	};

	const copyLink = () => {
		if (props.stuff) {
			CopyToClipboard.copy(`${window.location.origin}/stuffs/${props.stuff.code}`);
		}
	};

	const toggleIsPrivate = () => {
		props.setStuff(stuffManager?.toggleIsPrivate());
	};

	const goToEditPage = () => {
		history.push(`${location.pathname}/edit`);
	};

	return ready ? (
		<>
			<MiniBar>
				<Typography variant="body1" className={classes.title}>
					{t('DofusStuffsPage:stuffs.title')}
					{' '}
					-
					{' '}
					{props.stuff.name}
				</Typography>
				<ConditionDisplay condition={props.type === 'Create' || props.type === 'Edit'}>
					<MenuButton
						title={props.auth?.id
							? String(t('DofusStuffsPage:stuffs.save'))
							: String(t('DofusStuffsPage:stuffs.mustBeConnected'))}
						disabled={!props.auth?.id}
						onClick={props.onSaveClick}
						icon={SaveIcon}
					/>
				</ConditionDisplay>
				<ConditionDisplay condition={props.type === 'Edit'}>
					<>
						<MenuButton
							title={String(t('DofusStuffsPage:stuffs.settings'))}
							onClick={() => setOpenStuffSettings(true)}
							icon={SettingsIcon}
						/>
						<MenuButton
							title={props.stuff.is_private
								? String(t('DofusStuffsPage:stuffs.isPrivate'))
								: String(t('DofusStuffsPage:stuffs.isPublic'))}
							onClick={toggleIsPrivate}
							icon={props.stuff.is_private ? VisibilityOffIcon : VisibilityIcon}
						/>
					</>
				</ConditionDisplay>
				<ConditionDisplay condition={props.type === 'Edit' || props.type === 'Show'}>
					<MenuButton
						title={(props.type === 'Edit' || props.type === 'Show')
							? String(t('DofusStuffsPage:stuffs.getLink'))
							: String(t('DofusStuffsPage:stuffs.notSaved'))}
						onClick={copyLink}
						icon={LinkIcon}
					/>
				</ConditionDisplay>
				<ConditionDisplay condition={props.type === 'Show' && props.auth?.id === props.stuff.creator_id}>
					<MenuButton
						title={String(t('DofusStuffsPage:edit'))}
						onClick={goToEditPage}
						icon={EditIcon}
					/>
				</ConditionDisplay>
			</MiniBar>
			<PageContainer>
				<Grid container spacing={1}>
					<Grid item xs={12} lg={4}>
						<MainStats effectsList={effectsList} setCharacteristic={setCharacteristic} setParchment={setParchment} stuff={props.stuff} />
					</Grid>
					<Grid item xs={12} lg={4}>
						<Box mb={1}>
							<TofuCard>
								<StuffDisplayer equipItem={equipItem} stuff={props.stuff} desequipItem={desequipItem} />
							</TofuCard>
						</Box>
						<Box>
							<TofuCard>
								<WeaponDisplayer effectsList={effectsList} weapon={props.stuff.weapon} />
							</TofuCard>
						</Box>
					</Grid>
					<Grid item xs={12} lg={4}>
						<SecondaryStats user={props.creator} effectsList={effectsList} />
					</Grid>
				</Grid>
			</PageContainer>
			<StuffSettingsDialog open={openStuffSettings} setOpen={setOpenStuffSettings} stuff={props.stuff} setStuff={props.setStuff} saveSettings={props.saveSettings} />
		</>
	) : null;
}

interface ButtonSettings {
	title: string;
	onClick: React.MouseEventHandler<HTMLButtonElement>;
	icon: OverridableComponent<SvgIconTypeMap<{}, 'svg'>>;
	disabled?: Boolean;
}

MenuButton.defaultProps = {
	disabled: false,
};

function MenuButton(props: ButtonSettings) {
	const classes = useStyles();

	const Icon = props.icon;

	return (
		<Tooltip title={props.title}>
			<span>
				<Button variant="contained" disabled={!!props.disabled} className={classes.minibarButton} onClick={props.onClick}>
					<Icon />
				</Button>
			</span>
		</Tooltip>
	);
}
