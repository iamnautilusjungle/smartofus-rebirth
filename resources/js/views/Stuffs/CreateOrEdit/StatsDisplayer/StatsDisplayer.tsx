import { Box, Grid, Typography } from '@material-ui/core';
import React from 'react';
import { useTranslation } from 'react-i18next';
import CharacteristicIcon from '../../../../components/CharacteristicIcon/CharacteristicIcon';

interface ComponentProps {
	effectsList: Map<number, number>;
	statsList: SingleStat[]
}

interface SingleStat {
	id: number;
	name_id: number;
	icon: string;
}

const cleanString = (str: string) => str;

export default function StatsDisplayer(props: ComponentProps) {
	const { t, ready } = useTranslation(['DofusCharacteristic']);

	return ready ? (
		<Grid container>
			{
				props.statsList.map((e) => (
					<Grid item xs={12} key={e.id}>
						<Grid container>
							<Grid item xs={2}>
								<Box mr={1}>
									<Typography align="right" variant="body2">
										{
											props.effectsList.get(e.id) || 0
										}
									</Typography>
								</Box>
							</Grid>
							<Grid item xs={1}>
								<CharacteristicIcon icon={e.icon} />
							</Grid>
							<Grid item xs={9}>
								<Box ml={1}>
									<Typography variant="body2">
										{
											cleanString(t(`DofusCharacteristic:${e.name_id}`))
										}
									</Typography>
								</Box>
							</Grid>
						</Grid>
					</Grid>
				))
			}
		</Grid>
	) : null;
}
