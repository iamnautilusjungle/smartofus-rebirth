import axios from 'axios';
import { useSnackbar } from 'notistack';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import { useRecoilValue } from 'recoil';
import StuffsCreateOrEdit from '../CreateOrEdit/StuffsCreateOrEdit';
import AuthUserAtom from '../../../atoms/AuthUser';
import { Stuff } from '../../../interfaces';

type Params = {
	code: string
};

export default function StuffsShow() {
	const auth = useRecoilValue(AuthUserAtom);
	const { enqueueSnackbar } = useSnackbar();
	const { t } = useTranslation(['DofusJobsPage', 'DofusJob']);
	const [stuff, setStuff] = useState<Stuff>();
	const { code } = useParams<Params>();

	function loadInitialStuff() {
		axios.get(`/web-api/dofus/stuffs/${code}`).then((r) => {
			const loadedStuff: Stuff = r.data;
			setStuff(loadedStuff);
		}).catch(() => {
			enqueueSnackbar(
				t('DofusStuffsPage:stuffs.retrieving.error'),
				{ variant: 'error', autoHideDuration: 5000 },
			);
		});
	}

	useEffect(() => {
		loadInitialStuff();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const onSaveClick = () => {

	};

	return stuff ? (
		<StuffsCreateOrEdit type="Show" auth={auth} stuff={stuff} setStuff={setStuff} creator={stuff.creator} onSaveClick={onSaveClick} />
	) : null;
}
