import React from 'react';
import PageContainer from '../../components/PageContainer/PageContainer';
import TofuCard from '../../components/TofuCard/TofuCard';

export default function Home() {
	return (
		<PageContainer>
			<TofuCard>
				Home
			</TofuCard>
		</PageContainer>
	);
}
