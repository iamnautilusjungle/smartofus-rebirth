import { Job, JobItem } from '../../interfaces';

export interface ActiveJob extends Job {
	crafts: JobItem[];
}
