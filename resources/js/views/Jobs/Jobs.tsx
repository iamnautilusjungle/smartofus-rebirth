import React, { useEffect, useState } from 'react';
import Typography from '@material-ui/core/Typography';
import { useTranslation } from 'react-i18next';
import axios from 'axios';
import _ from 'lodash';
import { Button, makeStyles } from '@material-ui/core';
import { useSnackbar } from 'notistack';
import PageContainer from '../../components/PageContainer/PageContainer';
import MiniBar from '../../components/MiniBar/MiniBar';
import JobsInit from './JobsInit/JobsInit';
import JobsCrafting from './JobsCrafting/JobsCrafting';
import JobsFacade from '../../facades/JobsFacade';
import { Job, Craft, JobItem } from '../../interfaces';
import { ActiveJob } from './Jobs.types';

const STEP_SETUP = 0;
const STEP_CRAFTING = 1;

const useStyles = makeStyles((theme) => ({
	marginRight: {
		marginRight: theme.spacing(1),
	},
	flex: {
		flexGrow: 1,
	},
}));

export default function Jobs() {
	const { t, ready } = useTranslation(['DofusJobsPage', 'DofusJob']);
	const { enqueueSnackbar } = useSnackbar();
	const classes = useStyles();

	const [jobs, setJobs] = useState<Job[]>([]);
	const [selectedJob, setSelectedJob] = useState<Job>();
	const [startingXp, setStartingXp] = useState(0);
	const [currentXp, setCurrentXp] = useState(0);
	const [multiplicator, setMultiplicator] = useState(1);
	const [activeStep, setActiveStep] = useState(STEP_SETUP);
	const [activeJob, setActiveJob] = useState<ActiveJob>();
	const [activeItem, setActiveItem] = useState<JobItem>();
	const [craftList, setCraftList] = useState<Craft[]>([]);

	useEffect(() => {
		axios.get('/web-api/dofus/jobs').then((r) => {
			setJobs(r.data);
		}).catch(() => {
			enqueueSnackbar(
				t('DofusJobsPage:jobs.retrieving.error'),
				{ variant: 'error', autoHideDuration: 5000 },
			);
		});
	}, []);

	useEffect(() => {
		setCurrentXp(craftList.reduce((prev, curr) => prev + curr.xp_gained, startingXp));
	}, [craftList]);

	function setBeginningXp(xp: number) {
		setStartingXp(xp);
		setCurrentXp(xp);
	}

	function jobsInitDone() {
		setActiveStep(STEP_CRAFTING);
		axios.get(`/web-api/dofus/jobs/${selectedJob?.id}`).then((r) => {
			const job: ActiveJob = r.data;
			setActiveJob(job);
			if (job.crafts[0]) {
				setActiveItem(job.crafts[0]);
			}
		}).catch(() => {
			enqueueSnackbar(
				t('DofusJobsPage:jobs.retrieving.error'),
				{ variant: 'error', autoHideDuration: 5000 },
			);
		});
	}

	function resetJob() {
		setSelectedJob(undefined);
		setStartingXp(0);
		setCurrentXp(0);
		setMultiplicator(1);
		setActiveStep(STEP_SETUP);
		setActiveJob(undefined);
		setActiveItem(undefined);
		setCraftList([]);
	}

	function craftItem(item: JobItem, quantity: number, xp_gained: number) {
		if (quantity > 0) {
			const last = _.last(craftList);
			if (item.id === last?.item.id) {
				const newCraft: Craft = {
					item, quantity: quantity + last.quantity, xp_gained: xp_gained + last.xp_gained, xp_total: last.xp_total + xp_gained,
				};
				setCraftList((crafts: Craft[]) => [..._.initial(crafts), newCraft]);
			} else {
				const newCraft: Craft = {
					item, quantity, xp_gained, xp_total: xp_gained + currentXp,
				};
				setCraftList((crafts: Craft[]) => [...crafts, newCraft]);
			}
		}
	}

	function removeItem() {
		setCraftList((crafts) => _.initial(crafts));
	}

	return ready ? (
		<>
			<MiniBar>
				<Typography variant="body1" className={classes.marginRight}>
					{t('DofusJobsPage:Jobs')}
				</Typography>
				{
					selectedJob && activeStep === STEP_CRAFTING && (
						<>
							<Typography variant="body1" className={classes.marginRight}>
								{t(`DofusJob:${selectedJob.name_id}`)}
							</Typography>
							<Typography variant="body1" className={classes.flex}>
								{t('DofusJobsPage:Level')}
								{' '}
								{JobsFacade.getLevelFromXp(currentXp)}
							</Typography>
							<Button onClick={resetJob} variant="contained" color="secondary">{t('DofusJobsPage:Reset')}</Button>
						</>
					)
				}
			</MiniBar>
			<PageContainer>
				{
					activeStep === STEP_SETUP ? (
						<JobsInit jobs={jobs} setSelectedJob={setSelectedJob} setStartingXp={setBeginningXp} setMultiplicator={setMultiplicator} isReady={jobsInitDone} />
					) : activeStep === STEP_CRAFTING && activeJob ? (
						<JobsCrafting removeItem={removeItem} craftList={craftList} craftItem={craftItem} activeJob={activeJob} currentXp={currentXp} multiplicator={multiplicator} activeItem={activeItem} setActiveItem={setActiveItem} />
					) : null
				}
			</PageContainer>
		</>
	) : null;
}
