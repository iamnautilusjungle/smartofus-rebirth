import React, { useState } from 'react';
import { Grid, TextField, Typography } from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import JobsFacade from '../../../facades/JobsFacade';

interface JobStartingLevelSelectorProps {
	setStartingXp: (xp: number) => void
}

export default function JobStartingLevelSelector(props: JobStartingLevelSelectorProps) {
	const [disableLevel, setDisableLevel] = useState(false);
	const [disableExperience, setDisableExperience] = useState(false);

	const { t } = useTranslation(['DofusJobsPage']);

	function tryToGetLevel(e: string) {
		if (Number.isInteger(parseInt(e, 10))) { props.setStartingXp(JobsFacade.getXpFromLevel(Math.max(1, parseInt(e, 10)))); }
	}

	function tryToGetXp(e: string) {
		if (Number.isInteger(parseInt(e, 10))) { props.setStartingXp(Math.max(0, parseInt(e, 10))); }
	}

	return (
		<Grid container spacing={2}>
			<Grid item xs={5}>
				<Typography align="center">
					{t('DofusJobsPage:Input a starting level')}
				</Typography>
			</Grid>
			<Grid item xs={2}>
				<Typography align="center">
					{t('DofusJobsPage:OR')}
				</Typography>
			</Grid>
			<Grid item xs={5}>
				<Typography align="center">
					{t('DofusJobsPage:Input a starting experience amount')}
				</Typography>
			</Grid>
			<Grid item xs={6}>
				<TextField
					label={t('DofusJobsPage:Level')}
					type="number"
					fullWidth
					disabled={disableLevel}
					InputProps={{ inputProps: { min: 1, max: 200 } }}
					onFocus={() => setDisableExperience(true)}
					onBlur={(e) => {
						tryToGetLevel(e.target.value);
						setDisableExperience(!!e.target.value);
					}}
				/>
			</Grid>
			<Grid item xs={6}>
				<TextField
					label={t('DofusJobsPage:Experience points')}
					type="number"
					fullWidth
					disabled={disableExperience}
					InputProps={{ inputProps: { min: 0, max: 398000 } }}
					onFocus={() => setDisableLevel(true)}
					onBlur={(e) => {
						tryToGetXp(e.target.value);
						setDisableLevel(!!e.target.value);
					}}
				/>
			</Grid>
		</Grid>
	);
}
