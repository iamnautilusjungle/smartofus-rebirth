import {
	Button, ButtonGroup, Grid,
} from '@material-ui/core';
import React from 'react';

interface JobXpMultiplicatorSelectorProps {
	setMultiplicator: (multiplicator: number) => void;
}

export default function JobXpMultiplicatorSelector(props: JobXpMultiplicatorSelectorProps) {
	return (
		<Grid container spacing={2}>
			<Grid item xs={12}>
				<ButtonGroup color="primary" aria-label="outlined primary button group">
					{
						[1, 1.25, 1.5, 1.75, 2].map((multiplicator) => (
							<Button key={multiplicator} onClick={() => props.setMultiplicator(multiplicator)}>
								{multiplicator * 100}
								%
							</Button>
						))
					}
				</ButtonGroup>
			</Grid>
		</Grid>
	);
}
