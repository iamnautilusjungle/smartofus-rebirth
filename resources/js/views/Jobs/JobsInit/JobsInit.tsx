import React, { Dispatch, SetStateAction } from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Grid } from '@material-ui/core';
import JobsInitSteps from './JobsInitSteps';
import JobSelector from './JobSelector';
import JobStartingLevelSelector from './JobStartingLevelSelector';
import JobXpMultiplicatorSelector from './JobXpMultiplicatorSelector';
import TofuCard from '../../../components/TofuCard/TofuCard';
import { Job } from '../../../interfaces';

interface JobsInitProps {
	jobs: Job[],
	setSelectedJob: Dispatch<SetStateAction<Job | undefined>>,
	setStartingXp: (xp: number) => void,
	setMultiplicator: React.Dispatch<React.SetStateAction<number>>,
	isReady: () => void
}

const STEP_PICKING_JOB = 0;
const STEP_PICKING_STARTING_LEVEL = 1;
const STEP_PICKING_MULTIPLICATOR = 2;
const STEP_DONE = 3;

export default function JobsInit(props: JobsInitProps) {
	const [activeStep, setActiveStep] = React.useState(0);
	const [maxStep, setMaxStep] = React.useState(0);

	const { t, ready } = useTranslation(['DofusJobsPage']);

	function setStep(step: number) {
		setActiveStep(step);
		setMaxStep(Math.max(maxStep, step));
	}

	function setSelectedJob(job: Job) {
		props.setSelectedJob(job);
		setStep(STEP_PICKING_STARTING_LEVEL);
	}

	function setStartingXp(xp: number) {
		props.setStartingXp(xp);
		setStep(STEP_PICKING_MULTIPLICATOR);
	}

	function setMultiplicator(multiplicator: number) {
		props.setMultiplicator(multiplicator);
		setStep(STEP_DONE);
		props.isReady();
	}

	return ready ? (
		<TofuCard>
			<Grid container spacing={2}>
				<Grid item xs={12}>
					<JobsInitSteps activeStep={activeStep} />
				</Grid>
				<Grid item xs={6}>
					<Button disabled={activeStep === 0} onClick={() => setStep(activeStep - 1)}>
						{t('DofusJobsPage:Back')}
					</Button>
					<Button
						variant="contained"
						color="primary"
						onClick={() => setStep(activeStep + 1)}
						disabled={activeStep + 1 > maxStep}
					>
						{t('DofusJobsPage:Next')}
					</Button>
				</Grid>
				<Grid item xs={12}>
					{
						activeStep === STEP_PICKING_JOB ? (
							<JobSelector jobs={props.jobs} setSelectedJob={setSelectedJob} />
						) : activeStep === STEP_PICKING_STARTING_LEVEL ? (
							<JobStartingLevelSelector setStartingXp={setStartingXp} />
						) : activeStep === STEP_PICKING_MULTIPLICATOR ? (
							<JobXpMultiplicatorSelector setMultiplicator={setMultiplicator} />
						) : null
					}
				</Grid>
			</Grid>
		</TofuCard>
	) : null;
}
