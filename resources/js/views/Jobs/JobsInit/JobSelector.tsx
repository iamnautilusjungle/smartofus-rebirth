import React from 'react';
import { Grid } from '@material-ui/core';
import JobIcon from './JobIcon';
import { Job, JobType } from '../../../interfaces';

interface JobSelectorProps {
	jobs: Job[],
	setSelectedJob: (job: Job) => void
}

export default function JobSelector(props: JobSelectorProps) {
	function pickJob(job: Job) {
		props.setSelectedJob(job);
	}

	return (
		<Grid container spacing={2}>
			<Grid item xs={12} lg={6}>
				<Grid container spacing={2}>
					{
						props.jobs.filter((e) => e.job_type === JobType.COLLECTER).map((job) => (
							<Grid key={job.id} item xs={2}>
								<JobIcon job={job} onClick={pickJob} />
							</Grid>
						))
					}
				</Grid>
			</Grid>
			<Grid item xs={12} lg={6}>
				<Grid container spacing={2}>
					{
						props.jobs.filter((e) => e.job_type === JobType.CRAFTER).map((job) => (
							<Grid key={job.id} item xs={2}>
								<JobIcon job={job} onClick={pickJob} />
							</Grid>
						))
					}
				</Grid>
			</Grid>
		</Grid>
	);
}
