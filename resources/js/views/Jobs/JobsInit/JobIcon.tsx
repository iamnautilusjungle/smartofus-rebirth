import {
	Card, CardActionArea, CardContent, CardMedia, createStyles, makeStyles, Theme, Typography,
} from '@material-ui/core';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Job } from '../../../interfaces';

interface JobIconProps {
	job: Job,
	onClick: (job: Job) => void
}

const useStyles = makeStyles((theme: Theme) => createStyles({
	media: {
		minHeight: 80,
	},
	root: {
		padding: theme.spacing(1),
	},
}));

export default function JobIcon(props: JobIconProps) {
	const classes = useStyles();

	const { t, ready } = useTranslation(['DofusJob']);

	return ready ? (
		<Card onClick={() => props.onClick(props.job)}>
			<CardActionArea>
				<CardMedia
					className={classes.media}
					image={`/images/jobs/${props.job.id}.jpg`}
					title={t(`DofusJob:${props.job.name_id}`)}
				/>
				<CardContent className={classes.root}>
					<Typography gutterBottom variant="body2" component="span" align="center">
						{t(`DofusJob:${props.job.name_id}`)}
					</Typography>
				</CardContent>
			</CardActionArea>
		</Card>
	) : null;
}
