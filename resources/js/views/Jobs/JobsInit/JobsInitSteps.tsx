import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles((theme: Theme) => createStyles({
	root: {
		width: '100%',
	},
	backButton: {
		marginRight: theme.spacing(1),
	},
	instructions: {
		marginTop: theme.spacing(1),
		marginBottom: theme.spacing(1),
	},
}));

interface JobsInitStepsProps {
	activeStep: number
}

export default function JobsInitSteps(props: JobsInitStepsProps) {
	const classes = useStyles();

	const { t, ready } = useTranslation(['DofusJobsPage']);

	return ready ? (
		<div className={classes.root}>
			<Stepper activeStep={props.activeStep} alternativeLabel>
				<Step>
					<StepLabel>{t('DofusJobsPage:Pick a job')}</StepLabel>
				</Step>
				<Step>
					<StepLabel>{t('DofusJobsPage:Pick a starting level')}</StepLabel>
				</Step>
				<Step>
					<StepLabel>{t('DofusJobsPage:Pick a multiplicator')}</StepLabel>
				</Step>
			</Stepper>
		</div>
	) : null;
}
