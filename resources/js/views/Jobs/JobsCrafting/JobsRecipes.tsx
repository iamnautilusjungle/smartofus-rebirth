import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { useTranslation } from 'react-i18next';
import {
	createStyles, IconButton, TablePagination, Tooltip, Typography,
} from '@material-ui/core';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import PlusOneIcon from '@material-ui/icons/PlusOne';
import SkipNextIcon from '@material-ui/icons/SkipNext';
import JobsLineRecipe from './JobsLineRecipe';
import JobsFacade from '../../../facades/JobsFacade';
import { JobItem } from '../../../interfaces/Job';
import { ActiveJob } from '../Jobs.types';

const useStyles = makeStyles(() => createStyles({
	itemImage: {
		width: '2em',
	},
}));

interface ComponentProps {
	activeJob: ActiveJob;
	setActiveItem: React.Dispatch<React.SetStateAction<JobItem | undefined>>;
	currentXp: number;
	multiplicator: number;
	craftItem: (item: JobItem, quantity: number, xp_gained: number) => void;
}

export default function JobsRecipes(props: ComponentProps) {
	const classes = useStyles();

	const { t, ready } = useTranslation(['DofusItem', 'DofusJobsPage']);

	const [page, setPage] = useState(0);
	const [rowsPerPage, setRowsPerPage] = useState(10);

	const currentLevel = JobsFacade.getLevelFromXp(props.currentXp);

	const handleChangePage = (event: unknown, newPage: number) => {
		setPage(newPage);
	};

	const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
		setRowsPerPage(parseInt(event.target.value, 10));
		setPage(0);
	};

	const handleCraftQuantity = (item: JobItem, quantity: number) => {
		props.craftItem(item, quantity, JobsFacade.getXpForQuantity(quantity, props.currentXp, props.multiplicator, item) - props.currentXp);
	};

	const handleCraftToLevel = (item: JobItem, level: number) => {
		if (currentLevel < 200) {
			handleCraftQuantity(item, JobsFacade.getQuantityForLevel(level, props.currentXp, props.multiplicator, item));
		}
	};

	const getNextNthLevel = (level: number, rounding: number = 10) => Math.ceil((level + 1) / rounding) * rounding;

	return ready ? (
		<>
			<TableContainer component={Paper}>
				<Table size="small">
					<TableHead>
						<TableRow>
							<TableCell>{t('DofusJobsPage:Level')}</TableCell>
							<TableCell>{t('DofusJobsPage:Item')}</TableCell>
							<TableCell>{t('DofusJobsPage:Recipe')}</TableCell>
							<TableCell>{t('DofusJobsPage:XP')}</TableCell>
							<TableCell>{t('DofusJobsPage:Actions')}</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{props.activeJob.crafts.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((item: JobItem) => (
							<TableRow key={item.id}>
								<TableCell component="td" scope="row">
									<Typography variant="caption">{item.level}</Typography>
								</TableCell>
								<TableCell component="td" scope="row">
									<img src={String(`/images/items/${item.id}.png`)} alt={t(`DofusItem:${item.name_id}`)} className={classes.itemImage} />
									<Typography variant="caption">{t(`DofusItem:${item.name_id}`)}</Typography>
								</TableCell>
								<TableCell component="td" scope="row">
									<JobsLineRecipe item={item} multiplier={1} />
								</TableCell>
								<TableCell component="td" scope="row">
									<Typography>
										{JobsFacade.getXpFromCraft(props.currentXp, props.multiplicator, item)}
									</Typography>
								</TableCell>
								<TableCell component="td" scope="row">
									<Tooltip title={String(t('DofusJobsPage:Add a certain quantity'))}>
										<IconButton size="small" color="default" onClick={() => props.setActiveItem(item)}><AddCircleOutlineIcon color={item.level <= currentLevel ? 'primary' : 'secondary'} /></IconButton>
									</Tooltip>
									<Tooltip title={String(t('DofusJobsPage:Craft a single item'))}>
										<IconButton size="small" color="default" onClick={() => handleCraftQuantity(item, 1)}><PlusOneIcon color={item.level <= currentLevel ? 'primary' : 'secondary'} /></IconButton>
									</Tooltip>
									{
										currentLevel < 200 ? (
											<Tooltip title={String(t('DofusJobsPage:Craft to the next multiple of 10'))}>
												<IconButton size="small" color="default" onClick={() => handleCraftToLevel(item, getNextNthLevel(JobsFacade.getLevelFromXp(props.currentXp)))}><SkipNextIcon color={item.level <= currentLevel ? 'primary' : 'secondary'} /></IconButton>
											</Tooltip>
										) : null
									}
								</TableCell>
							</TableRow>
						))}
					</TableBody>
				</Table>
			</TableContainer>
			<TablePagination
				rowsPerPageOptions={[5, 10, 20, 25]}
				component="div"
				count={props.activeJob.crafts.length}
				rowsPerPage={rowsPerPage}
				page={page}
				onPageChange={handleChangePage}
				onRowsPerPageChange={handleChangeRowsPerPage}
			/>
		</>
	) : null;
}
