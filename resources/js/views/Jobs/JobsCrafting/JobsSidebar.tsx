import {
	Grid, Tab, Tabs,
} from '@material-ui/core';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Craft } from '../../../interfaces';
import JobsSidebarCrafts from './JobsSidebarCrafts';
import JobsSidebarResources from './JobsSidebarResources';

interface ComponentProps {
	craftList: Craft[];
	removeItem: () => void;
}

export default function JobsSidebar(props: ComponentProps) {
	const { t, ready } = useTranslation(['DofusJobsPage']);

	const [value, setValue] = React.useState(0);

	const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
		setValue(newValue);
	};

	return ready ? (
		<Grid container>
			<Grid item xs={12}>
				<Tabs value={value} variant="fullWidth" onChange={handleChange}>
					<Tab label={t('DofusJobsPage:Crafts')} />
					<Tab label={t('DofusJobsPage:Resources')} />
				</Tabs>
				{
					value === 0
						? (
							<JobsSidebarCrafts removeItem={props.removeItem} craftList={props.craftList} />
						) : (
							<JobsSidebarResources craftList={props.craftList} />
						)
				}
			</Grid>
		</Grid>
	) : null;
}
