import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import { useTranslation } from 'react-i18next';
import { IconButton, ListItemSecondaryAction } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import JobsFacade from '../../../facades/JobsFacade';
import { Craft } from '../../../interfaces';

interface ComponentProps {
	craftList: Craft[];
	removeItem: () => void;
}

export default function JobsSidebarCrafts(props: ComponentProps) {
	const { t, ready } = useTranslation(['DofusItem']);

	return ready ? (
		<List dense>
			{
				props.craftList.map((craft, i) => (
					// eslint-disable-next-line react/no-array-index-key
					<ListItem key={i}>
						<ListItemAvatar>
							<Avatar src={`/images/items/${craft.item.id}.png`} />
						</ListItemAvatar>
						<ListItemText primary={String(`${t(`DofusItem:${craft.item.name_id}`)} x${craft.quantity}`)} secondary={`Niveau ${JobsFacade.getLevelFromXp(craft.xp_total)} (${t('DofusJobsPage:Total')}: ${craft.xp_total} ${t('DofusJobsPage:XP')})`} />
						{
							i === props.craftList.length - 1
								? (
									<ListItemSecondaryAction>
										<IconButton edge="end" aria-label="delete" onClick={props.removeItem}>
											<DeleteIcon />
										</IconButton>
									</ListItemSecondaryAction>
								)
								: null
						}
					</ListItem>
				))
			}
		</List>
	) : null;
}
