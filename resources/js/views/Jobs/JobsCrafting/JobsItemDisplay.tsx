import {
	Avatar, Button, Chip, Grid, makeStyles, TextField,
} from '@material-ui/core';
import _ from 'lodash';
import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import JobsFacade from '../../../facades/JobsFacade';
import { JobItem } from '../../../interfaces/Job';
import JobsLineRecipe from './JobsLineRecipe';

interface ComponentProps {
	activeItem: JobItem | undefined;
	currentXp: number;
	multiplicator: number;
	craftItem: (item: JobItem, quantity: number, xp_gained: number) => void;
}

const useStyles = makeStyles((theme) => ({
	marginRight: {
		marginRight: theme.spacing(1),
	},
	input: {
		width: 110,
	},
}));

export default function JobsItemDisplay(props: ComponentProps) {
	const classes = useStyles();

	const [quantity, setQuantity] = useState(1);
	const [targetLevel, setTargetLevel] = useState(1);
	const [quantityToCraft, setQuantityToCraft] = useState(0);
	const [xpAfterCraft, setXpAfterCraft] = useState(0);
	const [isQuantitySelected, setIsQuantitySelected] = useState(false);
	const [isLevelSelected, setIsLevelSelected] = useState(false);

	const { t, ready } = useTranslation(['DofusItem', 'DofusJobsPage']);

	const handleLevelChanged = () => {
		if (props.activeItem) {
			handleQuantityChanged(JobsFacade.getQuantityForLevel(targetLevel, props.currentXp, props.multiplicator, props.activeItem), false);
		}
	};

	const delayedQuery = useCallback(_.debounce(handleLevelChanged, 500), [targetLevel]);

	useEffect(() => {
		delayedQuery();

		return delayedQuery.cancel;
	}, [targetLevel, delayedQuery]);

	function handleQuantityChanged(newQuantity: number, fromUser: boolean) {
		setQuantity(newQuantity);
		setQuantityToCraft(newQuantity);
		if (props.activeItem) setXpAfterCraft(JobsFacade.getXpForQuantity(newQuantity, props.currentXp, props.multiplicator, props.activeItem));
		if (fromUser) setIsQuantitySelected(!!newQuantity);
	}

	function handleTargetLevelChanged(level: number, fromUser: boolean) {
		setTargetLevel(level);
		if (fromUser) setIsLevelSelected(!!level);
	}

	const handleCraftClick = () => {
		if (props.activeItem) { props.craftItem(props.activeItem, quantityToCraft, xpAfterCraft - props.currentXp); }
	};

	useEffect(() => {
		if (props.activeItem) {
			if (isQuantitySelected) {
				handleQuantityChanged(quantityToCraft, false);
			} else if (isLevelSelected) {
				handleLevelChanged();
			}
		}
	}, [props.activeItem]);

	useEffect(() => {
		if (isQuantitySelected) {
			handleQuantityChanged(quantityToCraft, false);
		} else if (isLevelSelected) {
			handleLevelChanged();
		}
	}, [props.currentXp]);

	return ready && props.activeItem ? (
		<Grid container spacing={1}>
			<Grid item xs={12} md={6}>
				<Chip
					avatar={<Avatar src={String(`/images/items/${props.activeItem.id}.png`)} />}
					label={_.truncate(t(`DofusItem:${props.activeItem?.name_id}`), { length: 20 })}
					className={classes.marginRight}
				/>
				<JobsLineRecipe item={props.activeItem} multiplier={quantity} />
			</Grid>
			<Grid item xs={12} md={2}>
				{
					isLevelSelected ? (
						<>
							<TextField
								label={t('DofusJobsPage:Quantity')}
								type="number"
								variant="outlined"
								size="small"
								InputLabelProps={{
									shrink: true,
								}}
								disabled
								fullWidth
								value={quantityToCraft}
							/>
						</>
					) : (
						<TextField
							label={t('DofusJobsPage:Quantity')}
							InputProps={{ inputProps: { min: 1 } }}
							type="number"
							variant="outlined"
							size="small"
							InputLabelProps={{
								shrink: true,
							}}
							fullWidth
							onFocus={() => setIsQuantitySelected(true)}
							onBlur={(e) => setIsQuantitySelected(!!e.target.value)}
							onChange={(e) => handleQuantityChanged(parseInt(e.target.value, 10) || 0, true)}
						/>
					)
				}
			</Grid>
			<Grid item xs={12} md={2}>
				{
					isQuantitySelected ? (
						<>
							<TextField
								label={t('DofusJobsPage:Level')}
								type="number"
								variant="outlined"
								size="small"
								InputLabelProps={{
									shrink: true,
								}}
								fullWidth
								disabled
								value={JobsFacade.getLevelFromXp(xpAfterCraft)}
							/>
						</>
					) : (
						<TextField
							label={t('DofusJobsPage:Level')}
							InputProps={{ inputProps: { max: 200, min: JobsFacade.getLevelFromXp(props.currentXp) + 1 } }}
							type="number"
							variant="outlined"
							size="small"
							InputLabelProps={{
								shrink: true,
							}}
							fullWidth
							onFocus={() => setIsLevelSelected(true)}
							onBlur={(e) => setIsLevelSelected(!!e.target.value)}
							onChange={(e) => handleTargetLevelChanged(parseInt(e.target.value, 10) || 0, true)}
						/>
					)
				}
			</Grid>
			<Grid item xs={12} md={2}>
				<Button variant="contained" color="primary" onClick={handleCraftClick} fullWidth>
					{t('DofusJobsPage:Craft')}
				</Button>
			</Grid>
		</Grid>
	) : null;
}
