import {
	Badge, createStyles, makeStyles, Theme, Tooltip, withStyles,
} from '@material-ui/core';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { RecipeLine } from '../../../interfaces';
import { JobItem } from '../../../interfaces/Job';

const StyledBadge = withStyles((theme: Theme) => createStyles({
	badge: {
		right: -1,
		top: 18,
		border: `2px solid ${theme.palette.background.paper}`,
		padding: '0 2px',
		fontSize: 10,
	},
}))(Badge);

const useStyles = makeStyles((theme: Theme) => createStyles({
	itemRecipe: {
		marginRight: theme.spacing(1),
		left: -4,
	},
	itemImage: {
		width: '2em',
	},
}));

interface ComponentProps {
	item: JobItem,
	multiplier: number | undefined
}

export default function JobsLineRecipe(props: ComponentProps) {
	const classes = useStyles();

	const { t, ready } = useTranslation(['DofusItem']);

	return ready ? (
		<>
			{
				props.item.recipe.map((line: RecipeLine) => (
					<Tooltip key={line.needed_id} title={String(`${t(`DofusItem:${line.needed.name_id}`)} x${line.quantity}`)}>
						<StyledBadge max={999999} badgeContent={props.multiplier ? props.multiplier * line.quantity : line.quantity} className={classes.itemRecipe} color="secondary">
							<img src={String(`/images/items/${line.needed_id}.png`)} alt={t(`DofusItem:${line.needed.name_id}`)} className={classes.itemImage} />
						</StyledBadge>
					</Tooltip>
				))
			}
		</>
	) : null;
}
