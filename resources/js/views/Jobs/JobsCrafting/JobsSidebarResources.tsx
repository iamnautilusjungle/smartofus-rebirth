import React, { useEffect, useState } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import { useTranslation } from 'react-i18next';
import { Craft, RecipeLine, ResourceLine } from '../../../interfaces';

interface ComponentProps {
	craftList: Craft[];
}

export default function JobsSidebarResources(props: ComponentProps) {
	const { t, ready } = useTranslation(['DofusItem']);

	const [resourceList, setResourceList] = useState<Map<number, ResourceLine>>(new Map());

	useEffect(() => {
		setResourceList(props.craftList.reduce((previous: Map<number, ResourceLine>, craft: Craft) => {
			craft.item.recipe.forEach((recipe: RecipeLine) => {
				previous.set(recipe.needed_id, {
					item: recipe.needed,
					quantity: (previous.get(recipe.needed_id)?.quantity || 0) + recipe.quantity * craft.quantity,
				} as ResourceLine);
			});

			return previous;
		}, new Map()));
	}, [props.craftList]);

	return ready ? (
		<List dense>
			{
				Array.from(resourceList.values()).map((resource: ResourceLine) => (
					<ListItem key={resource.item.id}>
						<ListItemAvatar>
							<Avatar src={`/images/items/${resource.item.id}.png`} />
						</ListItemAvatar>
						<ListItemText primary={String(`${t(`DofusItem:${resource.item.name_id}`)} x${resource.quantity}`)} />
					</ListItem>
				))
			}
		</List>
	) : null;
}
