import { Grid, makeStyles } from '@material-ui/core';
import React from 'react';
import TofuCard from '../../../components/TofuCard/TofuCard';
import { Craft, JobItem } from '../../../interfaces/Job';
import { ActiveJob } from '../Jobs.types';
import JobsItemDisplay from './JobsItemDisplay';
import JobsRecipes from './JobsRecipes';
import JobsSidebar from './JobsSidebar';

interface ComponentProps {
	activeJob: ActiveJob;
	currentXp: number;
	multiplicator: number;
	activeItem: JobItem | undefined;
	setActiveItem: React.Dispatch<React.SetStateAction<JobItem | undefined>>;
	craftItem: (item: JobItem, quantity: number, xp_gained: number) => void;
	craftList: Craft[];
	removeItem: () => void;
}

const useStyles = makeStyles((theme) => ({
	marginGrid: {
		marginBottom: theme.spacing(1),
	},
}));

export default function JobsCrafting(props: ComponentProps) {
	const classes = useStyles();

	return (
		<Grid container spacing={1}>
			<Grid item xs={12} lg={8}>
				<Grid item xs={12} className={classes.marginGrid}>
					<TofuCard>
						<JobsItemDisplay craftItem={props.craftItem} currentXp={props.currentXp} multiplicator={props.multiplicator} activeItem={props.activeItem} />
					</TofuCard>
				</Grid>
				<Grid item xs={12}>
					<JobsRecipes craftItem={props.craftItem} activeJob={props.activeJob} currentXp={props.currentXp} multiplicator={props.multiplicator} setActiveItem={props.setActiveItem} />
				</Grid>
			</Grid>
			<Grid item xs={12} lg={4}>
				<TofuCard>
					<JobsSidebar removeItem={props.removeItem} craftList={props.craftList} />
				</TofuCard>
			</Grid>
		</Grid>
	);
}
