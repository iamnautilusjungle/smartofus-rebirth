import { atom } from 'recoil';
import IUser from '../types/IUser';

const AuthUserAtom = atom<IUser | undefined>({
	key: 'AuthUserAtom', // unique ID (with respect to other atoms/selectors)
	default: undefined, // default value (aka initial value)
});

export default AuthUserAtom;
