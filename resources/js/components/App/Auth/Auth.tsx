import React from 'react';
import {
	Button, Menu, MenuItem, Typography,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import GoogleLogin from 'react-google-login';
import { useSnackbar } from 'notistack';
import { useRecoilState } from 'recoil';
import UserFacade from '../../../facades/UserFacade';
import LoginRegisterForm from './LoginRegisterForm';
import AuthUserAtom from '../../../atoms/AuthUser';

export default function Auth() {
	const { enqueueSnackbar } = useSnackbar();
	const { t, ready } = useTranslation();

	const [user, setUser] = useRecoilState(AuthUserAtom);
	const [open, setOpen] = React.useState(false);
	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

	// Logged In

	const handleClickLoggedInMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
		setAnchorEl(event.currentTarget);
	};

	const handleCloseLoggedInMenu = () => {
		setAnchorEl(null);
	};

	const handleLogout = () => {
		handleCloseLoggedInMenu();
		UserFacade.logout().then(() => {
			enqueueSnackbar(
				t('translation:disconnection.successful'),
				{ variant: 'success', autoHideDuration: 5000 },
			);
			setUser(undefined);
		}).catch(() => {
			enqueueSnackbar(
				t('translation:disconnection.unsuccessful'),
				{ variant: 'error', autoHideDuration: 5000 },
			);
		});
	};

	const handleClose = () => {
		setOpen(false);
	};

	const handleClick = () => {
		if (user) {
			setUser(undefined);
		} else {
			setOpen(true);
		}
	};

	const handleManualLogin = (email: string, password: string) => {
		UserFacade.loginManually(email, password).then((r) => {
			setUser(r.data);
			afterLoginSuccess();
		}).catch(() => {
			afterLoginFail();
		});
	};

	const afterLoginSuccess = (): void => {
		enqueueSnackbar(
			t('translation:connection.successful'),
			{ variant: 'success', autoHideDuration: 5000 },
		);
		setOpen(false);
	};

	const afterLoginFail = (): void => {
		enqueueSnackbar(
			t('translation:connection.unsuccessful'),
			{ variant: 'error', autoHideDuration: 5000 },
		);
	};

	const handleManualRegister = (email: string, username: string, password: string, password_confirmation: string) => {
		UserFacade.registerManually(email, username, password, password_confirmation).then((r) => {
			setUser(r.data);
			afterLoginSuccess();
		}).catch(() => {
			afterLoginFail();
		});
	};

	const handleGoogleLogin = (e: any) => {
		UserFacade.loginWithSso('google', {
			token: e.tokenObj.access_token,
		}).then((r) => {
			setUser(r.data);
			afterLoginSuccess();
		}).catch(() => {
			afterLoginFail();
		});
	};

	return ready ? (
		<>
			<Button onClick={(e) => (user ? handleClickLoggedInMenu(e) : handleClick())}><Typography>{user?.name || t('translation:Login')}</Typography></Button>
			{/* Logged In Menu */}
			<Menu
				anchorEl={anchorEl}
				keepMounted
				open={Boolean(anchorEl)}
				onClose={handleCloseLoggedInMenu}
			>
				<MenuItem onClick={handleCloseLoggedInMenu}>Profile</MenuItem>
				<MenuItem onClick={handleCloseLoggedInMenu}>My account</MenuItem>
				<MenuItem onClick={handleLogout}>Logout</MenuItem>
			</Menu>
			{/* Logged Out Menu */}
			<Dialog open={open} onClose={handleClose}>
				<DialogTitle>Login</DialogTitle>
				<DialogContent>
					<LoginRegisterForm handleManualLogin={handleManualLogin} handleManualRegister={handleManualRegister} />
					<GoogleLogin
						clientId="434388111959-bbinv4511nbi07epb5c0saohbtjnvrp0.apps.googleusercontent.com"
						buttonText="Login"
						onSuccess={handleGoogleLogin}
						onFailure={handleGoogleLogin}
						cookiePolicy="single_host_origin"
					/>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose} color="primary">
						Annuler
					</Button>
				</DialogActions>
			</Dialog>
		</>
	) : null;
}
