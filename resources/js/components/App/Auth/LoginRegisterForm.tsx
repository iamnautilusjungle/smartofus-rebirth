import React from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import { Button, Grid, TextField } from '@material-ui/core';

interface TabPanelProps {
	children: React.ReactNode | undefined;
	index: any;
	value: any;
}

function TabPanel(props: TabPanelProps) {
	const {
		children, value, index, ...other
	} = props;

	return (
		<div
			role="tabpanel"
			hidden={value !== index}
			id={`simple-tabpanel-${index}`}
			aria-labelledby={`simple-tab-${index}`}
			{...other}
		>
			{value === index && (
				<Box py={3}>
					{children}
				</Box>
			)}
		</div>
	);
}

function a11yProps(index: any) {
	return {
		id: `simple-tab-${index}`,
		'aria-controls': `simple-tabpanel-${index}`,
	};
}

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		flexGrow: 1,
		backgroundColor: theme.palette.background.paper,
	},
}));

interface ComponentProps {
	handleManualLogin: (email: string, password: string) => void;
	handleManualRegister: (email: string, username: string, password: string, password_confirmation: string) => void;
}

export default function LoginRegisterForm(props: ComponentProps) {
	const classes = useStyles();
	const [value, setValue] = React.useState(0);

	// Login
	const [loginEmail, setLoginEmail] = React.useState('');
	const [loginPassword, setLoginPassword] = React.useState('');

	// Register
	const [registerEmail, setRegisterEmail] = React.useState('');
	const [registerUsername, setRegisterUsername] = React.useState('');
	const [registerPassword, setRegisterPassword] = React.useState('');
	const [registerPasswordConfirmation, setRegisterPasswordConfirmation] = React.useState('');

	const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
		setValue(newValue);
	};

	const handleLogin = () => {
		props.handleManualLogin(loginEmail, loginPassword);
	};

	const handleRegister = () => {
		props.handleManualRegister(registerEmail, registerUsername, registerPassword, registerPasswordConfirmation);
	};

	return (
		<div className={classes.root}>
			<AppBar position="static">
				<Tabs value={value} onChange={handleChange} variant="fullWidth">
					<Tab label="Connexion" {...a11yProps(0)} />
					<Tab label="Inscription" {...a11yProps(1)} />
				</Tabs>
			</AppBar>
			<TabPanel value={value} index={0}>
				<Grid container spacing={1}>
					<Grid item xs={12}>
						<TextField label="Email" fullWidth onChange={(e) => setLoginEmail(e.target.value)} />
					</Grid>
					<Grid item xs={12}>
						<TextField label="Password" fullWidth type="password" onChange={(e) => setLoginPassword(e.target.value)} />
					</Grid>
					<Grid item xs={12}>
						<Button variant="contained" onClick={handleLogin} fullWidth>Login</Button>
					</Grid>
				</Grid>
			</TabPanel>
			<TabPanel value={value} index={1}>
				<Grid container spacing={1}>
					<Grid item xs={12}>
						<TextField label="Email" fullWidth onChange={(e) => setRegisterEmail(e.target.value)} />
					</Grid>
					<Grid item xs={12}>
						<TextField label="Username" fullWidth onChange={(e) => setRegisterUsername(e.target.value)} />
					</Grid>
					<Grid item xs={12}>
						<TextField label="Password" fullWidth type="password" onChange={(e) => setRegisterPassword(e.target.value)} />
					</Grid>
					<Grid item xs={12}>
						<TextField label="Password Confirmation" fullWidth type="password" onChange={(e) => setRegisterPasswordConfirmation(e.target.value)} />
					</Grid>
					<Grid item xs={12}>
						<Button variant="contained" fullWidth onClick={handleRegister}>Login</Button>
					</Grid>
				</Grid>
			</TabPanel>
		</div>
	);
}
