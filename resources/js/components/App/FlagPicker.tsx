import React, { useState } from 'react';
import {
	createStyles, IconButton, makeStyles, Menu, MenuItem,
} from '@material-ui/core';

import {
	IconFlagFR,
	IconFlagUK,
	IconFlagES,
} from 'material-ui-flags';
import i18n from '../../i18n';

const useStyles = makeStyles(() => createStyles({
	icon: {
		paddingTop: 0,
		paddingBottom: 0,
	},
}));

enum Locale {
	FR = 'fr',
	EN = 'en',
	ES = 'es',
}

export default function FlagPicker() {
	const classes = useStyles();

	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
	const [localeState, setLocaleState] = useState<Locale>(Locale.FR);

	const open = Boolean(anchorEl);

	const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	const setLocale = (locale: Locale) => {
		setLocaleState(locale);
		i18n.changeLanguage(locale);
		handleClose();
	};

	return (
		<div>
			<IconButton
				aria-label="account of current user"
				aria-controls="menu-appbar"
				aria-haspopup="true"
				onClick={handleMenu}
				color="inherit"
			>
				{
					localeState === Locale.FR ? (
						<IconFlagFR />
					) : localeState === Locale.EN ? (
						<IconFlagUK />
					) : localeState === Locale.ES ? (
						<IconFlagES />
					) : (<IconFlagFR />)
				}
			</IconButton>
			<Menu
				id="menu-appbar"
				anchorEl={anchorEl}
				anchorOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
				keepMounted
				transformOrigin={{
					vertical: 'top',
					horizontal: 'right',
				}}
				open={open}
				onClose={handleClose}
			>
				<MenuItem onClick={() => setLocale(Locale.FR)}>
					<IconButton className={classes.icon}><IconFlagFR /></IconButton>
				</MenuItem>
				<MenuItem onClick={() => setLocale(Locale.EN)}>
					<IconButton className={classes.icon}><IconFlagUK /></IconButton>
				</MenuItem>
			</Menu>
		</div>
	);
}
