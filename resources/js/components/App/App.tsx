import React, { useLayoutEffect } from 'react';
import clsx from 'clsx';
import {
	createStyles, makeStyles, createTheme, Theme,
} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Redirect,
	RouteProps,
	Link as RouterLink, LinkProps as RouterLinkProps,
} from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import {
	Box, CircularProgress, ThemeProvider,
} from '@material-ui/core';
import { SnackbarProvider } from 'notistack';
import { useRecoilState } from 'recoil';
import routes, { IRoute } from '../../consts/routes';
import FlagPicker from './FlagPicker';
import Auth from './Auth/Auth';

import Home from '../../views/Home/Home';
import Jobs from '../../views/Jobs/Jobs';
import StuffsIndex from '../../views/Stuffs/Index/StuffsIndex';
import IUser from '../../types/IUser';
import StuffsCreate from '../../views/Stuffs/Create/StuffsCreate';
import StuffsEdit from '../../views/Stuffs/Edit/StuffsEdit';
import UserFacade from '../../facades/UserFacade';

import AuthUserAtom from '../../atoms/AuthUser';
import StuffsShow from '../../views/Stuffs/Show/StuffsShow';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) => createStyles({
	root: {
		display: 'flex',
		overflow: 'hidden',
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
	},
	appBarShift: {
		marginLeft: drawerWidth,
		width: `calc(100% - ${drawerWidth}px)`,
		transition: theme.transitions.create(['width', 'margin'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen,
		}),
	},
	menuButton: {
		marginRight: 36,
	},
	hide: {
		display: 'none',
	},
	drawer: {
		width: drawerWidth,
		flexShrink: 0,
		whiteSpace: 'nowrap',
	},
	drawerOpen: {
		width: drawerWidth,
		transition: theme.transitions.create('width', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen,
		}),
	},
	drawerClose: {
		transition: theme.transitions.create('width', {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen,
		}),
		overflowX: 'hidden',
		width: theme.spacing(7) + 1,
		[theme.breakpoints.up('sm')]: {
			width: theme.spacing(9) + 1,
		},
	},
	toolbar: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'flex-end',
		padding: theme.spacing(0, 1),
		// necessary for content to be below app bar
		...theme.mixins.toolbar,
	},
	content: {
		flexGrow: 1,
	},
	title: {
		flexGrow: 1,
	},
	child: {
		borderLeft: '3px solid',
		borderColor: theme.palette.primary.main,
	},
}));

function ListItemLink(props: IRoute) {
	const { icon, id, link } = props;

	const classes = useStyles();

	const { t } = useTranslation();

	const renderLink = React.useMemo(
		() => React.forwardRef<any, Omit<RouterLinkProps, 'to'>>((itemProps, ref) => (
			<RouterLink to={link} ref={ref} {...itemProps} />
		)),
		[link],
	);

	return (
		<li>
			<ListItem dense button component={renderLink}>
				{icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
				<ListItemText primary={t(`translation:${id}`)} />
			</ListItem>
			{
				props.children ? (
					<Box pl={1}>
						<li className={classes.child}>
							{
								props.children.map((child: IRoute, n: number) => (
									// eslint-disable-next-line react/no-array-index-key
									<ListItemLink {...child} key={n} />
								))
							}
						</li>
					</Box>
				) : null
			}
		</li>
	);
}

const theme = createTheme({
	palette: {
		type: 'dark',
	},
}, {
	locales: 'frFR',
});

interface PrivateRouteProps extends RouteProps {
	user: IUser | undefined
}

function PrivateRoute(props: PrivateRouteProps) {
	return (
		<Route
			render={() => (props.user ? (
				props.children
			) : (
				<Redirect
					to={{
						pathname: '/login',
						state: { from: props.location },
					}}
				/>
			))}
		/>
	);
}

export default function App() {
	const classes = useStyles();
	const [open, setOpen] = React.useState(false);
	const { ready } = useTranslation();

	const handleDrawerOpen = () => {
		setOpen(true);
	};

	const handleDrawerClose = () => {
		setOpen(false);
	};

	const getRoutes = (): IRoute[] => routes;

	const [user, setUser] = useRecoilState(AuthUserAtom);

	// useEffect(() => {
	//     switch (i18n.language) {
	//         case "fr":
	//             setLocale("frFR")
	//         case "en":
	//             setLocale("enUS")
	//     }
	// }, [i18n.language])

	useLayoutEffect(() => {
		UserFacade.getUserFromCache().then((r) => {
			setUser(r.data);
		});
	}, []);

	return (
		<SnackbarProvider dense>
			<ThemeProvider theme={theme}>
				<Router>
					<div className={classes.root}>
						<CssBaseline />
						<AppBar
							position="fixed"
							className={clsx(classes.appBar, {
								[classes.appBarShift]: open,
							})}
						>
							<Toolbar>
								<IconButton
									color="inherit"
									aria-label="open drawer"
									onClick={handleDrawerOpen}
									edge="start"
									className={clsx(classes.menuButton, {
										[classes.hide]: open,
									})}
								>
									<MenuIcon />
								</IconButton>
								<Typography variant="h6" noWrap className={classes.title}>
									Smartofus
								</Typography>
								<FlagPicker />
								<Auth />
							</Toolbar>
						</AppBar>
						<Drawer
							variant="permanent"
							className={clsx(classes.drawer, {
								[classes.drawerOpen]: open,
								[classes.drawerClose]: !open,
							})}
							classes={{
								paper: clsx({
									[classes.drawerOpen]: open,
									[classes.drawerClose]: !open,
								}),
							}}
						>
							<div className={classes.toolbar}>
								<IconButton onClick={handleDrawerClose}>
									{theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
								</IconButton>
							</div>
							<Divider />
							<List>
								{getRoutes().map((route: IRoute) => (
									<ListItemLink key={route.id} {...route} />
								))}
							</List>
						</Drawer>
						<main className={classes.content}>
							<div className={classes.toolbar} />
							{
								ready ? (
									<Switch>
										<PrivateRoute exact path="/stuffs" user={user}>
											<StuffsIndex />
										</PrivateRoute>
										<Route exact path="/stuffs/create">
											<StuffsCreate />
										</Route>
										<Route exact path="/stuffs/:code/edit">
											<StuffsEdit />
										</Route>
										<Route exact path="/stuffs/:code">
											<StuffsShow />
										</Route>
										<Route exact path="/jobs">
											<Jobs />
										</Route>
										<Route exact path="/">
											<Home />
										</Route>
									</Switch>
								) : (
									<CircularProgress size="8em" />
								)
							}
						</main>
					</div>
				</Router>
			</ThemeProvider>
		</SnackbarProvider>
	);
}
