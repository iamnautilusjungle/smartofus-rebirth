interface ComponentProps {
	condition: boolean;
	children: JSX.Element;
}

export default function ConditionDisplay(props: ComponentProps) {
	return props.condition ? props.children : null;
}
