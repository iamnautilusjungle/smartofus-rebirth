import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

interface MiniBarProps {
	children: React.ReactNode
}

export default function MiniBar(props: MiniBarProps) {
	return (
		<AppBar position="sticky">
			<Toolbar variant="dense">
				{props.children}
			</Toolbar>
		</AppBar>
	);
}
