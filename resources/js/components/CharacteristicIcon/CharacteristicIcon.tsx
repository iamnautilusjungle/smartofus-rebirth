import React from 'react';

interface ComponentProps {
	icon: string
}

export default function CharacteristicIcon(props: ComponentProps) {
	return (
		<span style={{
			height: '20px',
			width: '20px',
			backgroundImage: `url('/images/darkStone/Characteristics/${props.icon}.png')`,
			display: 'block',
			backgroundSize: 'cover',
		}}
		/>
	);
}
