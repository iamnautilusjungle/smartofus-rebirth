import { Grid, makeStyles, Paper } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles((theme) => ({
	container: {
		padding: theme.spacing(1),
		flexGrow: 1,
	},
}));

export default function TofuCard(props: any) {
	const classes = useStyles();

	return (
		<Paper elevation={3}>
			<Grid container>
				<Grid item className={classes.container}>
					{props.children}
				</Grid>
			</Grid>
		</Paper>
	);
}
