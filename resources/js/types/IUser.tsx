export default interface IUser {
	id: number;
	name: string;
	email: string;
	title?: ITitle | undefined;
}

export interface ITitle {
	id: number;
	name: string;
}
