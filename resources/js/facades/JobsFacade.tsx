import { JobItem } from '../interfaces/Job';

export default class JobsFacade {
	static getLevelFromXp(xp: number): number {
		let level = 0;
		while (level < 200 && this.getXpFromLevel((++level) + 1) <= xp);
		return level;
	}

	static getXpFromLevel(level: number): number {
		return ((level) * (level - 1)) * 10;
	}

	static getXpForQuantity(quantity: number, baseCurrentXp: number, multiplicator: number, item: JobItem): number {
		let i;
		let level = JobsFacade.getLevelFromXp(baseCurrentXp);
		let previousLevel = level;
		let gainedXp = JobsFacade.getXpFromCraft(baseCurrentXp, multiplicator, item);

		let newCurrentXp = baseCurrentXp;

		for (i = 0; i < quantity; i++) {
			newCurrentXp += gainedXp;
			level = JobsFacade.getLevelFromXp(newCurrentXp);

			if (level !== previousLevel) {
				gainedXp = JobsFacade.getXpFromCraft(newCurrentXp, multiplicator, item);
				previousLevel = level;
			}
		}

		return newCurrentXp;
	}

	static getQuantityForLevel(targetLevel: number, baseCurrentXp: number, multiplicator: number, item: JobItem): number {
		let quantity = 0;
		let level = JobsFacade.getLevelFromXp(baseCurrentXp);
		let previousLevel = level;
		let gainedXp = JobsFacade.getXpFromCraft(baseCurrentXp, multiplicator, item);

		let newCurrentXp = baseCurrentXp;

		while (gainedXp > 0 && level < targetLevel) {
			newCurrentXp += gainedXp;
			level = JobsFacade.getLevelFromXp(newCurrentXp);

			if (level !== previousLevel) {
				gainedXp = JobsFacade.getXpFromCraft(newCurrentXp, multiplicator, item);
				previousLevel = level;
			}

			quantity++;
		}

		return quantity;
	}

	static getXpFromCraft(currentXp: number, multiplicator: number, item: JobItem): number {
		const currentLevel = this.getLevelFromXp(currentXp);
		if ((currentLevel - item.level) >= 101) return 0;
		if (currentLevel > item.level) {
			return Math.floor((item.craft_xp / (1 + 0.1 * (currentLevel - item.level) ** 1.1)) * multiplicator);
		}
		// Same level, no XP change
		return Math.floor(item.craft_xp! * multiplicator);
	}
}
