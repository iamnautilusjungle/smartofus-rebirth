import { EffectToCalculate } from '../interfaces/Stuff/EffectToCalculate';
import { MassDamageCalculator } from '../services/Stuff/DamageCalculator/MassDamageCalculator';
import { StuffEffects } from '../interfaces/Stuff/Stuff';
import {
	Damage, EffectPivot, StuffItem,
} from '../interfaces';
import { DamageFromEffectPivotsSeparator } from '../services/Stuff/DamageCalculator/DamageFromEffectPivotsSeparator';

export class DamageCalculatorFacade {
	public static calculateWeapon(stuffCalculatedEffects: StuffEffects, weapon: StuffItem): Damage[] {
		return (new MassDamageCalculator(stuffCalculatedEffects, this.getWeaponEffectsToCalculate(weapon))).get();
	}

	// public static calculateSpell(stuffCalculatedEffects: StuffEffects, spell: Spell): Damage[] {

	// }

	private static getWeaponEffectsToCalculate(weapon: StuffItem): EffectToCalculate[] {
		const pivots: EffectPivot[] = DamageFromEffectPivotsSeparator.getDamageFrom(weapon.effects.map((e) => e.pivot));
		return pivots.map((e) => ({
			id: e.effect_id,
			min: e.dice_num,
			max: e.dice_side,
			critical_hit_bonus: weapon.critical_hit_bonus,
		}));
	}
}
