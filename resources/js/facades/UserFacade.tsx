import axios from 'axios';

export default class UserFacade {
	static logout() {
		return axios.post('/logout');
	}

	static loginWithSso(driver: string, params: any) {
		return axios.post(`/login/${driver}`, params);
	}

	static loginManually(email: string, password: string) {
		return axios.post('/login', {
			email,
			password,
		});
	}

	static registerManually(email: string, username: string, password: string, password_confirmation: string) {
		return axios.post('/register', {
			email,
			name: username,
			password,
			password_confirmation,
		});
	}

	static getUserFromCache() {
		return axios.get('/web-api/user');
	}
}
