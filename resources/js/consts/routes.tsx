import WorkIcon from '@material-ui/icons/Work';
import FitnessCenterIcon from '@material-ui/icons/FitnessCenter';
import AddIcon from '@material-ui/icons/Add';
import React from 'react';

export interface IRoute {
	id: string;
	link: string;
	icon: React.ReactElement;
	children?: IRoute[];
}

const routes: Array<IRoute> = [
	{
		id: 'stuffs',
		link: '/stuffs',
		icon: <FitnessCenterIcon />,
		children: [
			{
				id: 'stuffs.create',
				link: '/stuffs/create',
				icon: <AddIcon />,
			},
		],
	},
	{
		id: 'jobs',
		link: '/jobs',
		icon: <WorkIcon />,
	},
];

export default routes;
