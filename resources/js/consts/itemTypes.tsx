import { EquipmentsIdsOfStuff, EquipmentsOfStuff } from '../interfaces/Stuff/EquipmentsOfStuff';

export const AMULET = 1;
export const BACKPACK = 81;
export const BELT = 10;
export const BOOTS = 11;
export const CLOAK = 17;
export const DOFUS = 23;
export const HAT = 16;
export const LIVING_OBJECT = 113;
export const RING = 9;
export const SHIELD = 82;
export const TROPHY = 151;

export const AXE = 19;
export const BOW = 2;
export const DAGGER = 5;
export const HAMMER = 7;
export const PICKAXE = 21;
export const SCYTHE = 22;
export const SHOVEL = 8;
export const SOUL_STONE = 83;
export const STAFF = 4;
export const SWORD = 6;
export const TOOL = 20;
export const WAND = 3;

export const PET = 18;
export const PETSMOUNT = 121;
export const DRAGOTURKEY = 97;
export const RHINEETLE = 207;
export const SEEMYOOL = 196;

export const STUFF_ITEM_TYPES = {
	amulet: [AMULET],
	ring_one: [RING],
	ring_two: [RING],
	weapon: [
		AXE,
		BOW,
		HAMMER,
		PICKAXE,
		SCYTHE,
		SHOVEL,
		SOUL_STONE,
		STAFF,
		SWORD,
		TOOL,
		WAND,
		DAGGER,
	],
	shield: [SHIELD],
	hat: [HAT],
	cloak: [CLOAK, BACKPACK],
	belt: [BELT],
	boots: [BOOTS],
	pet: [PET, PETSMOUNT, DRAGOTURKEY, RHINEETLE, SEEMYOOL],
	trophy_one: [TROPHY, DOFUS],
	trophy_two: [TROPHY, DOFUS],
	trophy_three: [TROPHY, DOFUS],
	trophy_four: [TROPHY, DOFUS],
	trophy_five: [TROPHY, DOFUS],
	trophy_six: [TROPHY, DOFUS],
};

export function getTypeFromStuffString(type: keyof EquipmentsOfStuff): number[] {
	return STUFF_ITEM_TYPES[type];
}

export function getTypeIdFromType(type: keyof EquipmentsOfStuff): keyof EquipmentsIdsOfStuff {
	return (`${type}_id`) as keyof EquipmentsIdsOfStuff;
}
