import { getTypeFromStuffString, STUFF_ITEM_TYPES } from './itemTypes';

describe('itemTypes', () => {
	test('getTypeFromStuffString should work with valid value', () => {
		expect(getTypeFromStuffString('amulet')).toEqual(STUFF_ITEM_TYPES.amulet);
		expect(getTypeFromStuffString('pet')).toEqual(STUFF_ITEM_TYPES.pet);
	});
});
