export const HIT_POINTS = {
	id: 800,
	name_id: 501949,
	icon: 'tx_vitality',
};

export const PROSPECTING = {
	id: 176,
	name_id: 505866,
	icon: 'tx_prospecting',
};

export const INITIATIVE = {
	id: 44,
	name_id: 501918,
	icon: 'tx_initiative',
};

export const SUMMONABLE_CREATURES = {
	id: 26,
	name_id: 501880,
	icon: 'tx_summonableCreaturesBoost',
};

export const HEAL = {
	id: 49,
	name_id: 501914,
	icon: 'tx_heal',
};

export const WISDOM = {
	id: 124,
	name_id: 501946,
	icon: 'tx_wisdom',
};

export const VITALITY = {
	id: 125,
	name_id: 501947,
	icon: 'tx_vitality',
};

export const STRENGTH = {
	id: 118,
	name_id: 501945,
	icon: 'tx_strength',
};

export const INTELLIGENCE = {
	id: 126,
	name_id: 501944,
	icon: 'tx_intelligence',
};

export const CHANCE = {
	id: 123,
	name_id: 501942,
	icon: 'tx_chance',
};

export const AGILITY = {
	id: 119,
	name_id: 501941,
	icon: 'tx_agility',
};

export const POWER = {
	id: 138,
	name_id: 501883,
	icon: 'tx_stamina',
};

export const CRITICAL = {
	id: 115,
	name_id: 846702,
	icon: 'tx_crit',
};

export const RANGE = {
	id: 117,
	name_id: 501940,
	icon: 'tx_range',
};

export const AP = {
	id: 120,
	name_id: 501948,
	icon: 'tx_actionPoints',
};

export const DODGE = {
	id: 752,
	name_id: 501919,
	icon: 'tx_escape',
};

export const MINUS_DODGE = {
	id: 754,
	name_id: 501919,
	icon: 'tx_escape',
};

export const LOCK = {
	id: 753,
	name_id: 501917,
	icon: 'tx_tackle',
};

export const MINUS_LOCK = {
	id: 755,
	name_id: 501917,
	icon: 'tx_tackle',
};

export const AP_PARRY = {
	id: 160,
	name_id: 846698,
	icon: 'tx_dodgeAP',
};

export const MP_PARRY = {
	id: 161,
	name_id: 846700,
	icon: 'tx_dodgeMP',
};

export const AP_REDUCTION = {
	id: 410,
	name_id: 846697,
	icon: 'tx_attackAP',
};

export const MP_REDUCTION = {
	id: 412,
	name_id: 846699,
	icon: 'tx_attackMP',
};

export const PODS = {
	id: 158,
	name_id: 714556,
	icon: 'tx_energy',
};

export const NEUTRAL_DAMAGE = {
	id: 430,
	name_id: 501887,
	icon: 'tx_res_neutral',
};

export const AIR_DAMAGE = {
	id: 428,
	name_id: 501886,
	icon: 'tx_res_air',
};

export const WATER_DAMAGE = {
	id: 426,
	name_id: 501888,
	icon: 'tx_res_water',
};

export const FIRE_DAMAGE = {
	id: 424,
	name_id: 501857,
	icon: 'tx_res_fire',
};

export const EARTH_DAMAGE = {
	id: 422,
	name_id: 501859,
	icon: 'tx_res_earth',
};

export const PERCENT_MELEE_DAMAGE = {
	id: 2800,
	name_id: 723366,
	icon: 'tx_weaponDamage',
};

export const PERCENT_MELEE_RESISTANCE = {
	id: 2803,
	name_id: 723366,
	icon: 'icon_shield_cadre',
};

export const PERCENT_RANGED_DAMAGE = {
	id: 2804,
	name_id: 723361,
	icon: 'tx_weaponDamage',
};

export const PERCENT_RANGED_RESISTANCE = {
	id: 2807,
	name_id: 723361,
	icon: 'icon_shield_cadre',
};

export const PERCENT_WEAPON_DAMAGE = {
	id: 2808,
	name_id: 723363,
	icon: 'tx_weaponDamage',
};

export const PERCENT_WEAPON_RESISTANCE = {
	id: 2811,
	name_id: 723363,
	icon: 'icon_shield_cadre',
};

export const PERCENT_SPELL_DAMAGE = {
	id: 2812,
	name_id: 723364,
	icon: 'tx_weaponDamage',
};

export const PERCENT_SPELL_RESISTANCE = {
	id: 2815,
	name_id: 723364,
	icon: 'icon_shield_cadre',
};

export const CRITICAL_DAMAGE = {
	id: 418,
	name_id: 501855,
	icon: 'tx_criticalDamage',
};

export const CRITICAL_RESISTANCE = {
	id: 420,
	name_id: 501853,
	icon: 'tx_criticalReduction',
};

export const PUSHBACK_DAMAGE = {
	id: 414,
	name_id: 501871,
	icon: 'tx_push',
};

export const PUSHBACK_RESISTANCE = {
	id: 416,
	name_id: 501872,
	icon: 'tx_pushReduction',
};

export const EARTH_RESISTANCE = {
	id: 240,
	name_id: 501859,
	icon: 'tx_res_earth',
};

export const FIRE_RESISTANCE = {
	id: 243,
	name_id: 501857,
	icon: 'tx_res_fire',
};

export const WATER_RESISTANCE = {
	id: 241,
	name_id: 501888,
	icon: 'tx_res_water',
};

export const AIR_RESISTANCE = {
	id: 242,
	name_id: 501886,
	icon: 'tx_res_air',
};

export const NEUTRAL_RESISTANCE = {
	id: 244,
	name_id: 501887,
	icon: 'tx_res_neutral',
};

export const PERCENT_EARTH_RESISTANCE = {
	id: 210,
	name_id: 501856,
	icon: 'tx_res_earth',
};

export const PERCENT_WATER_RESISTANCE = {
	id: 211,
	name_id: 501860,
	icon: 'tx_res_water',
};

export const PERCENT_AIR_RESISTANCE = {
	id: 212,
	name_id: 501862,
	icon: 'tx_res_air',
};

export const PERCENT_FIRE_RESISTANCE = {
	id: 213,
	name_id: 501858,
	icon: 'tx_res_fire',
};

export const PERCENT_NEUTRAL_RESISTANCE = {
	id: 214,
	name_id: 501863,
	icon: 'tx_res_neutral',
};
