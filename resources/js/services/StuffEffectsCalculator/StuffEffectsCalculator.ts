import { EquipmentsOfStuff } from '../../interfaces/Stuff/EquipmentsOfStuff';
import {
	CHANCE, AGILITY, INTELLIGENCE, STRENGTH, VITALITY, WISDOM, HIT_POINTS, PROSPECTING, PODS, DODGE, LOCK, AP_PARRY, AP_REDUCTION, MP_PARRY, MP_REDUCTION, HEAL, INITIATIVE,
} from '../../consts/characteristics';
import { Stuff, Effect, StuffItem } from '../../interfaces';

export class StuffEffectsCalculator {
	stuff: Stuff;

	totalEffects: Map<number, number>;

	constructor(stuff: Stuff) {
		this.stuff = stuff;
		this.totalEffects = new Map();
	}

	getEffectsMap(): Map<number, number> {
		this.totalEffects = this.getItemsEffects();

		this.applyCharacteristics();
		this.applyBonusStats();

		return this.totalEffects;
	}

	protected getItemsEffects(): Map<number, number> {
		const ret = new Map<number, number>();

		this.getStuffItems().forEach((item: StuffItem) => {
			item.effects.forEach((effect: Effect) => {
				if (effect.pivot.dice_side !== 0) {
					ret.set(effect.pivot.effect_id, (ret.get(effect.pivot.effect_id) || 0) + effect.pivot.dice_side);
				} else {
					ret.set(effect.pivot.effect_id, (ret.get(effect.pivot.effect_id) || 0) + effect.pivot.dice_num);
				}
			});
		});

		return ret;
	}

	protected getStuffItems(): StuffItem[] {
		const ret: StuffItem[] = [];

		const equipmentKeys = [
			'amulet',
			'ring_one',
			'ring_two',
			'weapon',
			'shield',
			'hat',
			'cloak',
			'belt',
			'boots',
			'pet',
			'trophy_one',
			'trophy_two',
			'trophy_three',
			'trophy_four',
			'trophy_five',
			'trophy_six',
		];

		equipmentKeys.forEach((type: string) => {
			const translatedType = type as keyof EquipmentsOfStuff;
			const item = this.stuff[translatedType];
			if (item) ret.push(item);
		});

		return ret;
	}

	protected addStats(added: Map<number, number>): Map<number, number> {
		added.forEach((v, k) => {
			this.totalEffects.set(k, (this.totalEffects.get(k) || 0) + v);
		});

		return this.totalEffects;
	}

	protected applyCharacteristics(): Map<number, number> {
		const buffer = new Map<number, number>();

		buffer.set(VITALITY.id, (this.stuff.vitality_base || 0) + (this.stuff.vitality_parchment || 0));
		buffer.set(WISDOM.id, (this.stuff.wisdom_base || 0) + (this.stuff.wisdom_parchment || 0));
		buffer.set(STRENGTH.id, (this.stuff.strength_base || 0) + (this.stuff.strength_parchment || 0));
		buffer.set(INTELLIGENCE.id, (this.stuff.intelligence_base || 0) + (this.stuff.intelligence_parchment || 0));
		buffer.set(AGILITY.id, (this.stuff.agility_base || 0) + (this.stuff.agility_parchment || 0));
		buffer.set(CHANCE.id, (this.stuff.chance_base || 0) + (this.stuff.chance_parchment || 0));

		return this.addStats(buffer);
	}

	protected applyBonusStats(): Map<number, number> {
		const buffer = new Map<number, number>();

		// Base Hit Points (50) + Level Hit Points (level * 5)
		buffer.set(HIT_POINTS.id, (buffer.get(VITALITY.id) || 0) + 50 + 200 * 5);

		// Base Prospecting
		buffer.set(PROSPECTING.id, (buffer.get(PROSPECTING.id) || 0) + 100);
		// Bonus Prospecting
		buffer.set(PROSPECTING.id, (buffer.get(PROSPECTING.id) || 0) + ~~((this.totalEffects.get(CHANCE.id) || 0) / 10));
		// Bonus Pods + Base Pods (1000)
		buffer.set(PODS.id, (buffer.get(PODS.id) || 0) + ((this.totalEffects.get(STRENGTH.id) || 0) * 10) + 1000);
		// Bonus Dodge and Lock
		buffer.set(DODGE.id, (buffer.get(DODGE.id) || 0) + ~~((this.totalEffects.get(AGILITY.id) || 0) / 10));
		buffer.set(LOCK.id, (buffer.get(LOCK.id) || 0) + ~~((this.totalEffects.get(AGILITY.id) || 0) / 10));
		// AP and MP parry and reduction
		buffer.set(AP_PARRY.id, (buffer.get(AP_PARRY.id) || 0) + ~~((this.totalEffects.get(WISDOM.id) || 0) / 10));
		buffer.set(AP_REDUCTION.id, (buffer.get(AP_REDUCTION.id) || 0) + ~~((this.totalEffects.get(WISDOM.id) || 0) / 10));
		buffer.set(MP_PARRY.id, (buffer.get(MP_PARRY.id) || 0) + ~~((this.totalEffects.get(WISDOM.id) || 0) / 10));
		buffer.set(MP_REDUCTION.id, (buffer.get(MP_REDUCTION.id) || 0) + ~~((this.totalEffects.get(WISDOM.id) || 0) / 10));
		// Heal
		buffer.set(HEAL.id, (buffer.get(HEAL.id) || 0) + ~~((this.totalEffects.get(INTELLIGENCE.id) || 0) / 10));
		// Initiative
		buffer.set(INITIATIVE.id, (this.totalEffects.get(INITIATIVE.id) || 0)
			+ (this.totalEffects.get(STRENGTH.id) || 0)
			+ (this.totalEffects.get(INTELLIGENCE.id) || 0)
			+ (this.totalEffects.get(AGILITY.id) || 0)
			+ (this.totalEffects.get(CHANCE.id) || 0));

		return this.addStats(buffer);
	}
}
