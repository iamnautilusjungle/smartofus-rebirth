import { DamageCalculatorFactory } from './DamageCalculatorFactory';
import {
	Damage, EffectToCalculate, StuffEffects,
} from '../../../interfaces';
import { DamageCalculator } from './DamageCalculator';

export class MassDamageCalculator {
	protected stuffEffects: StuffEffects;

	protected originalDamageList: EffectToCalculate[];

	protected calculatedDamageList: Damage[];

	constructor(stuffEffects: StuffEffects, effects: EffectToCalculate[]) {
		this.stuffEffects = stuffEffects;
		this.originalDamageList = effects;
		this.calculatedDamageList = [];

		this.calculate();
	}

	protected calculate() {
		this.calculatedDamageList = this.originalDamageList.map((effect) => this.getCalculatedDamage(effect));
	}

	protected getCalculatedDamage(effect: EffectToCalculate): Damage {
		const calculator: DamageCalculator = DamageCalculatorFactory.getCalculatorFor(this.stuffEffects, effect);
		return calculator.getCalculatedDamage();
	}

	public get(): Damage[] {
		return this.calculatedDamageList;
	}
}
