import { Effects } from '../../../../consts/effects';
import { SpecificCharacteristicsForDamageCalculation } from '../../../../interfaces/Stuff/CharacteristicsForDamageCalculation';
import { DamageCalculator } from '../DamageCalculator';

export class NeutralDamageCalculator extends DamageCalculator {
	protected getSpecificCharacteristicsForCalculation(): SpecificCharacteristicsForDamageCalculation {
		return {
			characteristic: this.stuffCalculatedCharacteristics.get(Effects.STRENGTH) || 0,
			flatCharacteristicDamage: this.stuffCalculatedCharacteristics.get(Effects.NEUTRAL_DAMAGE) || 0,
		};
	}
}
