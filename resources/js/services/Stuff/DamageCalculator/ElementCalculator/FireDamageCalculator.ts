import { Effects } from '../../../../consts/effects';
import { SpecificCharacteristicsForDamageCalculation } from '../../../../interfaces/Stuff/CharacteristicsForDamageCalculation';
import { DamageCalculator } from '../DamageCalculator';

export class FireDamageCalculator extends DamageCalculator {
	protected getSpecificCharacteristicsForCalculation(): SpecificCharacteristicsForDamageCalculation {
		return {
			characteristic: this.stuffCalculatedCharacteristics.get(Effects.INTELLIGENCE) || 0,
			flatCharacteristicDamage: this.stuffCalculatedCharacteristics.get(Effects.FIRE_DAMAGE) || 0,
		};
	}
}
