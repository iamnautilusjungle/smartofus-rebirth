import { Effects } from '../../../../consts/effects';
import { SpecificCharacteristicsForDamageCalculation } from '../../../../interfaces/Stuff/CharacteristicsForDamageCalculation';
import { DamageCalculator } from '../DamageCalculator';

export class AirDamageCalculator extends DamageCalculator {
	protected getSpecificCharacteristicsForCalculation(): SpecificCharacteristicsForDamageCalculation {
		return {
			characteristic: this.stuffCalculatedCharacteristics.get(Effects.AGILITY) || 0,
			flatCharacteristicDamage: this.stuffCalculatedCharacteristics.get(Effects.AIR_DAMAGE) || 0,
		};
	}
}
