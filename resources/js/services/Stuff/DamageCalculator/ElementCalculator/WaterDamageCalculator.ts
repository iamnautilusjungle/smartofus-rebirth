import { Effects } from '../../../../consts/effects';
import { SpecificCharacteristicsForDamageCalculation } from '../../../../interfaces/Stuff/CharacteristicsForDamageCalculation';
import { DamageCalculator } from '../DamageCalculator';

export class WaterDamageCalculator extends DamageCalculator {
	protected getSpecificCharacteristicsForCalculation(): SpecificCharacteristicsForDamageCalculation {
		return {
			characteristic: this.stuffCalculatedCharacteristics.get(Effects.CHANCE) || 0,
			flatCharacteristicDamage: this.stuffCalculatedCharacteristics.get(Effects.WATER_DAMAGE) || 0,
		};
	}
}
