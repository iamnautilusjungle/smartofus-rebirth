import { StuffEffects } from '../../../interfaces/Stuff/Stuff';
import { Damage, EffectToCalculate } from '../../../interfaces';
import { CharacteristicsForDamageCalculation, SpecificCharacteristicsForDamageCalculation } from '../../../interfaces/Stuff/CharacteristicsForDamageCalculation';
import { Effects } from '../../../consts/effects';

export abstract class DamageCalculator {
	protected stuffCalculatedCharacteristics: StuffEffects;

	protected effectToCalculate: EffectToCalculate;

	protected chars: CharacteristicsForDamageCalculation;

	protected damage: Damage;

	public constructor(stuffEffects: StuffEffects, effectToCalculate: EffectToCalculate) {
		this.stuffCalculatedCharacteristics = stuffEffects;
		this.effectToCalculate = effectToCalculate;
		this.chars = this.getCharacteristicsForCalculation();
		this.damage = this.getCalculatedDamage();
	}

	protected getCharacteristicsForCalculation(): CharacteristicsForDamageCalculation {
		return {
			power: this.stuffCalculatedCharacteristics.get(Effects.POWER) || 0,
			flatDamage: this.stuffCalculatedCharacteristics.get(Effects.FLAT_DAMAGE) || 0,
			flatCritDamage: this.stuffCalculatedCharacteristics.get(Effects.CRITICAL_DAMAGE) || 0,
			...this.getSpecificCharacteristicsForCalculation(),
		};
	}

	protected abstract getSpecificCharacteristicsForCalculation(): SpecificCharacteristicsForDamageCalculation;

	public getCalculatedDamage(): Damage {
		return this.damage || {
			min: this.getCalculatedMinDamage(),
			max: this.getCalculatedMaxDamage(),
			min_cs: this.getCalculatedMinCsDamage(),
			max_cs: this.getCalculatedMaxCsDamage(),
			type: this.effectToCalculate.id,
			icon: this.getIcon(),
		};
	}

	protected getCalculatedMinDamage(): number {
		return this.applyBaseFormula(this.effectToCalculate.min);
	}

	protected applyBaseFormula(baseDamage: number): number {
		return Math.floor(baseDamage + baseDamage * ((this.chars.characteristic + this.chars.power) / 100) + this.chars.flatDamage + this.chars.flatCharacteristicDamage);
	}

	protected getCalculatedMinCsDamage(): number | undefined {
		if (this.effectToCalculate.critical_hit_bonus && this.canCriticalHit()) {
			return this.applyBaseFormula(this.effectToCalculate.min + this.effectToCalculate.critical_hit_bonus);
		}
		return undefined;
	}

	protected canCriticalHit(): boolean {
		return !!this.effectToCalculate.critical_hit_bonus;
	}

	protected getCalculatedMaxCsDamage(): number | undefined {
		if (this.effectToCalculate.critical_hit_bonus && this.canCriticalHit()) {
			return this.applyBaseFormula(this.effectToCalculate.max + this.effectToCalculate.critical_hit_bonus);
		}
		return undefined;
	}

	protected applyCsFormula(baseDamage: number): number {
		return Math.floor(this.applyBaseFormula(baseDamage) + this.chars.flatCritDamage);
	}

	protected getCalculatedMaxDamage(): number {
		return this.applyCsFormula(this.effectToCalculate.max);
	}

	protected getIcon(): string {
		switch (this.effectToCalculate.id) {
			case Effects.AIR_DAMAGE_ON_ATTACK:
			case Effects.AIR_STEAL:
				return 'tx_agility';
			case Effects.EARTH_DAMAGE_ON_ATTACK:
			case Effects.EARTH_STEAL:
				return 'tx_strength';
			case Effects.NEUTRAL_DAMAGE_ON_ATTACK:
			case Effects.NEUTRAL_STEAL:
				return 'tx_neutral';
			case Effects.FIRE_DAMAGE_ON_ATTACK:
			case Effects.FIRE_STEAL:
				return 'tx_intelligence';
			case Effects.WATER_DAMAGE_ON_ATTACK:
			case Effects.WATER_STEAL:
				return 'tx_chance';
			default:
				return '';
		}
	}
}
