import { NeutralDamageCalculator } from './ElementCalculator/NeutralDamageCalculator';
import { AirDamageCalculator } from './ElementCalculator/AirDamageCalculator';
import { WaterDamageCalculator } from './ElementCalculator/WaterDamageCalculator';
import { StuffEffects } from '../../../interfaces/Stuff/Stuff';
import { EarthDamageCalculator } from './ElementCalculator/EarthDamageCalculator';
import { Effects } from '../../../consts/effects';
import { DamageCalculator } from './DamageCalculator';
import { FireDamageCalculator } from './ElementCalculator/FireDamageCalculator';
import { EffectToCalculate } from '../../../interfaces';

export class DamageCalculatorFactory {
	public static getCalculatorFor(stuffEffects: StuffEffects, effect: EffectToCalculate): DamageCalculator {
		switch (effect.id) {
			case Effects.NEUTRAL_DAMAGE_ON_ATTACK:
			case Effects.NEUTRAL_STEAL:
				return new NeutralDamageCalculator(stuffEffects, effect);
			case Effects.EARTH_DAMAGE_ON_ATTACK:
			case Effects.EARTH_STEAL:
				return new EarthDamageCalculator(stuffEffects, effect);
			case Effects.FIRE_DAMAGE_ON_ATTACK:
			case Effects.FIRE_STEAL:
				return new FireDamageCalculator(stuffEffects, effect);
			case Effects.WATER_DAMAGE_ON_ATTACK:
			case Effects.WATER_STEAL:
				return new WaterDamageCalculator(stuffEffects, effect);
			case Effects.AIR_DAMAGE_ON_ATTACK:
			case Effects.AIR_STEAL:
				return new AirDamageCalculator(stuffEffects, effect);
			default:
				throw Error('Wrong effect type for damage calculation');
		}
	}
}
