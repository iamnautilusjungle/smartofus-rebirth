import { Effects } from '../../../consts/effects';
import { EffectPivot } from '../../../interfaces';

export const DamageEffects = [
	Effects.WATER_STEAL,
	Effects.EARTH_STEAL,
	Effects.AIR_STEAL,
	Effects.FIRE_STEAL,
	Effects.NEUTRAL_STEAL,
	Effects.WATER_DAMAGE_ON_ATTACK,
	Effects.EARTH_DAMAGE_ON_ATTACK,
	Effects.AIR_DAMAGE_ON_ATTACK,
	Effects.FIRE_DAMAGE_ON_ATTACK,
	Effects.NEUTRAL_DAMAGE_ON_ATTACK,
];

export class DamageFromEffectPivotsSeparator {
	public static getDamageFrom(effects: EffectPivot[]): EffectPivot[] {
		return effects.filter((e) => DamageEffects.includes(e.effect_id));
	}
}
