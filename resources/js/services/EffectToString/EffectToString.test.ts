import { EffectToString } from './EffectToString';

describe('EffectToString', () => {
	const testedPositiveString = '#1{~1~2 to }#2 withTestString and words';

	test('constructor should work with standard parameters and positive string', () => {
		const effToString = new EffectToString({
			id: 1,
			name_id: 1,
			pivot: {
				dice_num: 30,
				dice_side: 60,
				effect_id: 1,
				item_id: 5,
				value: 90,
			},
		}, testedPositiveString);

		expect(effToString.getValue()).toBe('30 to 60 withTestString and words');
	});

	test('constructor should work with zero dice side parameters', () => {
		const effToString = new EffectToString({
			id: 1,
			name_id: 1,
			pivot: {
				dice_num: 30,
				dice_side: 0,
				effect_id: 1,
				item_id: 5,
				value: 90,
			},
		}, testedPositiveString);

		expect(effToString.getValue()).toBe('30 withTestString and words');
	});
});
