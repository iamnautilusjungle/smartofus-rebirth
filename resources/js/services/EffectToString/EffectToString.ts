import { Effect } from '../../interfaces';

export class EffectToString {
	effect: Effect;

	updatedString: string;

	constructor(effect: Effect, originalString: string) {
		this.effect = effect;
		this.updatedString = originalString;
		this.apply();
	}

	public getValue(): string {
		return this.updatedString;
	}

	private apply(): void {
		if (this.isDiceSideZero()) {
			this.applyWithDiceSideZero();
		} else {
			this.applyWithDefaultSettings();
		}
	}

	private isDiceSideZero(): Boolean {
		return this.effect.pivot.dice_side === 0;
	}

	private applyWithDiceSideZero(): void {
		this.cleanupForSingleDiceSide();
		this.replaceFirstParameterWith(String(this.effect.pivot.dice_num));
		this.replaceSecondParameterWith('');
	}

	private cleanupForSingleDiceSide(): void {
		this.updatedString = this.updatedString.replace(/{~1~2 (.*)-}/g, '');
		this.updatedString = this.updatedString.replace(/{~1~2 (.*)}/g, '');
	}

	private replaceFirstParameterWith(str: string): void {
		this.updatedString = this.updatedString.replace('#1', str);
	}

	private replaceSecondParameterWith(str: string): void {
		this.updatedString = this.updatedString.replace('#2', str);
	}

	private applyWithDefaultSettings(): void {
		this.cleanupForDefaultSettings();
		this.replaceFirstParameterWith(String(this.effect.pivot.dice_num));
		this.replaceSecondParameterWith(String(this.effect.pivot.dice_side));
	}

	private cleanupForDefaultSettings(): void {
		this.updatedString = this.updatedString.replace('{~1~2', '').replace('}', '');
	}
}
