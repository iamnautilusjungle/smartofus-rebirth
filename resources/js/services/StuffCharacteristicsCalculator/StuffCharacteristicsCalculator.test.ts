import { StuffCharacteristicsCalculator } from './StuffCharacteristicsCalculator';

class TestableStuffCharacteristicsCalculator extends StuffCharacteristicsCalculator {
	public static testableCalculateCostForCharAmount(v: number) {
		return TestableStuffCharacteristicsCalculator.calculateCostForCharAmount(v);
	}
}

const generateStuff = (level: number, vitality_base: number, wisdom_base: number, strength_base: number, intelligence_base: number, agility_base: number, chance_base: number) => ({
	level,
	vitality_base,
	wisdom_base,
	strength_base,
	intelligence_base,
	agility_base,
	chance_base,
});

describe('StuffCharacteristicsCalculator', () => {
	test('getRemainingCharacteristicsPoints should work with zero characteristics', () => {
		expect((new StuffCharacteristicsCalculator(generateStuff(1, 0, 0, 0, 0, 0, 0))).getRemainingCharacteristicsPoints()).toBe(0);
		expect((new StuffCharacteristicsCalculator(generateStuff(200, 0, 0, 0, 0, 0, 0))).getRemainingCharacteristicsPoints()).toBe(995);
		expect((new StuffCharacteristicsCalculator(generateStuff(113, 0, 0, 0, 0, 0, 0))).getRemainingCharacteristicsPoints()).toBe(560);
	});

	test('getRemainingCharacteristicsPoints should work with any distributed characteristics', () => {
		expect((new StuffCharacteristicsCalculator(generateStuff(200, 995, 0, 0, 0, 0, 0))).getRemainingCharacteristicsPoints()).toBe(0);
		expect((new StuffCharacteristicsCalculator(generateStuff(200, 0, 331, 0, 0, 0, 0))).getRemainingCharacteristicsPoints()).toBe(2);
		expect((new StuffCharacteristicsCalculator(generateStuff(200, 0, 0, 398, 0, 0, 0))).getRemainingCharacteristicsPoints()).toBe(3);
		expect((new StuffCharacteristicsCalculator(generateStuff(200, 0, 0, 0, 398, 0, 0))).getRemainingCharacteristicsPoints()).toBe(3);
		expect((new StuffCharacteristicsCalculator(generateStuff(200, 0, 0, 0, 0, 398, 0))).getRemainingCharacteristicsPoints()).toBe(3);
		expect((new StuffCharacteristicsCalculator(generateStuff(200, 0, 0, 0, 0, 0, 398))).getRemainingCharacteristicsPoints()).toBe(3);

		expect((new StuffCharacteristicsCalculator(generateStuff(200, 395, 0, 300, 0, 0, 0))).getRemainingCharacteristicsPoints()).toBe(0);
		expect((new StuffCharacteristicsCalculator(generateStuff(100, 0, 0, 200, 0, 0, 0))).getRemainingCharacteristicsPoints()).toBe(195);
		expect((new StuffCharacteristicsCalculator(generateStuff(100, 195, 0, 200, 0, 0, 0))).getRemainingCharacteristicsPoints()).toBe(0);
	});

	test('getRemainingCharacteristicsPoints should work with overdistributed characteristics', () => {
		expect((new StuffCharacteristicsCalculator(generateStuff(200, 1000, 0, 0, 0, 0, 0))).getRemainingCharacteristicsPoints()).toBe(-5);
		expect((new StuffCharacteristicsCalculator(generateStuff(100, 0, 0, 300, 0, 0, 0))).getRemainingCharacteristicsPoints()).toBe(-105);
	});

	test('calculateCostForCharAmount should work', () => {
		expect(TestableStuffCharacteristicsCalculator.testableCalculateCostForCharAmount(0)).toBe(0);
		expect(TestableStuffCharacteristicsCalculator.testableCalculateCostForCharAmount(50)).toBe(50);
		expect(TestableStuffCharacteristicsCalculator.testableCalculateCostForCharAmount(100)).toBe(100);
		expect(TestableStuffCharacteristicsCalculator.testableCalculateCostForCharAmount(101)).toBe(102);
		expect(TestableStuffCharacteristicsCalculator.testableCalculateCostForCharAmount(200)).toBe(300);
		expect(TestableStuffCharacteristicsCalculator.testableCalculateCostForCharAmount(201)).toBe(303);
		expect(TestableStuffCharacteristicsCalculator.testableCalculateCostForCharAmount(300)).toBe(600);
		expect(TestableStuffCharacteristicsCalculator.testableCalculateCostForCharAmount(301)).toBe(604);
		expect(TestableStuffCharacteristicsCalculator.testableCalculateCostForCharAmount(398)).toBe(992);
	});
});
