import { Characteristics } from '../../interfaces';

interface CharacteristicsWithLevel extends Characteristics {
	level: number,
}

export class StuffCharacteristicsCalculator {
	protected stuff: CharacteristicsWithLevel;

	constructor(stuff: CharacteristicsWithLevel) {
		this.stuff = stuff;
	}

	getRemainingCharacteristicsPoints(): number {
		let base = this.stuff.level * 5 - 5;

		base -= this.stuff.vitality_base || 0;
		base -= (this.stuff.wisdom_base * 3) || 0;
		base -= StuffCharacteristicsCalculator.calculateCostForCharAmount(this.stuff.strength_base);
		base -= StuffCharacteristicsCalculator.calculateCostForCharAmount(this.stuff.intelligence_base);
		base -= StuffCharacteristicsCalculator.calculateCostForCharAmount(this.stuff.agility_base);
		base -= StuffCharacteristicsCalculator.calculateCostForCharAmount(this.stuff.chance_base);

		return base;
	}

	protected static calculateCostForCharAmount(n: number): number {
		if (!n) return 0;

		if (n > 300) {
			return (n - 300) * 4 + 600;
		} if (n > 200) {
			return (n - 200) * 3 + 300;
		} if (n > 100) {
			return (n - 100) * 2 + 100;
		}

		return n;
	}
}
