import { BreedsService } from './BreedsService';

describe('BreedsService', () => {
	test('all should work', () => {
		const breeds = BreedsService.all();
		expect(breeds).toHaveLength(18);
	});
});
