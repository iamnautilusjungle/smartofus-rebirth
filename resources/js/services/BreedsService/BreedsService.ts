import Breeds from '../../data/Breeds.json';
import { Breed } from '../../interfaces';

export class BreedsService {
	static all(): Breed[] {
		return Breeds;
	}
}
