import { RING, AMULET } from '../../consts/itemTypes';
import { StuffItem } from '../../interfaces';
import { StuffBuilder } from './StuffBuilder';

const generateStuff = (
	vitality_base?: number,
	wisdom_base?: number,
	strength_base?: number,
	intelligence_base?: number,
	agility_base?: number,
	chance_base?: number,
) => ({
	id: 1,
	code: 'eZ',
	creator_id: 1,
	level: 1,
	vitality_base: vitality_base || 0,
	wisdom_base: wisdom_base || 0,
	strength_base: strength_base || 0,
	intelligence_base: intelligence_base || 0,
	agility_base: agility_base || 0,
	chance_base: chance_base || 0,
	vitality_parchment: 0,
	wisdom_parchment: 0,
	strength_parchment: 0,
	intelligence_parchment: 0,
	agility_parchment: 0,
	chance_parchment: 0,
});

function getRandomInt(max: number) {
	return Math.floor(Math.random() * max);
}

const generateItem = (typeId: number): StuffItem => ({
	id: getRandomInt(10000),
	name_id: 1,
	set: undefined,
	effects: [],
	level: 100,
	type: {
		id: typeId,
		name_id: 1,
	},
});

describe('StuffBuilder', () => {
	test('newStuffValue should work', () => {
		const stuff = generateStuff(100, 15, 100);

		const stuffBuilder = new StuffBuilder(stuff);

		expect(stuffBuilder.newStuffValue()).toMatchObject(stuff);
	});

	test('setCharacteristic should work', () => {
		const stuff = generateStuff(100, 15, 100);

		const stuffBuilder = new StuffBuilder(stuff);

		expect(stuffBuilder.setCharacteristic('vitality_base', 150)).toMatchObject({ ...stuff, vitality_base: 150 });
		expect(stuffBuilder.setCharacteristic('agility_base', 250)).toMatchObject({ ...stuff, vitality_base: 150, agility_base: 250 });
	});

	test('setParchment should work', () => {
		const stuff = generateStuff(100, 15, 100);

		const stuffBuilder = new StuffBuilder(stuff);
		const previousStuff = stuffBuilder.newStuffValue();

		expect(previousStuff.vitality_parchment).toBe(0);
		expect(previousStuff.wisdom_parchment).toBe(0);
		expect(previousStuff.intelligence_parchment).toBe(0);
		expect(previousStuff.strength_parchment).toBe(0);
		expect(previousStuff.agility_parchment).toBe(0);
		expect(previousStuff.chance_parchment).toBe(0);

		stuffBuilder.setParchment(50);
		const parchmentedStuff = stuffBuilder.newStuffValue();

		expect(parchmentedStuff.vitality_parchment).toBe(50);
		expect(parchmentedStuff.wisdom_parchment).toBe(50);
		expect(parchmentedStuff.intelligence_parchment).toBe(50);
		expect(parchmentedStuff.strength_parchment).toBe(50);
		expect(parchmentedStuff.agility_parchment).toBe(50);
		expect(parchmentedStuff.chance_parchment).toBe(50);
	});

	test('equipItem should equip valid item', () => {
		const stuff = generateStuff();

		const stuffBuilder = new StuffBuilder(stuff);
		const newItem: StuffItem = {
			id: 1,
			name_id: 1,
			set: undefined,
			effects: [],
			level: 100,
			type: {
				id: AMULET,
				name_id: 1,
			},
		};
		const previousStuff = stuffBuilder.newStuffValue();

		expect(previousStuff.amulet_id).toBeUndefined();
		expect(previousStuff.amulet).toBeUndefined();

		stuffBuilder.equipItem(newItem);

		expect(stuffBuilder.newStuffValue().amulet_id).toBe(newItem.id);
		expect(stuffBuilder.newStuffValue().amulet).toMatchObject(newItem);
	});

	test('equipItem should not equip invalid item', () => {
		const stuff = generateStuff();

		const stuffBuilder = new StuffBuilder(stuff);
		const newItem: StuffItem = generateItem(-1);
		const previousStuff = stuffBuilder.newStuffValue();

		expect(previousStuff).toMatchObject(stuffBuilder.newStuffValue());

		stuffBuilder.equipItem(newItem);

		expect(previousStuff).toMatchObject(stuffBuilder.newStuffValue());
	});

	test('equipItem should switch to next possible slot if possible', () => {
		const stuff = generateStuff();

		const stuffBuilder = new StuffBuilder(stuff);
		const newItemOne: StuffItem = generateItem(RING);
		const newItemTwo: StuffItem = generateItem(RING);
		const newItemThree: StuffItem = generateItem(RING);
		const previousStuff = stuffBuilder.newStuffValue();
		let newStuff;

		expect(previousStuff.ring_one).toBeUndefined();
		expect(previousStuff.ring_one_id).toBeUndefined();
		expect(previousStuff.ring_two).toBeUndefined();
		expect(previousStuff.ring_two_id).toBeUndefined();

		newStuff = stuffBuilder.equipItem(newItemOne);

		expect(newStuff.ring_one).toMatchObject(newItemOne);
		expect(newStuff.ring_one_id).toBe(newItemOne.id);

		newStuff = stuffBuilder.equipItem(newItemTwo);

		expect(newStuff.ring_one).toMatchObject(newItemOne);
		expect(newStuff.ring_one_id).toBe(newItemOne.id);
		expect(newStuff.ring_two).toMatchObject(newItemTwo);
		expect(newStuff.ring_two_id).toBe(newItemTwo.id);

		newStuff = stuffBuilder.equipItem(newItemThree);

		expect(newStuff.ring_one).toMatchObject(newItemOne);
		expect(newStuff.ring_one_id).toBe(newItemOne.id);
		expect(newStuff.ring_two).toMatchObject(newItemThree);
		expect(newStuff.ring_two_id).toBe(newItemThree.id);
	});

	test('desequipItem should work on empty slot', () => {
		const stuff = generateStuff();

		const stuffBuilder = new StuffBuilder(stuff);

		expect(stuffBuilder.newStuffValue().ring_one).toBeUndefined();
		expect(stuffBuilder.newStuffValue().ring_one_id).toBeUndefined();

		const newStuff = stuffBuilder.desequipItem('ring_one');

		expect(newStuff.ring_one).toBeUndefined();
		expect(newStuff.ring_one_id).toBeUndefined();
	});

	test('desequipItem should work on non empty slot', () => {
		const stuff = generateStuff();

		const stuffBuilder = new StuffBuilder(stuff);
		const newItemOne: StuffItem = generateItem(RING);
		let newStuff = stuffBuilder.equipItem(newItemOne);

		expect(stuffBuilder.newStuffValue().ring_one).toMatchObject(newItemOne);
		expect(stuffBuilder.newStuffValue().ring_one_id).toBe(newItemOne.id);

		newStuff = stuffBuilder.desequipItem('ring_one');

		expect(newStuff.ring_one).toBeUndefined();
		expect(newStuff.ring_one_id).toBeUndefined();
	});

	test('toggleIsPrivate should work', () => {
		const stuffBuilder = new StuffBuilder(generateStuff());
		let stuff = stuffBuilder.newStuffValue();

		expect(stuff.is_private).toBeUndefined();

		stuff = stuffBuilder.toggleIsPrivate();
		expect(stuff.is_private).toBeTruthy();

		stuff = stuffBuilder.toggleIsPrivate();
		expect(stuff.is_private).toBeFalsy();
	});

	test('setBreeds should work', () => {
		const stuffBuilder = new StuffBuilder(generateStuff());

		let stuff = stuffBuilder.newStuffValue();
		expect(stuff.breeds_ids).toBeUndefined();

		stuff = stuffBuilder.setBreeds([]);
		expect(stuff.breeds_ids).toEqual([]);

		stuff = stuffBuilder.setBreeds([1, 2, 3]);
		expect(stuff.breeds_ids).toEqual([1, 2, 3]);
	});

	test('setName should work', () => {
		const stuffBuilder = new StuffBuilder(generateStuff());

		let stuff = stuffBuilder.newStuffValue();
		expect(stuff.name).toBeUndefined();
		const newName = 'This is a new name';

		stuff = stuffBuilder.setName(newName);
		expect(stuff.name).toBe(newName);
	});
});
