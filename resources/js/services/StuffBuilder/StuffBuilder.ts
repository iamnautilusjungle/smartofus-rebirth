import { STUFF_ITEM_TYPES, getTypeIdFromType } from '../../consts/itemTypes';
import { Stuff, CharacteristicsOrParchments, StuffItem } from '../../interfaces';
import { EquipmentsOfStuff } from '../../interfaces/Stuff/EquipmentsOfStuff';

export class StuffBuilder {
	protected stuff: Stuff;

	constructor(stuff: Stuff) {
		this.stuff = stuff;
	}

	newStuffValue() {
		return { ...this.stuff };
	}

	setCharacteristic(k: keyof CharacteristicsOrParchments, v: number): Stuff {
		this.stuff[k] = v;
		return this.newStuffValue();
	}

	setParchment(amount: number): Stuff {
		this.stuff.vitality_parchment = amount;
		this.stuff.wisdom_parchment = amount;
		this.stuff.strength_parchment = amount;
		this.stuff.intelligence_parchment = amount;
		this.stuff.agility_parchment = amount;
		this.stuff.chance_parchment = amount;
		return this.newStuffValue();
	}

	equipItem(item: StuffItem): Stuff {
		const itemType = StuffBuilder.getStuffItemTypeOf(item);
		if (!!itemType && StuffBuilder.isTypeValidForStuff(itemType)) {
			this.tryToEquipItem(item, itemType);
		}

		return this.newStuffValue();
	}

	private static getStuffItemTypeOf(item: StuffItem): keyof EquipmentsOfStuff | undefined {
		// eslint-disable-next-line no-restricted-syntax
		for (const [key, value] of Object.entries(STUFF_ITEM_TYPES)) {
			if (value.includes(item.type.id)) {
				return key as keyof EquipmentsOfStuff;
			}
		}
		return undefined;
	}

	private static isTypeValidForStuff(type: keyof EquipmentsOfStuff | undefined): Boolean {
		return !!type;
	}

	protected tryToEquipItem(itemToEquip: StuffItem, stuffItemType: keyof EquipmentsOfStuff) {
		const emptySlotForItemType = this.getEmptySlotForItemType(stuffItemType);
		this.stuff[getTypeIdFromType(emptySlotForItemType)] = itemToEquip.id;
		this.stuff[emptySlotForItemType] = itemToEquip;
		return this.newStuffValue();
	}

	protected getEmptySlotForItemType = (type: keyof EquipmentsOfStuff): keyof EquipmentsOfStuff => {
		const isTypeSwitchable = [
			'ring_one',
			'trophy_one',
			'trophy_two',
			'trophy_three',
			'trophy_four',
			'trophy_five',
		].includes(type);

		if (!isTypeSwitchable) {
			return type;
		}

		const switchToNextTypeIfSlotIsNotEmpty = (currentType: keyof EquipmentsOfStuff, checkedType: keyof EquipmentsOfStuff, nextType: keyof EquipmentsOfStuff) => {
			if (currentType === checkedType && this.stuff[checkedType]) {
				return nextType;
			}
			return currentType;
		};

		let newType = type;

		newType = switchToNextTypeIfSlotIsNotEmpty(newType, 'ring_one', 'ring_two');
		newType = switchToNextTypeIfSlotIsNotEmpty(newType, 'trophy_one', 'trophy_two');
		newType = switchToNextTypeIfSlotIsNotEmpty(newType, 'trophy_two', 'trophy_three');
		newType = switchToNextTypeIfSlotIsNotEmpty(newType, 'trophy_three', 'trophy_four');
		newType = switchToNextTypeIfSlotIsNotEmpty(newType, 'trophy_four', 'trophy_five');
		newType = switchToNextTypeIfSlotIsNotEmpty(newType, 'trophy_five', 'trophy_six');

		return newType;
	};

	desequipItem(type: keyof EquipmentsOfStuff): Stuff {
		this.stuff[type] = undefined;
		this.stuff[getTypeIdFromType(type)] = undefined;
		return this.newStuffValue();
	}

	toggleIsPrivate(): Stuff {
		this.stuff.is_private = !this.stuff.is_private;
		return this.newStuffValue();
	}

	setBreeds(breeds_ids: Array<number>): Stuff {
		this.stuff.breeds_ids = breeds_ids;
		return this.newStuffValue();
	}

	setName(name: string): Stuff {
		this.stuff.name = name;
		return this.newStuffValue();
	}

	hasItems(): boolean {
		return this.getStuffItems().length !== 0;
	}

	getStuffItems(): StuffItem[] {
		const ret: StuffItem[] = [];

		const equipmentKeys = [
			'amulet',
			'ring_one',
			'ring_two',
			'weapon',
			'shield',
			'hat',
			'cloak',
			'belt',
			'boots',
			'pet',
			'trophy_one',
			'trophy_two',
			'trophy_three',
			'trophy_four',
			'trophy_five',
			'trophy_six',
		];

		equipmentKeys.forEach((type: string) => {
			const translatedType = type as keyof EquipmentsOfStuff;
			const item = this.stuff[translatedType];
			if (item) ret.push(item);
		});

		return ret;
	}
}
