export class CopyToClipboard {
	static copy(text: string): void {
		navigator.clipboard.writeText(text);
	}
}
