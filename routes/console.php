<?php

use App\Models\Dofus\Effect;
use App\Models\Dofus\Item;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

if (config("app.env") === 'local') {
    Artisan::command('show-effect {name}', function () {
        $effects = Effect::where("name_translations->fr", "LIKE", "%" . $this->argument('name') . "%")->get();
        foreach ($effects as $effect) {
            dump($effect);
            dump("\n");
            echo "
            {
                id: {$effect->id},
                name_id: {$effect->name_id}    
            }";
        }
    })->purpose('Show an Effect from the database.');

    Artisan::command('show-item {name}', function () {
        $effects = Item::where("name_translations->fr", "LIKE", "%" . $this->argument('name') . "%")->get();
        foreach ($effects as $effect) {
            dump($effect);
        }
    })->purpose('Show an Effect from the database.');
}
