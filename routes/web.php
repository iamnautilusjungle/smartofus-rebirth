<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// i18n Routes
Route::post("/locales/add/{locale}/{file}", [App\Http\Controllers\LocaleController::class, "add"]);

// Login Routes
Route::post("/login", [App\Http\Controllers\Auth\LoginController::class, "login"]);
Route::post("/register", [App\Http\Controllers\Auth\RegisterController::class, "register"]);
Route::post("/login/google", [App\Http\Controllers\Auth\Socialite\GoogleController::class, "login"]);
Route::post("/logout", [App\Http\Controllers\Auth\LoginController::class, "logout"]);

// Catches all other web routes.
Route::get('{slug}', function () {
    return view('welcome');
})->where('slug', '^(?!api|js|css|srv|public|locales).*$');
