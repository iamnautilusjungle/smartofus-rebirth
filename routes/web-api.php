<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Dofus\JobController as DofusJobController;
use App\Http\Controllers\Dofus\StuffController as DofusStuffController;
use App\Http\Controllers\Dofus\ItemController as DofusItemController;
use App\Http\Controllers\Auth\SpaController;

Route::middleware("web")->group(function () {
    // Miscellaneous
    Route::get("/user", [SpaController::class, "user"]);

    // Dofus
    Route::prefix("dofus")->group(function () {
        // Jobs
        Route::get("jobs", [DofusJobController::class, "jobs"]);
        Route::get("jobs/{job}", [DofusJobController::class, "job"]);
        // Stuffs
        Route::prefix("stuffs")->group(function () {
            Route::get("/my-stuffs", [DofusStuffController::class, "myStuffs"]);
        });
        Route::resource('stuffs', DofusStuffController::class);
        // Items
        Route::get("/items", [DofusItemController::class, "index"]);
    });
});
