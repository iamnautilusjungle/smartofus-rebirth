import type { Config } from '@jest/types';

// Sync object
const config: Config.InitialOptions = {
  verbose: true,
  roots: [
    './resources/js/',
  ],
};

export default config;
