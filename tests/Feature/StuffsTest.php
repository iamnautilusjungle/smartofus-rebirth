<?php

namespace Tests\Feature;

use App\Models\Dofus\Stuff;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StuffsTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    protected $seed = true;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();
        $this->normalUser = User::first();
        $stuff = new Stuff();
        $stuff->creator_id = $this->normalUser->id;
        $stuff->save();
        $this->normalUserStuff = $stuff;
    }

    public function testUnauthenticatedUserCantSeeHisStuffs()
    {
        $response = $this->get('/web-api/dofus/stuffs/my-stuffs');

        $response->assertStatus(403);
    }

    public function testAuthenticatedUserCanSeeHisStuffs()
    {
        $response = $this->actingAs($this->normalUser)->get('/web-api/dofus/stuffs/my-stuffs');

        $response->assertStatus(200);
    }

    public function testUnauthenticatedUserCanCreateStuff()
    {
        $response = $this->get('/web-api/dofus/stuffs/create');

        $response->assertStatus(200);
    }

    public function testAuthenticatedUserCanCreateStuff()
    {
        $response = $this->actingAs($this->normalUser)->get('/web-api/dofus/stuffs/create');

        $response->assertStatus(200);
    }

    public function testUnauthenticatedUserCantStoreStuff()
    {
        $response = $this->post('/web-api/dofus/stuffs');

        $response->assertStatus(403);
    }

    public function testAuthenticatedUserCanStoreStuff()
    {
        $currentCount = Stuff::count();

        $response = $this->actingAs($this->normalUser)->get('/web-api/dofus/stuffs/create');

        $stuff = new Stuff((array) $response->baseResponse->getData());

        $stuff->name = "My new name";

        $response = $this->actingAs($this->normalUser)->post('/web-api/dofus/stuffs', [
            "stuff" => $stuff->toArray()
        ]);

        $newStuff = new Stuff((array) $response->baseResponse->getData());

        $response->assertStatus(201);
        $this->assertEquals($stuff->name, $newStuff->name);
        $this->assertEquals($currentCount + 1, Stuff::count());
    }

    public function testUnauthenticatedUserCantEditStuff()
    {
        $stuff = Stuff::first();

        $response = $this->get('/web-api/dofus/stuffs/' . $stuff->id . '/edit');

        $response->assertStatus(403);
    }

    public function testAuthenticatedUserCanEditStuff()
    {
        $stuff = Stuff::first();
        $stuff->creator_id = $this->normalUser->id;
        $stuff->save();

        $response = $this->get('/web-api/dofus/stuffs/' . $stuff->id . '/edit');

        $response->assertStatus(403);
    }

    public function testAuthenticatedUserCantEditOthersPeoplesStuff()
    {
        $stuff = Stuff::first();
        $stuff->creator_id = $this->normalUser->id + 1;
        $stuff->save();

        $response = $this->get('/web-api/dofus/stuffs/' . $stuff->id . '/edit');

        $response->assertStatus(403);
    }

    public function testUnauthenticatedUserCantUpdateStuff()
    {
        $stuff = Stuff::first();

        $stuff->creator_id = $this->normalUser->id;
        $stuff->save();

        $originalName = $stuff->name;
        $stuff->name = "My new name";

        $response = $this->put('/web-api/dofus/stuffs/' . $stuff->id, [
            "stuff" => $stuff->toArray()
        ]);

        $stuff = Stuff::first();
        $response->assertStatus(403);
        $this->assertEquals($originalName, $stuff->name);
    }

    public function testAuthenticatedUserCanUpdateStuff()
    {
        $stuff = Stuff::first();

        $stuff->creator_id = $this->normalUser->id;
        $stuff->save();

        $stuff->name = "My new name";

        $response = $this->actingAs($this->normalUser)->put('/web-api/dofus/stuffs/' . $stuff->id, [
            "stuff" => $stuff->toArray()
        ]);

        $stuff = Stuff::first();
        $response->assertStatus(200);
        $this->assertEquals("My new name", $stuff->name);
    }

    public function testUnauthenticatedUserCantDeleteStuff()
    {
        $stuff = Stuff::first();

        $response = $this->delete('/web-api/dofus/stuffs/' . $stuff->id);

        $response->assertStatus(403);
    }

    public function testAuthenticatedUserCanDeleteHisStuff()
    {
        $currentCount = Stuff::count();

        $myStuff = Stuff::first();
        $myStuff->creator()->associate($this->normalUser);
        $myStuff->save();

        $response = $this->actingAs($this->normalUser)->delete('/web-api/dofus/stuffs/' . $myStuff->id);

        $response->assertStatus(200);

        $this->assertEquals($currentCount - 1, Stuff::count());
    }

    public function testAuthenticatedUserCantDeleteOthersPeoplesStuff()
    {
        $currentCount = Stuff::count();

        $notMyStuff = Stuff::first();
        $notMyStuff->creator()->associate($this->normalUser->id + 1);
        $notMyStuff->save();

        $response = $this->actingAs($this->normalUser)->delete('/web-api/dofus/stuffs/' . $notMyStuff->id);

        $response->assertStatus(403);

        $this->assertEquals($currentCount, Stuff::count());
    }
}
