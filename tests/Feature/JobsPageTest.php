<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class JobsPageTest extends TestCase
{
    use DatabaseMigrations;
    use RefreshDatabase;

    protected $seed = true;

    public function testUnauthenticatedUser()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testAllJobsRoute()
    {
        $response = $this->get('/web-api/dofus/jobs');

        $response->assertStatus(200);
    }

    public function testSpecificJobsRoute()
    {
        $this->seed();
        $job = \App\Models\Dofus\Job::first();

        $response = $this->get("/web-api/dofus/jobs/$job->id");

        $response->assertStatus(200);
    }
}
